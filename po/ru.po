# Copyright (C) 2000 Free Software Foundation, Inc.
# Valek Filippov <frob@df.ru>, 2000.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-iconedit\n"
"POT-Creation-Date: 2000-08-13 21:13+0000\n"
"PO-Revision-Date: 2000-08-13 21:21+00:00\n"
"Last-Translator: Valek Filippov <frob@df.ru>\n"
"Language-Team: Russian <ru@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=koi8-r\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/app.c:96
#, c-format
msgid "GNOME Iconedit: %s"
msgstr "�������� ���������� Gnome: %s"

#: src/app.c:143
msgid "Select region mode."
msgstr "����� ��������� �������."

#: src/app.c:144
msgid "Paint brush mode."
msgstr "����� ��������� ������."

#: src/app.c:145
msgid "Flood fill mode."
msgstr "����� �������."

#: src/app.c:146
msgid "Eyedropper mode."
msgstr "����� �������."

#: src/app.c:147
msgid "Brush paster mode."
msgstr "����� �����."

#: src/app.c:148
msgid "Unfilled rectangle mode."
msgstr "����� ���������� ��������������."

#: src/app.c:149
msgid "Unfilled circle mode."
msgstr "����� ���������� �����."

#: src/app.c:150
msgid "Filled rectangle mode."
msgstr "����� �������� ��������������."

#: src/app.c:151
msgid "Filled circle mode."
msgstr "����� �������� �����."

#: src/app.c:152
msgid "Straight line mode."
msgstr "����� ������ �����."

#: src/app.c:236
#, c-format
msgid "(%d,%d): %d,%d,%d,%d"
msgstr "(%d,%d): %d,%d,%d,%d"

#: src/edit.c:246 src/edit.c:301 src/edit.c:363
msgid "Paste Buffer is empty"
msgstr "����� ������� ����"

#: src/edit.c:308
msgid "No area is selected."
msgstr "������� �� ��������."

#: src/gnome-iconedit-component.c:254
msgid "Unable to initialise Bonobo"
msgstr "���������� ���������������� Bonobo"

#: src/gnome-iconedit-component.c:303
msgid "Paint brush mode"
msgstr "����� ��������� ������"

#: src/gnome-iconedit-component.c:310
msgid "Flood fill mode"
msgstr "����� �������� �������"

#: src/gnome-iconedit-component.c:317
msgid "Eye-dropper mode"
msgstr "����� �������"

#: src/iconedit.c:140 src/iconedit.c:171
msgid ""
"GNOME-Iconedit could not start, as it could not find the file "
"gnome-iconedit.gnorba.\n"
"This file should be installed in $(sysconfdir)/CORBA/servers where "
"$(sysconfdir) is the path returned when `gnome-config --sysconfdir` is "
"executed.\n"
"Either copy this file from the source directory, or re-compile "
"gnome-iconedit passing the argument --sysconfdir=`gnome-config --sysconfdir` "
"to the configure script."
msgstr ""
"GNOME-Iconedit �� ����� ����������, ��� ��� �� ����� ����� ���� gnome-iconedit.gnorba.\n"
"���� ���� ������ ���� ���������� � $(sysconfdir)/CORBA/servers ��� $(sysconfdir) -- ���� ������������ ��� ���������� \"gnome-config --sysconfdir\".\n"
"���� ���������� ���� ���� �� �������� �������� �������, ��� ���������������� gnome-iconedit ������� ������� configure �������� --sysconfdir=`gnome-config --sysconfdir`."

#: src/iconedit.c:213
#, c-format
msgid "Could not open %s."
msgstr "�� ������� ������� %s."

#: src/io.c:72 src/io.c:282 src/io.c:292 src/io.c:302
msgid "There was an error writing the file."
msgstr "�������� ������ ��� ������ �����."

#: src/io.c:191 src/io.c:334
#, c-format
msgid "Saving %s"
msgstr "���������� %s"

#: src/io.c:198 src/io.c:341
msgid "Saving"
msgstr "����������"

#: src/menus.c:71
msgid "_New Icon"
msgstr "����� �����������"

#: src/menus.c:72 src/menus.c:151
msgid "Create a new icon"
msgstr "������� ����� �����������"

#: src/menus.c:105
msgid "Paste _Into"
msgstr "�������� �"

#: src/menus.c:105
msgid "Paste into the current selection"
msgstr "�������� � ������� ���������"

#: src/menus.c:108
msgid "Clear"
msgstr "��������"

#: src/menus.c:108
msgid "Clear the whole icon"
msgstr "�������� ��� �����������"

#: src/menus.c:116
msgid "Show _Paste Buffer"
msgstr "�������� ����� ������"

#: src/menus.c:117
msgid "Show the paste buffer window"
msgstr "�������� ���� ������ ������"

#. {GNOME_APP_UI_ITEM, N_("Show _Undo Buffer"),
#. N_("Show the undo/redo buffer window"),
#. nothing_cb, NULL, NULL,
#. GNOME_APP_PIXMAP_FILENAME, "gnome-iconedit/gnome-tasklist.png",
#. 'O', GDK_CONTROL_MASK},
#: src/menus.c:126
msgid "Show Palette _Editor"
msgstr "�������� �������� �������"

#: src/menus.c:127
msgid "Show the palette editor window"
msgstr "�������� ���� ��������� �������"

#: src/menus.c:151
msgid "New"
msgstr "�����"

#: src/menus.c:152
msgid "Open"
msgstr "�������"

#: src/menus.c:152
msgid "Open an existing icon"
msgstr "������� ������������ �����������"

#: src/menus.c:153
msgid "Save"
msgstr "���������"

#: src/menus.c:153
msgid "Save the icon"
msgstr "��������� �����������"

#: src/menus.c:154
msgid "Save as"
msgstr "��������� ���"

#: src/menus.c:154
msgid "Save the icon under a different name"
msgstr "��������� ����������� ��� ������ ���������"

#: src/menus.c:156
msgid "Undo"
msgstr "�����"

#: src/menus.c:156
msgid "Undo the last action"
msgstr "����� ���������� ��������"

#: src/menus.c:157
msgid "Redo"
msgstr "�������"

#: src/menus.c:157
msgid "Redo the last undo action"
msgstr "������� ���������� ����������� ��������"

#: src/menus.c:159
msgid "Cut"
msgstr "��������"

#: src/menus.c:159
msgid "Cut the current selection to the paste buffer"
msgstr "�������� ������� ��������� � ����� ������"

#: src/menus.c:160
msgid "Copy"
msgstr "�����������"

#: src/menus.c:160
msgid "Copy the current selection to the paste buffer"
msgstr "����������� ������� ��������� � ����� ������"

#: src/menus.c:161
msgid "Paste"
msgstr "��������"

#: src/menus.c:161
msgid "Paste the top item in the paste buffer into the icon"
msgstr "�������� ������� ������� ������ ������ � �����������"

#: src/menus.c:167
msgid "Normal"
msgstr "����������"

#: src/menus.c:168
msgid "Additive"
msgstr "�����������"

#: src/menus.c:169
msgid "Subtractive"
msgstr "����������"

#. N_("Burn"),
#. N_("Dodge"),
#: src/menus.c:172
msgid "Alpha"
msgstr "�����"

#: src/menus.c:241
msgid "Toggle the chequerboard"
msgstr "����������� ������"

#: src/menus.c:244
msgid "Toggle the gridlines"
msgstr "������������� �����"

#: src/menus.c:247
msgid "The alpha level of the gridlines"
msgstr "�����-������� ����� �����"

#: src/menus.c:280
msgid "Paint Mode"
msgstr "����� ���������"

#: src/menus.c:290
msgid "The paint mode"
msgstr "����� ���������"

#: src/menus.c:320
msgid "Zoom level 1:"
msgstr "������� ������� 1:"

#: src/menus.c:335
msgid "The grid zoom level"
msgstr "������� �����"

#: src/menus.c:365
msgid "Select pixels"
msgstr "�������� �����"

#: src/menus.c:366
msgid "Paint pixels"
msgstr "���������� �����"

#: src/menus.c:367
msgid "Flood fill"
msgstr "���������"

#: src/menus.c:368
msgid "Eyedropper (pick color from image)"
msgstr "������� (������ ����� �� �����������)"

#: src/menus.c:369
msgid "Brush mode"
msgstr "����� �����"

#: src/menus.c:370
msgid "Draw unfilled rectangles"
msgstr "�������� ��������� ��������������"

#: src/menus.c:371
msgid "Draw unfilled circles"
msgstr "�������� ��������� �����"

#: src/menus.c:372
msgid "Draw filled rectangles"
msgstr "�������� ������� ��������������"

#: src/menus.c:373
msgid "Draw filled circles"
msgstr "�������� ������� �����"

#: src/menus.c:374
msgid "Draw straight lines"
msgstr "�������� ������ �����"

#: src/menus.c:532
msgid "This does nothing; it is not yet implemented."
msgstr "��� ������ �� ������, ������ ��� ��� �� �����������."

#: src/menus.c:563
msgid "New icon"
msgstr "����� �����������"

#: src/menus.c:572
msgid "New icon size:"
msgstr "������ ����� �����������:"

#: src/menus.c:589
msgid "New icon's width"
msgstr "������ ����� �����������"

#: src/menus.c:601
msgid "New icon's height"
msgstr "������ ����� �����������"

#. gnome_pixmap_file("gnome-hello-logo.png");
#: src/menus.c:654
msgid "Icon Edit"
msgstr "������ �����������"

#: src/menus.c:657
msgid ""
"Edits icons.\n"
"Homepage: http://www.abdn.ac.uk/~u07ih/gnome-iconedit"
msgstr ""
"������ ����������.\n"
"�������� ��������: http://www.abdn.ac.uk/~u07ih/gnome-iconedit"

#: src/menus.c:701
msgid "You must give a filename."
msgstr "�� ������ ������ ��� �����."

#: src/menus.c:712
msgid "Unable to load that file. Perhaps it isn't an image file?"
msgstr "�� ������� ��������� ���� ����. �������� ��� �� �����������?"

#: src/menus.c:741
msgid "Icon Editor Open"
msgstr "�������� ����������: �������"

#: src/menus.c:751
msgid "Icon Editor: Browse Files"
msgstr "�������� ����������: �������� ������"

#: src/menus.c:801
msgid "Icon Editor Save As"
msgstr "�������� ����������: ��������� ���"

#: src/menus.c:812
msgid "Icon Editor: Browse Files For Save As"
msgstr "�������� ����������: �������� ������ ��� \"��������� ���\""

#: src/menus.c:843 src/palette.c:384
msgid "You must give a filename"
msgstr "�� ������ ������� ��� �����"

#: src/menus.c:866
msgid ""
"Unable to load that file.\n"
"Perhaps it isn't an image file?"
msgstr ""
"�� ������� ��������� ���� ����.\n"
"�������� ��� �� �����������?"

#: src/palette.c:134
msgid "Select a palette file"
msgstr "������� ���� �������"

#: src/palette.c:203
msgid "Save the palette file as..."
msgstr "��������� ���� ������� ���..."

#: src/palette.c:335 src/palette.c:445
msgid "Load Palette From File"
msgstr "��������� ������� �� �����"

#: src/palette.c:343
msgid "Icon Editor: Browse Files For Palette From File"
msgstr "�������� ����������: �������� ������ ��� \"������� �� �����\""

#: src/palette.c:370
#, c-format
msgid "Could not open %s for reading."
msgstr "�� ������� ������� %s ��� ������."

#: src/palette.c:409
msgid "Edit Palette"
msgstr "������ �������"

#: src/palette.c:440
msgid "Load Palette"
msgstr "��������� �������"

#: src/palette.c:450
msgid "Save Palette"
msgstr "��������� �������"

#. Minimum row height
#: src/pastebuffer.c:210
msgid "Paste Buffer"
msgstr "�������� �� ������"

#: src/pastebuffer.c:223
msgid "Keep transparency"
msgstr "��������� ������������"

#: src/pastebuffer.c:236
msgid "Remove the selected item from the buffer"
msgstr "������� ���������� �������� �� ������"

#: src/view.c:187
msgid "Level: %d%%"
msgstr "�������: %d%%"

#: src/view.c:256
msgid "Preview"
msgstr "��������"

#: src/view.c:271
msgid "GNOME-Iconedit: Preview"
msgstr "�������� ���������� Gnome: ��������"

#: src/view.c:281
msgid "Mini-Preview"
msgstr "����-��������"

#: src/view.c:312
msgid "Alpha level"
msgstr "�����-�������"

#: src/view.c:315
#, c-format
msgid "Level:100%"
msgstr "�������: 100%"
