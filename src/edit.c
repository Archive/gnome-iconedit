/* edit.c
 *
 * Copyright (C) 1999 Iain Holmes <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include <gtkpastebuffer.h>

#include <ie-model.h>
#include "edit.h"
#include "app.h"

#ifdef USE_LOCAL_ART_RGBA
#include "art_rgba.h"
#else
#include <libart_lgpl/art_rgba.h>
#endif

#define OUTLINE_WIDTH 3.0
#define SQUARE_SIZE 17.0

extern GtkObject *paste_buffer;

void
ie_selection_destroy (IEView *view)
{
  IESelection *selection;
  AppMenus *am;

  if (view->selection == NULL)
    return;

  selection = view->selection;
  view->selection = NULL;

  if (selection->selitem != NULL)
    gtk_object_destroy (GTK_OBJECT (selection->selitem));

  g_free (selection);

  am = view->menus;
  gtk_widget_set_sensitive (am->cut, FALSE);
  gtk_widget_set_sensitive (am->copy, FALSE);
  gtk_widget_set_sensitive (am->b_cut, FALSE);
  gtk_widget_set_sensitive (am->b_copy, FALSE);
  gtk_widget_set_sensitive (am->paste_into, FALSE);
}

IESelection*
ie_selection_get (IEView *view)
{
  IESelection *selection;
  AppMenus *am;

  selection = view->selection;
  if (selection == NULL) {
    selection = g_new0 (IESelection, 1);
    view->selection = selection;
    
    /* Set the copy/cut options sensitive */
    am = view->menus;
    gtk_widget_set_sensitive (am->cut, TRUE);
    gtk_widget_set_sensitive (am->copy, TRUE);
    gtk_widget_set_sensitive (am->b_cut, TRUE);
    gtk_widget_set_sensitive (am->b_copy, TRUE);
    if (gtk_paste_buffer_get_size (GTK_PASTE_BUFFER (paste_buffer)) > 0)
      gtk_widget_set_sensitive (am->paste_into, TRUE);
  }
  
  return selection;
}

static void
ie_selection_draw (IEView *view)
{
  gdouble x1, y1;
  gdouble x2, y2;
  IESelection *selection;
  GnomeCanvasGroup *group;
  
  selection = view->selection;
  g_return_if_fail (selection != NULL);
  
  if (selection->selitem != NULL)
    gtk_object_destroy (GTK_OBJECT (selection->selitem));
  
  x1 = selection->x * 10;
  y1 = selection->y * 10;
  x2 = (selection->x + selection->width) * 10;
  y2 = (selection->y + selection->height) * 10;
  
  group = GNOME_CANVAS_GROUP (GNOME_CANVAS (view->canvas)->root);
  selection->selitem = gnome_canvas_item_new (group,
					      gnome_canvas_rect_get_type (),
					      "x1", x1,
					      "y1", y1,
					      "x2", x2,
					      "y2", y2,
					      "fill_color", NULL,
					      "outline_color", "white",
					      "fill_color_rgba", 0x8888FF55,
					      "width_pixels", 1,
					      NULL);
  gtk_widget_queue_resize (view->canvas); /* Work round a bug in the canvas */
}

void
edit_clear_cb (GtkWidget *widget,
	       IEView *view)
{
  guchar *pixels;
  
  pixels = gdk_pixbuf_get_pixels (view->model->model);
  memset(pixels, 0x00, view->model->rowstride * view->model->height);
  
  ie_model_model_updated (view->model, 0, 0, view->model->width, view->model->height);
}

void
edit_select_all_cb (GtkWidget *widget,
		    IEView *view)
{
  IESelection *selection;
  
  selection = view->selection;
  
  if (selection == NULL) {
    selection = ie_selection_get (view);
  }
  
  selection->model = view->model;
  selection->x = 0;
  selection->y = 0;
  selection->width = view->model->width;
  selection->height = view->model->height;
  
  ie_selection_draw (view); 
}

static void
edit_cutcopy (GtkWidget *widget,
	      IEView *view,
	      gboolean cut)
{
  int y;
  IEPasteBuffer *pbuffer;
  IEModel *model;
  IESelection *selection;
  guchar *pixels, *pixel;
  guchar *buf, *pdata;
  GdkPixbuf *pb;

  selection = view->selection;
  if (selection == NULL)
    return;

  pbuffer = g_new (IEPasteBuffer, 1);

  model = selection->model;

  pixels = gdk_pixbuf_get_pixels (model->model);

  pbuffer->x = selection->x;
  pbuffer->y = selection->y;
  pbuffer->width = selection->width;
  pbuffer->height = selection->height;

  pbuffer->rowstride = (selection->width * 4);
  g_assert (pbuffer->rowstride % 4 == 0);

  buf = pdata = g_new (guchar, pbuffer->rowstride * pbuffer->height);

  pixel = pixels + (pbuffer->y * model->width * 4) + (pbuffer->x * 4);

  for (y = 0; y < selection->height; y++) {
    /* Copy the data to the Paste Buffer, then clear it */
    memcpy (pdata, pixel, selection->width * 4);
    if (cut)
	memset (pixel, 0x00, selection->width * 4);
    
    pixel += model->rowstride;
    pdata += pbuffer->rowstride;
  }
  
  /* Make a gdk-pixbuf */
  pb = gdk_pixbuf_new_from_data (buf, GDK_COLORSPACE_RGB, TRUE, 8,
				 pbuffer->width, pbuffer->height,
				 pbuffer->rowstride, NULL, NULL);
  pbuffer->pixbuf = pb;

  /* Add our new IEPasteBuffer structure to the global paste buffer */
  gtk_paste_buffer_add (GTK_PASTE_BUFFER (paste_buffer), pbuffer);

  /* Remove the selection, and grey all related buttons */
  if (cut) {
    ie_selection_destroy (view);

    ie_model_model_updated (model, selection->x, selection->y,
			    selection->width, selection->height);
  }
}

void
edit_cut_cb (GtkWidget *widget,
	     IEView *view)
{
  edit_cutcopy (widget, view, TRUE);
}

void
edit_copy_cb (GtkWidget *widget,
	     IEView *view)
{
  edit_cutcopy (widget, view, FALSE);
}

void
edit_paste_cb (GtkWidget *widget,
	       IEView *view)
{
  IEPasteBuffer *pbuffer;
  IEModel *model;
  guchar *pixels, *pixel;
  guchar *ppixels;
  gpointer kt;
  gboolean keep_trans = FALSE;
  gint y;

  if (gtk_paste_buffer_get_size (GTK_PASTE_BUFFER (paste_buffer)) == 0) {
    gnome_error_dialog_parented (_("Paste Buffer is empty"), GTK_WINDOW (view));
    return;
  }
  
  pbuffer = gtk_paste_buffer_get (GTK_PASTE_BUFFER (paste_buffer), 
				  GTK_PASTE_BUFFER (paste_buffer)->selection);

  kt = gtk_object_get_data (paste_buffer, "keep-trans");
  if (kt)
    keep_trans = GPOINTER_TO_INT (kt);

  model = view->model;
  pixels = gdk_pixbuf_get_pixels (model->model);
  pixel = pixels + (pbuffer->y * model->width * 4) + (pbuffer->x * 4);

  ppixels = gdk_pixbuf_get_pixels (pbuffer->pixbuf);

  /* There are two different ways to do this which depend on keep_trans
     because the !keep_trans way is much faster than the keep_trans way
     and I don't want to impose the speed penalty if !keep_trans */
  if (!keep_trans) {
    for (y = 0; y < MIN(pbuffer->height, model->height); y++) {
      memcpy (pixel, ppixels, MIN(pbuffer->width, model->width) * 4);
      
      ppixels += pbuffer->rowstride;
      pixel += model->rowstride;
    }  
  } else {
    for (y = 0; y < MIN(pbuffer->height, model->height); y++) {
      art_rgba_rgba_composite (pixel, ppixels, 
			       MIN (pbuffer->width, model->width));
      ppixels += pbuffer->rowstride;
      pixel += model->rowstride;
    }
  }
  ie_model_model_updated (model, pbuffer->x, pbuffer->y, 
			  MIN (pbuffer->width, model->width), 
			  MIN (pbuffer->height, model->height));
  ie_selection_destroy (view);
}

void
edit_paste_into_cb (GtkWidget *widget,
		    IEView *view)
{
  IEPasteBuffer *pbuffer;
  IESelection *selection;
  IEModel *model;
  guchar *pixels, *pixel;
  guchar *ppixels;
  gboolean keep_trans = FALSE;
  gpointer kt;
  gint y;

  if (gtk_paste_buffer_get_size (GTK_PASTE_BUFFER (paste_buffer)) == 0) {
    gnome_error_dialog_parented (_("Paste Buffer is empty"), GTK_WINDOW (view));
    return;
  }
  
  /* We don't want to create a selection, just use one if it already exists */
  selection = view->selection;
  if (selection == NULL) {
    gnome_error_dialog_parented (_("No area is selected."),
				 GTK_WINDOW (view));
    return;
  }
  
  kt = gtk_object_get_data (paste_buffer, "keep-trans");
  if (kt)
    keep_trans = GPOINTER_TO_INT (kt);

  pbuffer = gtk_paste_buffer_get (GTK_PASTE_BUFFER (paste_buffer), 
				  GTK_PASTE_BUFFER (paste_buffer)->selection);

  model = view->model;
  pixels = gdk_pixbuf_get_pixels (model->model);
  pixel = pixels + (selection->y * model->width * 4) + (selection->x * 4);

  ppixels = gdk_pixbuf_get_pixels (pbuffer->pixbuf);

  if (!keep_trans) {
    for (y = 0; y < MIN(pbuffer->height, selection->height); y++) {
      memcpy (pixel, ppixels, MIN(pbuffer->width, selection->width) * 4);
      
      ppixels += pbuffer->rowstride;
      pixel += model->rowstride;
    }  
  } else {
    for (y = 0; y < MIN(pbuffer->height, selection->height); y++) {
      art_rgba_rgba_composite (pixel, ppixels,
			       MIN (pbuffer->width, selection->width));
      ppixels += pbuffer->rowstride;
      pixel += model->rowstride;
    }
  }
  
  /* Need to use selection, so don't destroy it till after */
  ie_model_model_updated (model, selection->x, selection->y,
			  MIN (pbuffer->width, selection->width),
			  MIN (pbuffer->height, selection->height));
  ie_selection_destroy (view);
}

void
edit_paste_at (IEView *view,
	       gint p_x,
	       gint p_y)
{
  IEPasteBuffer *pbuffer;
  IEModel *model;
  guchar *pixels, *pixel;
  guchar *ppixels;
  gboolean keep_trans = FALSE;
  gpointer kt;
  gint y;

  if (gtk_paste_buffer_get_size (GTK_PASTE_BUFFER (paste_buffer)) == 0) {
    gnome_error_dialog_parented (_("Paste Buffer is empty"), GTK_WINDOW (view));
    return;
  }
  
  model = view->model;
  kt = gtk_object_get_data (paste_buffer, "keep-trans");
  if (kt)
    keep_trans = GPOINTER_TO_INT (kt);
  
  pbuffer = gtk_paste_buffer_get (GTK_PASTE_BUFFER (paste_buffer), 
				  GTK_PASTE_BUFFER (paste_buffer)->selection);
  
  pixels = gdk_pixbuf_get_pixels (model->model);
  pixel = pixels + (p_y * model->width * 4) + (p_x * 4);

  ppixels = gdk_pixbuf_get_pixels (pbuffer->pixbuf);

  if (!keep_trans){
    for (y = 0; y < MIN(pbuffer->height, model->height - p_y); y++) {
      memcpy (pixel, ppixels, MIN(pbuffer->width, model->width - p_x) * 4);
      
      ppixels += pbuffer->rowstride;
      pixel += model->rowstride;
    }  
  } else {
    for (y = 0; y < MIN(pbuffer->height, model->height - p_y); y++) {
      art_rgba_rgba_composite (pixel, ppixels,
			       MIN (pbuffer->width, model->width - p_x));
      ppixels += pbuffer->rowstride;
      pixel += model->rowstride;
    }
  }
  
  ie_selection_destroy (view);
  ie_model_model_updated (model, p_x, p_y, 
			  MIN (pbuffer->width, model->width - p_x),
			  MIN (pbuffer->height, model->height - p_y));
}
