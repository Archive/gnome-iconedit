/* pastebuffer.c
 *
 * A graphical representation of the GtkPasteBuffer object 
 * Copyright (C) 1999 Iain Holmes
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include <gtkpastebuffer.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "edit.h"
#include "pastebuffer.h"

extern GtkObject *paste_buffer;

void pbwindow_delete (GtkButton *button,
		      PBWindow *pbw);
static void
make_preview_from_pixbuf (GdkPixbuf *pixbuf,
			  GdkPixmap **pmap,
			  GdkPixmap **bmap)
{
  GdkVisual *visual;
  GdkGC *gc;
  GdkPixmap *p, *b;
  gint width, height;

  *pmap = NULL;
  *bmap = NULL;

  g_return_if_fail (pixbuf != NULL);
  g_return_if_fail (pmap != NULL && bmap != NULL);

  width = gdk_pixbuf_get_width (pixbuf);
  height = gdk_pixbuf_get_height (pixbuf);

  visual = gdk_rgb_get_visual ();
  p = gdk_pixmap_new (NULL, width, height, visual->depth);
  b = gdk_pixmap_new (NULL, width, height, 1);

  gc = gdk_gc_new (p);
  gdk_pixbuf_render_to_drawable (pixbuf,
				 p, gc,
				 0, 0,
				 0, 0,
				 width, height,
				 GDK_RGB_DITHER_NONE,
				 0, 0);
  gdk_pixbuf_render_threshold_alpha (pixbuf, b,
				     0, 0,
				     0, 0,
				     width, height,
				     127);
  gdk_gc_unref (gc);

  *pmap = p;
  *bmap = b;
}
				 
static void
pb_window_item_added_cb (GtkPasteBuffer *pbuffer,
			 PBWindow *pbw)
{
  GtkCList *clist;
  IEPasteBuffer *pb = NULL;
  gchar *row[2];
  gint rowno;
  GdkPixmap *pmap, *bmap;

  pb = gtk_paste_buffer_get (pbuffer, 0);
  if (!pb)
    return;

  clist = GTK_CLIST (pbw->clist);
  row[1] = g_strdup_printf ("%d,%d: %dx%d", pb->x, pb->y, 
			    pb->width, pb->height);
  make_preview_from_pixbuf (pb->pixbuf, &pmap, &bmap);
  rowno = gtk_clist_prepend (clist, row);
  gtk_clist_set_pixmap (clist, rowno, 0, pmap, bmap);
  pbw->row_size = MAX(pbw->row_size, pb->height);
  pbw->row_size = MIN(pbw->row_size, 64);
  gtk_clist_set_row_height (clist, pbw->row_size);
}

static void
build_paste_buffer (PBWindow *pbw)
{
  GtkCList *clist;
  GList *tmp;

  tmp = GTK_PASTE_BUFFER (paste_buffer)->buffer;

  clist = GTK_CLIST (pbw->clist);
  for (; tmp; tmp = tmp->next)
    {
      gchar *row[2];
      gint rowno;
      GdkPixmap *pmap, *bmap;
      IEPasteBuffer *pb;
      
      pb = tmp->data;
      row[1] = g_strdup_printf ("%d,%d: %dx%d", pb->x, pb->y, 
				pb->width, pb->height);
      make_preview_from_pixbuf (pb->pixbuf, &pmap, &bmap);
      rowno = gtk_clist_append (clist, row);
      gtk_clist_set_pixmap (clist, rowno, 0, pmap, bmap);

      /* Row size at least as big as is needed, up to a max of 64 */
      pbw->row_size = MAX(pbw->row_size, pb->height);
      pbw->row_size = MIN(pbw->row_size, 64);
      gtk_clist_set_row_height (clist, pbw->row_size);
    }
}

static void
pbwindow_destroyed (GtkWidget *window,
		    PBWindow **pbwindow)
{
  gtk_signal_disconnect_by_data (paste_buffer, *pbwindow);

  g_free (*pbwindow);
  *pbwindow = NULL;
}

static void
pbwindow_row_selected (GtkCList *clist,
		       gint row,
		       gint column,
		       GdkEventButton *event,
		       PBWindow *pbw)
{
  GTK_PASTE_BUFFER (paste_buffer)->selection = row;
  gtk_widget_set_sensitive (pbw->delete, TRUE);
}

static void
pbwindow_row_unselected (GtkCList *clist,
			 gint row,
			 gint column,
			 GdkEventButton *event,
			 PBWindow *pbw)
{
  GTK_PASTE_BUFFER (paste_buffer)->selection = 0;
  gtk_widget_set_sensitive (pbw->delete, FALSE);
}

void
pbwindow_delete (GtkButton *button,
		 PBWindow *pbw)
{
  GtkPasteBuffer *pb;
  IEPasteBuffer *iepb;
  gint row;

  pb = GTK_PASTE_BUFFER (paste_buffer);
  row = GPOINTER_TO_INT (GTK_CLIST (pbw->clist)->selection->data);
  
  iepb = gtk_paste_buffer_remove (GTK_PASTE_BUFFER (paste_buffer), row);
  gtk_clist_remove (GTK_CLIST (pbw->clist), row);

  if (GTK_CLIST (pbw->clist)->rows == 0)
    gtk_widget_set_sensitive (pbw->delete, FALSE);
}

static void
trans_toggle_cb (GtkToggleButton *toggle,
		 gpointer data)
{
  gtk_object_set_data (paste_buffer, "keep-trans",
		       GINT_TO_POINTER (gtk_toggle_button_get_active (toggle)));
}

void
paste_buffer_show (GtkWidget *widget,
		   gpointer app)
{
  static PBWindow *pbw = NULL;
  GtkWidget *scroller;
  GtkWidget *hbox;
  GtkWidget *button_pmap;
  GtkWidget *checkbox;
  GtkTooltips *tt;

  if (pbw != NULL)
    {
      g_assert(GTK_WIDGET_REALIZED(pbw->window));
      gdk_window_show(pbw->window->window);
      gdk_window_raise(pbw->window->window);
    }
  else
    {
      tt = gtk_tooltips_new ();
      pbw = g_new (PBWindow, 1);
      pbw->row_size = 20; /* Minimum row height */
      pbw->window = gnome_dialog_new (_("Paste Buffer"),
				      GNOME_STOCK_BUTTON_CLOSE,
				      NULL);
      gnome_dialog_set_close (GNOME_DIALOG (pbw->window), TRUE);

      scroller = gtk_scrolled_window_new (NULL, NULL);
      pbw->clist = gtk_clist_new (2);
      gtk_clist_set_column_width (GTK_CLIST (pbw->clist), 0, 64);
      gtk_container_add (GTK_CONTAINER (scroller), pbw->clist);
      gtk_widget_set_usize (scroller, 200, 250);

      gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (pbw->window)->vbox), scroller,
			  TRUE, TRUE, 0);
      checkbox = gtk_check_button_new_with_label (_("Keep transparency"));
      gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (pbw->window)->vbox), checkbox,
			  FALSE, FALSE, 0);
      gtk_signal_connect (GTK_OBJECT (checkbox), "toggled",
			  GTK_SIGNAL_FUNC (trans_toggle_cb), NULL);

      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (checkbox), FALSE);
      hbox = gtk_hbox_new (TRUE, GNOME_PAD);

      pbw->delete = gtk_button_new ();
      button_pmap = gnome_stock_new_with_icon (GNOME_STOCK_PIXMAP_TRASH);
      gtk_container_add (GTK_CONTAINER (pbw->delete), button_pmap);
      gtk_box_pack_start (GTK_BOX (hbox), pbw->delete, FALSE, FALSE, 0);
      gtk_tooltips_set_tip (tt, pbw->delete, _("Remove the selected item from the buffer"),
			    NULL);

      gtk_widget_set_sensitive (pbw->delete, FALSE);

      gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (pbw->window)->vbox), hbox,
			  FALSE, FALSE, 0);

      /* Connect to the paste buffer GtkObject so we can update
	 the paste buffer window */
      gtk_signal_connect (paste_buffer, "item-added",
			  GTK_SIGNAL_FUNC (pb_window_item_added_cb),
			  pbw);

      gtk_signal_connect (GTK_OBJECT (pbw->window), "destroy",
			  GTK_SIGNAL_FUNC (pbwindow_destroyed),
			  &pbw);

      gtk_signal_connect (GTK_OBJECT (pbw->clist), "select-row",
			  GTK_SIGNAL_FUNC (pbwindow_row_selected),
			  pbw);

      gtk_signal_connect (GTK_OBJECT (pbw->clist), "unselect-row",
			  GTK_SIGNAL_FUNC (pbwindow_row_unselected),
			  pbw);

      gtk_signal_connect (GTK_OBJECT (pbw->delete), "clicked",
			  GTK_SIGNAL_FUNC (pbwindow_delete),
			  pbw);

      if (gtk_paste_buffer_get_size (GTK_PASTE_BUFFER (paste_buffer)) > 0)
	build_paste_buffer (pbw);

      gtk_widget_show_all (pbw->window);
    }

  return;
}
