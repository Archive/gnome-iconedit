/* view.h - View widget for GNOME-Iconedit
 *
 * Copyright (C) 2000 Iain Holmes
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __IE_VIEW_H__
#define __IE_VIEW_H__

#include <ie-model.h>
#include <app.h>
#include <gnome.h>
#include "selection.h"

typedef struct _IEView IEView;
typedef struct _IEViewClass IEViewClass;
typedef struct _IEBonoboView IEBonoboView;

typedef struct _AppMenus AppMenus;
struct _AppMenus
{
  GtkWidget *undo;
  GtkWidget *redo;
  GtkWidget *paste;
  GtkWidget *paste_into;
  GtkWidget *cut;
  GtkWidget *copy;
  
  /* Toolbar buttons */
  GtkWidget *b_paste;
  GtkWidget *b_cut;
  GtkWidget *b_copy;
  GtkWidget *b_undo;
  GtkWidget *b_redo;

  GtkWidget *tools[IET_END];
};

struct _IEView
{
  GnomeApp window;

  IEModel *model; /* Model */

  IESelection *selection; /* Selection */

  Iconedit *app;

  GtkWidget *canvas;
  GtkWidget *preview;
  GtkWidget *mini;
  GtkWidget *status;
  GtkWidget *picker;
  GtkObject *alpha;
  GtkWidget *palette;
  GtkWidget *floater;

  GdkColor current_colour;
  GdkColor black;
  IETool tool;
  IEMode mode;

  gpointer grid;

  AppMenus *menus;
};

struct _IEBonoboView {
  IEModel *model;

  GdkColor current_colour;
  GdkColor black;
  IETool tool;
  IESelection *selection;

  GtkWidget *canvas;
  GtkWidget *picker;
  GtkWidget *alpha;

  gpointer grid;
};

struct _IEViewClass
{
  GnomeAppClass parent_class;
};

GtkType ie_view_get_type (void);
GtkWidget* ie_view_new (IEModel *model);

#endif
