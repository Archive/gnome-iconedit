/* iconedit.c
 *
 * Copyright (C) 1999 Havoc Pennington
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>

#ifdef USING_OAF
#include <liboaf/liboaf.h>
#else
#include <libgnorba/gnorba.h>
#endif

#include <gdk-pixbuf/gdk-pixbuf.h>

#include <gtkpastebuffer.h>
#include "app.h"
#include "iconedit.h"
#include "menus.h"
#include "corba.h"

extern int has_iconedit_factory;

static gint session_die(GnomeClient* client, gpointer client_data);

static gint save_session(GnomeClient *client, gint phase, 
                         GnomeSaveStyle save_style,
                         gint is_shutdown, GnomeInteractStyle interact_style,
                         gint is_fast, gpointer client_data);

Iconedit *iconedit_new_app (gchar *filename);
Iconedit *iconedit_new_app_at_size (CORBA_short w,
				    CORBA_short h);

GtkObject *paste_buffer = NULL;

struct poptOption options[] = {
  {NULL, '\0', 0, NULL, 0, NULL, NULL}
};

int 
main(int argc, char* argv[])
{
  poptContext pctx;

  GSList *files = NULL;
  char** args;

  GnomeClient* client;

  CORBA_ORB orb;
  CORBA_Environment ev;

  gboolean is_factory = FALSE;

  bindtextdomain(PACKAGE, GNOMELOCALEDIR);  
  textdomain(PACKAGE);

  CORBA_exception_init (&ev);

#ifdef USING_OAF
  gnome_init_with_popt_table (PACKAGE, VERSION, argc, argv,
			      oaf_popt_options, 0, &pctx);
  orb = oaf_init (argc, argv);
#else
  orb = gnome_CORBA_init_with_popt_table(PACKAGE, VERSION, &argc, argv, 
					 options, 0, &pctx, 0, &ev);
#endif

  if (ev._major != CORBA_NO_EXCEPTION)
    exit (5);

  CORBA_exception_free (&ev);

  gdk_rgb_init ();
  /* Argument parsing */
  args = (char **)poptGetArgs(pctx);

  if (args != NULL) {
    while (*args != NULL) {
      files = g_slist_prepend (files, *args);
      args++;
    }
  }

    poptFreeContext(pctx);

  corba_init_server (orb);
  if (!has_iconedit_factory){
    corba_activate_server ();
    is_factory = TRUE;
  }

  /* Session Management */
  
  client = gnome_master_client ();
  gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
                      GTK_SIGNAL_FUNC (save_session), argv[0]);
  gtk_signal_connect (GTK_OBJECT (client), "die",
                      GTK_SIGNAL_FUNC (session_die), NULL);

  /* Create a blank paste buffer */
  paste_buffer = gtk_paste_buffer_new ();

  /* Main app */
  if (files){
    gboolean has_icon;
    while (files) {
      gboolean use_factory = TRUE;
      has_icon = FALSE;
      
      if (use_factory) {
	CORBA_Environment new_ev;
	CORBA_Object icon;
	
	CORBA_exception_init (&new_ev);
	icon = create_iconedit_via_factory (files->data, &new_ev);
	if (ev._major != CORBA_NO_EXCEPTION)
	  exit (5);
	if (icon) {
	  CORBA_Object_release (icon, &new_ev);
	  has_icon = TRUE;
	} else {
	  g_error (_("GNOME-Iconedit could not start, as it could not find the file gnome-iconedit.gnorba.\n"
		     "This file should be installed in $(sysconfdir)/CORBA/servers where $(sysconfdir) is the path returned when `gnome-config --sysconfdir` is executed.\n"
		     "Either copy this file from the source directory, or re-compile gnome-iconedit passing the argument --sysconfdir=`gnome-config --sysconfdir` to the configure script."));
	}
	
	CORBA_exception_free (&new_ev);
      } else
	iconedit_new_app (files->data);
      
      files = files->next;
    }
    /* All files launched by the factory
       Just return if this isn't the factory itself */
    if (!is_factory)
      return 0;
  } else {
    gboolean has_icon = FALSE;
    gboolean use_factory = TRUE;
    
    if (use_factory) {
      CORBA_Environment new_ev;
      CORBA_Object icon;
      
      CORBA_exception_init (&new_ev);
      icon = create_iconedit_at_size_via_factory (48, 48, &new_ev);
      if (ev._major != CORBA_NO_EXCEPTION)
	exit (5);
      if (icon) {
	CORBA_Object_release (icon, &new_ev);
	has_icon = TRUE;
      } else {
	g_error (_("GNOME-Iconedit could not start, as it could not find the file gnome-iconedit.gnorba.\n"
		   "This file should be installed in $(sysconfdir)/CORBA/servers where $(sysconfdir) is the path returned when `gnome-config --sysconfdir` is executed.\n"
		   "Either copy this file from the source directory, or re-compile gnome-iconedit passing the argument --sysconfdir=`gnome-config --sysconfdir` to the configure script."));
      }
      
      CORBA_exception_free (&new_ev);
    } else
      iconedit_new_app_at_size (48, 48);
    
    if (!is_factory) {
	g_print ("Beep!\n");
	return 0;
      }
    }
  
  gtk_main();
  
  return 0;
}

Iconedit *
iconedit_new_app_at_size (CORBA_short w,
			  CORBA_short h)
{
  Iconedit *app;

  g_return_val_if_fail (w > 0 && h > 0, NULL);
  app = ie_app_new (w, h, NULL);

  return app;
}

Iconedit *
iconedit_new_app (gchar *filename)
{
  Iconedit *app;
  GdkPixbuf *pb;
  gint w, h;
  
  pb = gdk_pixbuf_new_from_file (filename);
  
  if (pb == NULL) {
      g_warning (_("Could not open %s."), filename);
      return NULL;
  }
  
  w = gdk_pixbuf_get_width (pb);
  h = gdk_pixbuf_get_height (pb);
  
  app = ie_app_new(w, h, pb);
  
  gdk_pixbuf_unref (pb);
  return app;
}

static gint
save_session (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
              gint is_shutdown, GnomeInteractStyle interact_style,
              gint is_fast, gpointer client_data)
{
  gchar** argv;
  guint argc;

  /* allocate 0-filled, so it will be NULL-terminated */
  argv = g_malloc0(sizeof(gchar*)*4);
  argc = 1;

  argv[0] = client_data;
  
  gnome_client_set_clone_command (client, argc, argv);
  gnome_client_set_restart_command (client, argc, argv);

  return TRUE;
}

static gint 
session_die(GnomeClient* client, gpointer client_data)
{
  gtk_main_quit ();
  return TRUE;
}
