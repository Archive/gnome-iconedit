/* grid.h
 *
 * Copyright (C) 1999,2000 Havoc Pennington, Iain Holmes
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */
#ifndef ICONEDIT_GRID_H
#define ICONEDIT_GRID_H

#include <config.h>
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "view.h"

typedef struct _Grid Grid;

struct _Grid {
  IEView *view; /* The view the grid is attached to */

  GnomeCanvasItem *layer;
  GnomeCanvasItem *mainlayer; /* Current drawing layer */
  GnomeCanvasItem *cboard; /* Chequerboard */
  GnomeCanvasItem *gridlines; /* Gridlines */
  GnomeCanvasItem *sel;
};

void grid_destroy (Grid *grid);          
Grid* grid_new(IEView *view);

GnomeCanvasItem* grid_square(Grid* g, guint x, guint y);

const guchar* grid_pixel(Grid* g, guint x, guint y);

const guchar* grid_alpha(Grid* g, guint x, guint y);

void grid_set(Grid* g, guint x, guint y, 
	      guchar r, guchar gr, guchar b, guchar a, gboolean update);

void grid_set_gdk(Grid* g, guint x, guint y,
		  GdkColor* color, guchar a, gboolean update);

void grid_destroy(Grid* grid);

void grid_set_from_pixbuf(Grid* g, GdkPixbuf* pb);

GdkPixbuf* grid_to_pixbuf(Grid* grid);

void grid_update_item (Grid *g,
		       int x,
		       int y,
		       int width,
		       int height);
#endif


