/* mode.c - Mode handling routines
 *
 * Copyright (C) 2000 Iain Holmes <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include "app.h"
#include "grid.h"

void mode_convert_pixel (Grid *g,
			 guint x,
			 guint y,
			 guchar *r,
			 guchar *gr,
			 guchar *b,
			 guchar *a);

void
mode_convert_pixel (Grid *g,
		    guint x,
		    guint y,
		    guchar *r,
		    guchar *gr,
		    guchar *b,
		    guchar *a)
{
  int mode = g->view->mode;

  switch (mode)
    {
    case IEM_NORMAL:
      return;
    case IEM_ADDITIVE:
      {
	const guchar *o_colour;
	guchar n_r, n_g, n_b, n_a;

	o_colour = ie_model_get_pixel (g->view->model, x, y);
	n_r = MIN (o_colour[0] + *r, 255);
	n_g = MIN (o_colour[1] + *gr, 255);
	n_b = MIN (o_colour[2] + *b, 255);
	n_a = MIN (o_colour[3] + *a, 255);

	*r = n_r;
	*gr = n_g;
	*b = n_b;
	*a = n_a;
	break;
      }
    case IEM_SUBTRACTIVE:
      {
	const guchar *o_colour;
	guchar n_r, n_g, n_b, n_a;
	
	o_colour = ie_model_get_pixel (g->view->model, x, y);
	n_r = MAX (o_colour[0] - *r, 0);
	n_g = MAX (o_colour[1] - *gr, 0);
	n_b = MAX (o_colour[2] - *b, 0);
	n_a = MAX (o_colour[3] - *a, 0);
	
	*r = n_r;
	*gr = n_g;
	*b = n_b;
	*a = n_a;
	break;
      }
    case IEM_ALPHA:
      {
	const guchar *o_colour;
	
	o_colour = ie_model_get_pixel (g->view->model, x, y);
	
	*r = o_colour[0];
	*gr = o_colour[1];
	*b = o_colour[2];
	
	break;
      }

#if 0
      /* Turned off until I work out how dodge and burn actually work */
    case IEM_BURN:
      {
	guchar *o_colour;
	guchar mx, h, s, v;
	
	o_colour = ie_model_get_pixel (g->view->model, x, y);
	
      }
      break;
    case IEM_DODGE:
      {
	guchar *o_colour;
	guchar mx;
	
	o_colour = ie_model_get_pixel (g->view->model, x, y);
	    mx = MAX (o_colour[0], MAX (o_colour[1], o_colour[2]));
	    
	    if (mx == o_colour[0])
	      {
		*r = MAX (o_colour[0] - *r, 0);
		*gr = o_colour[1];
		*b = o_colour[2];
	      }
	    else if (mx == o_colour[1])
	      {
		*r = o_colour[0];
		*gr = MAX (o_colour[1] - *gr, 0);
		*b = o_colour[2];
	      }
	    else if (mx == o_colour[2])
	      {
		*r = o_colour[0];
		*gr = o_colour[1];
		*b = MAX (o_colour[2] - *b, 0);
	      }
	    *a = o_colour[3];
      }
      break;
#endif
  
    default:
      break;
    }
}

