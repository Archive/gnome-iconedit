/* menus.c
 *
 * Copyright (C) 1999 Havoc Pennington
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "menus.h"
#include "view.h"
#include "app.h"
#include "grid.h"
#include "palette.h"
#include "pastebuffer.h"
#include "io.h"
#include "edit.h"
#include <config.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#ifdef HAVE_GNOME_PRINT
#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gnome-printer.h>
#include <libgnomeprint/gnome-print-meta.h>
#include <libgnomeprint/gnome-print-preview.h>
#include <libgnomeprint/gnome-print-pixbuf.h>
#include <libgnomeprint/gnome-font.h>
#include <libgnomeprint/gnome-printer-profile.h>
#include <libgnomeprint/gnome-printer-dialog.h>
#include <libgnomeprint/gnome-print-master.h>
#include <libgnomeprint/gnome-print-master-preview.h>
#include <libgnomeprint/gnome-print-dialog.h>
#endif

#include <gtkpastebuffer.h>
#include <iestatus.h>

extern GtkObject *paste_buffer;

static void nothing_cb(GtkWidget* widget, gpointer data);
static void new_app_cb(GtkWidget* widget, gpointer data);
static void open_cb   (GtkWidget* widget, IEView *view);
static void save_as_cb(GtkWidget* widget, IEView *view);
static void revert_cb (GtkWidget* widget, IEView *view);

#ifdef HAVE_GNOME_PRINT
static void print_cb  (GtkWidget *widget, IEView *view);
#endif

static void close_cb  (GtkWidget* widget, IEView *view);
static void exit_cb   (GtkWidget* widget, gpointer data);
static void about_cb  (GtkWidget* widget, gpointer data);

static void mode_changed_cb (GtkWidget *widget, IEView *view);
static void zoom_changed (GtkAdjustment *adj, IEView *view);

void set_filename (IEView *view, gchar* filename);

static GnomeUIInfo file_menu [] = {
  GNOMEUIINFO_MENU_NEW_ITEM(N_("_New Icon"),
                            N_("Create a new icon"),
                            new_app_cb, NULL),

  GNOMEUIINFO_MENU_OPEN_ITEM(open_cb, NULL),

  GNOMEUIINFO_MENU_SAVE_ITEM(save_cb, NULL),

  GNOMEUIINFO_MENU_SAVE_AS_ITEM(save_as_cb, NULL),

  GNOMEUIINFO_MENU_REVERT_ITEM(revert_cb, NULL),

#ifdef HAVE_GNOME_PRINT
  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_MENU_PRINT_ITEM (print_cb, NULL),
#endif

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_MENU_CLOSE_ITEM(close_cb, NULL),

  GNOMEUIINFO_MENU_EXIT_ITEM(exit_cb, NULL),

  GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu [] = {
  GNOMEUIINFO_MENU_UNDO_ITEM(nothing_cb, NULL),
  GNOMEUIINFO_MENU_REDO_ITEM(nothing_cb, NULL), 
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_CUT_ITEM(edit_cut_cb, NULL), 
  GNOMEUIINFO_MENU_COPY_ITEM(edit_copy_cb, NULL),
  GNOMEUIINFO_MENU_PASTE_ITEM(nothing_cb, NULL),
  {GNOME_APP_UI_ITEM,N_("Paste _Into"), N_("Paste into the current selection"),
   edit_paste_into_cb, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
   GNOME_STOCK_MENU_PASTE, 'I', GDK_CONTROL_MASK},
  {GNOME_APP_UI_ITEM, N_("Clear"), N_("Clear the whole icon"),
   edit_clear_cb, NULL, NULL, 
   0, 0, 'K', GDK_CONTROL_MASK},
  GNOMEUIINFO_MENU_SELECT_ALL_ITEM(edit_select_all_cb, NULL), 
  GNOMEUIINFO_END
};

static GnomeUIInfo window_menu [] = {
  {GNOME_APP_UI_ITEM, N_("Show _Paste Buffer"),
   N_("Show the paste buffer window"),
   paste_buffer_show, NULL, NULL, 
   GNOME_APP_PIXMAP_FILENAME, "gnome-iconedit/gnome-tasklist.png", 
   'P', GDK_CONTROL_MASK},
  /*  {GNOME_APP_UI_ITEM, N_("Show _Undo Buffer"),
   N_("Show the undo/redo buffer window"),
   nothing_cb, NULL, NULL,
   GNOME_APP_PIXMAP_FILENAME, "gnome-iconedit/gnome-tasklist.png",
   'O', GDK_CONTROL_MASK},*/
  {GNOME_APP_UI_ITEM, N_("Show Palette _Editor"),
   N_("Show the palette editor window"),
   palette_edit, NULL, NULL,
   GNOME_APP_PIXMAP_FILENAME, "gnome-iconedit/palette-edit.png",
   'E', GDK_CONTROL_MASK},
  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu [] = {
  GNOMEUIINFO_HELP ("gnome-iconedit"),
  
  GNOMEUIINFO_MENU_ABOUT_ITEM(about_cb, NULL),
  
  GNOMEUIINFO_END
};

static GnomeUIInfo menu [] = {
  GNOMEUIINFO_MENU_FILE_TREE(file_menu),
  GNOMEUIINFO_MENU_EDIT_TREE(edit_menu),
  GNOMEUIINFO_MENU_WINDOWS_TREE(window_menu),
  GNOMEUIINFO_MENU_HELP_TREE(help_menu),
  GNOMEUIINFO_END
};

static GnomeUIInfo toolbar [] = {
  GNOMEUIINFO_ITEM_STOCK (N_("New"), N_("Create a new icon"), new_app_cb, GNOME_STOCK_PIXMAP_NEW),
  GNOMEUIINFO_ITEM_STOCK (N_("Open"), N_("Open an existing icon"), open_cb, GNOME_STOCK_PIXMAP_OPEN),
  GNOMEUIINFO_ITEM_STOCK (N_("Save"), N_("Save the icon"), save_cb, GNOME_STOCK_PIXMAP_SAVE),
  GNOMEUIINFO_ITEM_STOCK (N_("Save as"), N_("Save the icon under a different name"), save_as_cb, GNOME_STOCK_PIXMAP_SAVE_AS),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK (N_("Undo"), N_("Undo the last action"), nothing_cb, GNOME_STOCK_PIXMAP_UNDO),
  GNOMEUIINFO_ITEM_STOCK (N_("Redo"), N_("Redo the last undo action"), nothing_cb, GNOME_STOCK_PIXMAP_REDO),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_ITEM_STOCK (N_("Cut"), N_("Cut the current selection to the paste buffer"), edit_cut_cb, GNOME_STOCK_PIXMAP_CUT),
  GNOMEUIINFO_ITEM_STOCK (N_("Copy"), N_("Copy the current selection to the paste buffer"), edit_copy_cb, GNOME_STOCK_PIXMAP_COPY),
  GNOMEUIINFO_ITEM_STOCK (N_("Paste"), N_("Paste the top item in the paste buffer into the icon"), edit_paste_cb, GNOME_STOCK_PIXMAP_PASTE),
  GNOMEUIINFO_END
};

static char *ie_paint_mode[IEM_END] =
{
  N_("Normal"),
  N_("Additive"),
  N_("Subtractive"),
  /*  N_("Burn"),
      N_("Dodge"), */
  N_("Alpha")
};

static void
toggle_grid (GtkToggleButton *toggle,
	     IEView *view)
{
  if (gtk_toggle_button_get_active (toggle)) {
    gnome_canvas_item_show ( ((Grid *) view->grid)->gridlines);
  } else {
    gnome_canvas_item_hide ( ((Grid *) view->grid)->gridlines);
  }
}

static void
toggle_cheques (GtkToggleButton *toggle,
		IEView *view)
{
  if (gtk_toggle_button_get_active (toggle)) {
    gnome_canvas_item_show ( ((Grid *)view->grid)->cboard);
  } else {
    gnome_canvas_item_hide ( ((Grid *)view->grid)->cboard);
  }
}

static void
grid_value_changed_cb (GtkAdjustment *adj,
		       IEView *view)
{
  gnome_canvas_item_set ( ((Grid *)view->grid)->gridlines,
			  "fill_color_rgba", (guint32)adj->value,
			  NULL);
}

static GtkWidget*
ie_create_grid_bar (IEView *app)
{
  GtkWidget *modebar;
  GtkWidget *gridlines, *chequers;
  GtkWidget *p_button;
  GtkWidget *hscale;
  GtkObject *adj;

  modebar = gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL,
			     GTK_TOOLBAR_ICONS);
  gtk_toolbar_set_space_size (GTK_TOOLBAR (modebar), 2);
  p_button = gnome_pixmap_new_from_file (gnome_pixmap_file ("gnome-iconedit/gridlines.png"));
  gridlines = gtk_toggle_button_new ();
  gtk_container_add (GTK_CONTAINER (gridlines), p_button);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (gridlines), TRUE);
  gtk_signal_connect (GTK_OBJECT (gridlines), "toggled",
		      GTK_SIGNAL_FUNC (toggle_grid), app);

  p_button = gnome_pixmap_new_from_file (gnome_pixmap_file ("gnome-iconedit/cheques.png"));
  chequers = gtk_toggle_button_new ();
  gtk_container_add (GTK_CONTAINER (chequers), p_button);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (chequers), TRUE);
  gtk_signal_connect (GTK_OBJECT (chequers), "toggled",
		      GTK_SIGNAL_FUNC (toggle_cheques), app);

  adj = gtk_adjustment_new (255.0, 0.0, 255.0, 1.0, 10.0, 0.0);
  gtk_signal_connect (GTK_OBJECT (adj), "value-changed",
		      GTK_SIGNAL_FUNC (grid_value_changed_cb), app);

  hscale = gtk_hscale_new (GTK_ADJUSTMENT (adj));
  gtk_scale_set_digits (GTK_SCALE (hscale), 0);
  gtk_scale_set_value_pos (GTK_SCALE (hscale), GTK_POS_LEFT);

  gtk_toolbar_append_widget (GTK_TOOLBAR (modebar),
			     chequers, _("Toggle the chequerboard"), NULL);
  gtk_toolbar_append_space (GTK_TOOLBAR (modebar));
  gtk_toolbar_append_widget (GTK_TOOLBAR (modebar),
			     gridlines, _("Toggle the gridlines"), NULL);
  gtk_toolbar_append_space (GTK_TOOLBAR (modebar));
  gtk_toolbar_append_widget (GTK_TOOLBAR (modebar),
			     hscale, _("The alpha level of the gridlines"), NULL);
  return modebar;
}

static GtkWidget*
ie_create_mode_menu (IEView *app)
{
  GtkWidget *modemenu;
  int i;

  modemenu = gtk_menu_new ();
  for (i = 0; i < IEM_END; i++)
    {
      GtkWidget *item;

      item = gtk_menu_item_new_with_label (_(ie_paint_mode[i]));
      gtk_object_set_data (GTK_OBJECT (item), "mode", GINT_TO_POINTER (i));
      gtk_signal_connect (GTK_OBJECT (item), "activate",
			  GTK_SIGNAL_FUNC (mode_changed_cb), app);
      gtk_widget_show (item);
      gtk_menu_append (GTK_MENU (modemenu), item);
    }
  return modemenu;
}
      
static GtkWidget*
ie_create_mode_bar (IEView *app)
{
  GtkWidget *modebar;
  GtkWidget *label, *modemenu;

  modebar = gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL,
			     GTK_TOOLBAR_ICONS);
  label = gtk_label_new (_("Paint Mode"));
  gtk_toolbar_append_widget (GTK_TOOLBAR (modebar),
			     label,
			     NULL, NULL);

  modemenu = gtk_option_menu_new ();
  gtk_option_menu_set_menu (GTK_OPTION_MENU (modemenu), 
			    ie_create_mode_menu (app));
  gtk_toolbar_append_widget (GTK_TOOLBAR (modebar),
			     modemenu,
			     _("The paint mode"),
			     NULL);
  gtk_widget_show_all (modebar);
  return modebar;
}

static void
zoom_changed (GtkAdjustment *adj,
	      IEView *view)
{
  int ppu;

  ppu = (int)(adj->value / 1);
  if (ppu == 0)
    ppu = 1;

  if (ppu != GNOME_CANVAS (view->canvas)->pixels_per_unit)
    gnome_canvas_set_pixels_per_unit (GNOME_CANVAS (view->canvas), ppu);
}

static GtkWidget*
ie_create_zoom_bar (IEView *app)
{
  GtkWidget *zoombar;
  GtkWidget *label, *zoomscale;
  GtkObject *adj;

  zoombar = gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL,
			     GTK_TOOLBAR_ICONS);
  gtk_toolbar_set_space_size (GTK_TOOLBAR (zoombar), 2);
  label = gtk_label_new (_("Zoom level 1:"));
  gtk_toolbar_append_widget (GTK_TOOLBAR (zoombar),
			     label, NULL, NULL);
  gtk_toolbar_append_space (GTK_TOOLBAR (zoombar));

  adj = gtk_adjustment_new (10.0, 1.0, 100.0, 1.0, 10.0, 1.0);
  gtk_signal_connect (adj, "value-changed",
		      GTK_SIGNAL_FUNC (zoom_changed), app);

  zoomscale = gtk_hscale_new (GTK_ADJUSTMENT (adj));
  gtk_scale_set_digits (GTK_SCALE (zoomscale), 0);
  gtk_scale_set_value_pos (GTK_SCALE (zoomscale), GTK_POS_LEFT);

  gtk_toolbar_append_widget (GTK_TOOLBAR (zoombar),
			     zoomscale,
			     _("The grid zoom level"),
			     NULL);
  gtk_widget_show_all (zoombar);
  return zoombar;
}

typedef struct 
{
  IEView *app; /* The app this is associated with */
  GtkWidget *functions[IET_END]; /* The pointers to the widgets */
  GtkWidget *on; /* Pointer into functions[] */
} IEFunctions;

/* Keep this in sync with IETool */
static gchar *pixmaps[IET_END] =
{
  "gnome-iconedit/selector.png",
  "gnome-iconedit/paint.png",
  "gnome-iconedit/fill.png",
  "gnome-iconedit/dropper.png",
  "gnome-iconedit/roller.png",
  "gnome-iconedit/empty-square.png",
  "gnome-iconedit/empty-circle.png",
  "gnome-iconedit/filled-square.png",
  "gnome-iconedit/filled-circle.png",
  "gnome-iconedit/line.png"
};

static gchar *function_names[IET_END] =
{
  N_("Select pixels"),
  N_("Paint pixels"),
  N_("Flood fill"),
  N_("Eyedropper (pick color from image)"),
  N_("Brush mode"),
  N_("Draw unfilled rectangles"),
  N_("Draw unfilled circles"),
  N_("Draw filled rectangles"),
  N_("Draw filled circles"),
  N_("Draw straight lines")
};

static void
function_toggled_cb (GtkToggleButton *toggle,
		     IEFunctions *fun)
{
  int i;

  if (!gtk_toggle_button_get_active (toggle))
    {
      if (fun->on == (GtkWidget*) toggle)
	gtk_toggle_button_set_active (toggle, TRUE);
      
      return;
    }

  /* Hack to make toggle buttons work like radiobuttons */
  for (i = IET_POINT; i < IET_END; i++)
    {
      if (fun->functions[i] != (GtkWidget*) toggle &&
	  gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (fun->functions[i])))
	{
	  GtkWidget *tmp = fun->on;
	  fun->on = NULL;
	  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (fun->functions[i]), 
					FALSE);
	  fun->on = tmp;
	}
      else if (fun->functions[i] == (GtkWidget*) toggle)
	{
  	  ie_app_set_tool (GTK_WIDGET (fun->app), i); 
	  fun->on = fun->functions[i];
	}
    }
}
  
static GtkWidget*
ie_create_function_bar (IEView *view)
{
  GtkWidget *functionbar;
  GtkWidget *p_button;
  IEFunctions *fun;
  int i;

  fun = g_new (IEFunctions, 1);
  fun->app = view;

  functionbar = gtk_toolbar_new (GTK_ORIENTATION_VERTICAL,
				 GTK_TOOLBAR_ICONS);
  for (i = IET_POINT; i < IET_END; i++)
    {
      fun->functions[i] = gtk_toggle_button_new();
      p_button = gnome_pixmap_new_from_file (gnome_pixmap_file (pixmaps[i]));
      gtk_container_add (GTK_CONTAINER (fun->functions[i]), p_button);
      if (i == IET_FG)
	{
	  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (fun->functions[i]), 
					TRUE);
	  fun->on = fun->functions[i];
	}
      gtk_toolbar_append_widget (GTK_TOOLBAR (functionbar), 
				 fun->functions[i], 
				 _(function_names[i]), NULL);
      gtk_signal_connect_full (GTK_OBJECT (fun->functions[i]), "toggled",
			       GTK_SIGNAL_FUNC (function_toggled_cb), NULL,
			       fun, g_free, FALSE, FALSE);
    }

  return functionbar;
}

void 
ie_install_menus_and_toolbar(IEView *view)
{
  AppMenus *am;
  GtkWidget *minitoolbar;
  GtkWidget *mode, *zoom, *gridbar;
  
  gnome_app_create_toolbar_with_data(GNOME_APP(view), toolbar, view);
  gnome_app_create_menus_with_data(GNOME_APP(view), menu, view);
  /*  gnome_app_install_menu_hints(GNOME_APP(view), menu); */
  ie_status_set_menu_hints (IE_STATUS (GNOME_APP (view)->statusbar), menu, 0); 

  am = g_new (AppMenus, 1);
  view->menus = am;

  /* Create the mode toolbar */
  mode = ie_create_mode_bar (view);
  gnome_app_add_docked (GNOME_APP (view), mode,
			"Mode-Toolbar",
			GNOME_DOCK_ITEM_BEH_NEVER_VERTICAL,
			GNOME_DOCK_TOP, 2, 0, 0);
  gtk_widget_show_all (mode);
  
  zoom = ie_create_zoom_bar (view);
  gnome_app_add_docked (GNOME_APP (view), zoom,
			"Zoom-Toolbar",
			GNOME_DOCK_ITEM_BEH_NEVER_VERTICAL,
			GNOME_DOCK_TOP, 2, 1, 0);
  gtk_widget_show_all (zoom);

  gridbar = ie_create_grid_bar (view);
  gnome_app_add_docked (GNOME_APP (view), gridbar,
			"Grid-Toolbar",
			GNOME_DOCK_ITEM_BEH_NEVER_VERTICAL,
			GNOME_DOCK_TOP, 2, 2, 0);
  gtk_widget_show_all (gridbar);

  /* Create the mini toolbar */
  minitoolbar = ie_create_function_bar (view);
  /* Add it to the dock. */
  gnome_app_add_docked (GNOME_APP (view), minitoolbar,
			"Function-Toolbar",
			GNOME_DOCK_ITEM_BEH_NORMAL,
			GNOME_DOCK_LEFT, 0, 0, 0);
  gtk_widget_show_all (minitoolbar);


  /* Copy menu/toolbar widgets so they can be manipulated later */
  am->undo = edit_menu[EDIT_UNDO].widget;
  am->redo = edit_menu[EDIT_REDO].widget;
  am->paste = edit_menu[EDIT_PASTE].widget;
  am->paste_into = edit_menu[EDIT_PASTE_INTO].widget;
  am->copy = edit_menu[EDIT_COPY].widget;
  am->cut = edit_menu[EDIT_CUT].widget;

  am->b_paste = toolbar[TOOLBAR_PASTE].widget;
  am->b_copy = toolbar[TOOLBAR_COPY].widget;
  am->b_cut = toolbar[TOOLBAR_CUT].widget;
  am->b_undo = toolbar[TOOLBAR_UNDO].widget;
  am->b_redo = toolbar[TOOLBAR_REDO].widget;

  if (gtk_paste_buffer_get_size (GTK_PASTE_BUFFER (paste_buffer)) == 0)
    {
      gtk_widget_set_sensitive (am->paste, FALSE);
      gtk_widget_set_sensitive (am->b_paste, FALSE);
      gtk_widget_set_sensitive (am->paste_into, FALSE);
    }

  gtk_widget_set_sensitive (am->undo, FALSE);
  gtk_widget_set_sensitive (am->redo, FALSE);
  gtk_widget_set_sensitive (am->cut, FALSE);
  gtk_widget_set_sensitive (am->copy, FALSE);
  gtk_widget_set_sensitive (am->b_cut, FALSE);
  gtk_widget_set_sensitive (am->b_copy, FALSE);
  gtk_widget_set_sensitive (am->b_undo, FALSE);
  gtk_widget_set_sensitive (am->b_redo, FALSE);
}

static void 
nothing_cb(GtkWidget* widget, gpointer data)
{
  GtkWidget* dialog;
  GtkWidget* app;
  
  app = (GtkWidget*) data;

  dialog = gnome_ok_dialog_parented(_("This does nothing; it is not yet implemented."),  GTK_WINDOW(app));
}

static void
new_app_ok_cb (GtkButton *button,
	       GnomeDialog *new_icon)
{
  GtkSpinButton *w_size, *h_size;
  gint w, h;

  w_size = gtk_object_get_data (GTK_OBJECT (new_icon), "new-width");
  h_size = gtk_object_get_data (GTK_OBJECT (new_icon), "new-height");

  w = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (w_size));
  h = gtk_spin_button_get_value_as_int (GTK_SPIN_BUTTON (h_size));

  ie_app_new (w, h, NULL);
}

static void 
new_app_cb(GtkWidget* widget, gpointer data)
{
  GtkWidget* new_icon;
  GtkWidget* w_size;
  GtkWidget* h_size;
  GtkWidget* frame;
  GtkWidget* table;
  GtkWidget* label;
  GtkObject* w_adj;
  GtkObject* h_adj;

  new_icon = gnome_dialog_new (_("New icon"), 
			       GNOME_STOCK_BUTTON_OK,
			       GNOME_STOCK_BUTTON_CANCEL,
			       NULL);
  gnome_dialog_set_close (GNOME_DIALOG (new_icon), TRUE);
  gnome_dialog_button_connect (GNOME_DIALOG (new_icon), 0,
			       GTK_SIGNAL_FUNC (new_app_ok_cb), new_icon);
  gnome_dialog_set_parent (GNOME_DIALOG (new_icon), GTK_WINDOW (data));
  
  frame = gtk_frame_new (_("New icon size:"));
  gtk_container_set_border_width (GTK_CONTAINER (frame), 2);

  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (new_icon)->vbox), frame, TRUE,
		      TRUE, 0);
  table = gtk_table_new (2, 2, FALSE);
  gtk_container_set_border_width (GTK_CONTAINER (table), 2);

  w_adj = gtk_adjustment_new (48.0, 0.0, 1000.0, 1.0, 10.0, 10.0);
  h_adj = gtk_adjustment_new (48.0, 0.0, 1000.0, 1.0, 10.0, 10.0);

  w_size = gtk_spin_button_new (GTK_ADJUSTMENT (w_adj), 1.0, 0);
  h_size = gtk_spin_button_new (GTK_ADJUSTMENT (h_adj), 1.0, 0);

  gtk_object_set_data (GTK_OBJECT (new_icon), "new-width", w_size);
  gtk_object_set_data (GTK_OBJECT (new_icon), "new-height", h_size);

  label = gtk_label_new (_("New icon's width"));
  gtk_table_attach (GTK_TABLE (table), label,
		    0, 1,
		    0, 1,
		    0, 0,
		    GNOME_PAD, GNOME_PAD);
  gtk_table_attach (GTK_TABLE (table), w_size,
		    1, 2,
		    0, 1,
		    0, 0,
		    GNOME_PAD, GNOME_PAD);

  label = gtk_label_new (_("New icon's height"));
  gtk_table_attach (GTK_TABLE (table), label,
		    0, 1,
		    1, 2,
		    0, 0,
		    GNOME_PAD, GNOME_PAD);
  gtk_table_attach (GTK_TABLE (table), h_size,
		    1, 2,
		    1, 2,
		    0, 0,
		    GNOME_PAD, GNOME_PAD);

  gtk_container_add (GTK_CONTAINER (frame), table);
  gtk_widget_show_all (new_icon);
}

static void 
close_cb(GtkWidget* widget, 
	 IEView *view)
{
  gtk_object_destroy (GTK_OBJECT (view));
}

static void 
exit_cb(GtkWidget* widget, gpointer data)
{
  gtk_main_quit();
}

static void 
about_cb(GtkWidget* widget, gpointer data)
{
  static GtkWidget* dialog = NULL;
  GtkWidget* app;

  app = (GtkWidget*) data;

  if (dialog != NULL) 
    {
      g_assert(GTK_WIDGET_REALIZED(dialog));
      gdk_window_show(dialog->window);
      gdk_window_raise(dialog->window);
    }
  else
    {        
      const gchar *authors[] = {
	"Iain Holmes <ih@csd.abdn.ac.uk>",
        "Havoc Pennington <hp@pobox.com>",
        NULL
      };

      gchar* logo = NULL; /*gnome_pixmap_file("gnome-hello-logo.png");*/

      dialog = gnome_about_new (_("Icon Edit"), VERSION,
                                "(C) 1999, 2000 Havoc Pennington",
                                authors,
                                _("Edits icons.\nHomepage: http://www.abdn.ac.uk/~u07ih/gnome-iconedit"),
                                logo);

      g_free(logo);

      gtk_signal_connect(GTK_OBJECT(dialog),
                         "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                         &dialog);

      gnome_dialog_set_parent(GNOME_DIALOG(dialog), GTK_WINDOW(app));

      gtk_widget_show(dialog);
    }
}

static void
mode_changed_cb (GtkWidget *widget,
		 IEView *view)
{
  gint mode;

  mode = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (widget), "mode"));
/*    ie_app_set_mode (view, mode); */
}

/*
 * Load/save
 */

static void
open_okay_cb (GtkWidget *button,
	      GtkWidget *fileentry)
{
  gchar* s;
  GdkPixbuf *pb;
  Iconedit* newapp;
  gint w, h;
  
  s = gnome_file_entry_get_full_path(GNOME_FILE_ENTRY(fileentry),
				     TRUE);
  
  if (s == NULL || *s == '\0')
    {
      gnome_error_dialog_parented(_("You must give a filename."),
				  GTK_WINDOW (gtk_widget_get_toplevel (fileentry)));
      g_free(s);
      gtk_widget_destroy (gtk_widget_get_toplevel (fileentry));
      return;
    }
  
  pb = gdk_pixbuf_new_from_file (s);
  
  if (pb == NULL)
    {
      gnome_error_dialog_parented(_("Unable to load that file. Perhaps it isn't an image file?"),
				  GTK_WINDOW (gtk_widget_get_toplevel (fileentry)));
      g_free(s);
      gtk_widget_destroy (gtk_widget_get_toplevel (fileentry));
      return;
    }
  
  w = gdk_pixbuf_get_width (pb);
  h = gdk_pixbuf_get_height (pb);
  
  newapp = ie_app_new(w, h, pb);
  ie_app_set_title (newapp, s);
  gdk_pixbuf_unref (pb);
}  

static void
open_destroy_cb (GtkButton *button,
		 GtkWidget *dialog)
{
  gtk_widget_destroy (dialog);
}

static void 
open_cb   (GtkWidget* widget, 
	   IEView *view)
{
  GtkWidget * dialog;
  GtkWidget* fileentry;
  
  dialog = gnome_dialog_new(_("Icon Editor Open"), 
                            GNOME_STOCK_BUTTON_OK,
                            GNOME_STOCK_BUTTON_CANCEL,
                            NULL);

  gnome_dialog_set_close(GNOME_DIALOG(dialog), TRUE);
  gnome_dialog_close_hides(GNOME_DIALOG(dialog), TRUE);
  gnome_dialog_set_parent(GNOME_DIALOG(dialog), GTK_WINDOW (view));

  fileentry = gnome_file_entry_new("iconedit:iconedit_loadsave_history",
                                   _("Icon Editor: Browse Files"));

  gnome_dialog_editable_enters(GNOME_DIALOG(dialog), 
                               GTK_EDITABLE(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(fileentry))));
  gnome_dialog_set_default(GNOME_DIALOG(dialog), GNOME_OK);

  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)->vbox),
                     fileentry,
                     TRUE, TRUE, GNOME_PAD);
  gnome_dialog_button_connect (GNOME_DIALOG (dialog), 0,
			       GTK_SIGNAL_FUNC (open_okay_cb), fileentry);
  gnome_dialog_button_connect (GNOME_DIALOG (dialog), 1,
			       GTK_SIGNAL_FUNC (open_destroy_cb), dialog);

  gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
		      GTK_SIGNAL_FUNC (open_destroy_cb), dialog);

  /* Make the box the default */
  gtk_widget_grab_focus (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (fileentry)));

  gtk_widget_show_all(dialog);
}

void 
save_cb (GtkWidget* widget, 
	 IEView *view)
{
  gchar* name;
  IEModel *model;

  name = gtk_object_get_data (GTK_OBJECT (view), "filename");
  model = view->model;

  if (name == NULL)
    {
      save_as_cb(widget, view);
      return;
    }

  ie_save_pixbuf (GNOME_APP (view), model->model, name);
}

static void 
save_as_cb(GtkWidget* widget, 
	   IEView *view)
{
  GtkWidget* dialog;
  GtkWidget* fileentry;
  int reply;

  dialog = gnome_dialog_new(_("Icon Editor Save As"), 
                            GNOME_STOCK_BUTTON_OK,
                            GNOME_STOCK_BUTTON_CANCEL,
                            NULL);

  gnome_dialog_set_close(GNOME_DIALOG(dialog), TRUE);
  gnome_dialog_close_hides(GNOME_DIALOG(dialog), TRUE);

  gnome_dialog_set_parent(GNOME_DIALOG(dialog), GTK_WINDOW (view));

  fileentry = gnome_file_entry_new("iconedit:iconedit_loadsave_history",
                                   _("Icon Editor: Browse Files For Save As"));
  
  gnome_dialog_editable_enters(GNOME_DIALOG(dialog), 
                               GTK_EDITABLE(gnome_file_entry_gtk_entry(GNOME_FILE_ENTRY(fileentry))));
  
  gnome_dialog_set_default(GNOME_DIALOG(dialog), GNOME_OK);
  
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dialog)->vbox),
                     fileentry,
                     TRUE, TRUE, GNOME_PAD);
  
  /* Make the box the default */
  gtk_widget_grab_focus (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (fileentry)));
  gtk_widget_show_all(dialog);
  
  reply = gnome_dialog_run(GNOME_DIALOG(dialog));
  
  if (reply == GNOME_OK)
    {      
      gchar* s = 
        gnome_file_entry_get_full_path(GNOME_FILE_ENTRY(fileentry),
                                       FALSE);

      if (s && s[0])
        {
          ie_app_set_title (view->app, s);
          save_cb(widget, view);
        }
      else
        {
          g_free(s);
          gnome_error_dialog_parented(_("You must give a filename"),
                                      GTK_WINDOW (view));
        }
    }
  
  gtk_widget_destroy(dialog);  
}

static void
revert_cb (GtkWidget *widget,
	   IEView *view)
{
  GdkPixbuf *pb;
  gchar *filename;

  filename = gtk_object_get_data (GTK_OBJECT (view), "filename");
  if (filename == NULL)
    return;

  pb = gdk_pixbuf_new_from_file (filename);
  
  if (pb == NULL)
    {
      gnome_error_dialog_parented(_("Unable to load that file.\nPerhaps it isn't an image file?"),
				  GTK_WINDOW (view));
      return;
    }
  
  ie_model_set_from_pixbuf (view->model, pb);      

  gdk_pixbuf_unref (pb);
}

/* GNOME-Print related functions */
#ifdef HAVE_GNOME_PRINT
static void
close_window (GtkObject *o,
	      gpointer data)
{
  gtk_main_quit ();
}

static void
do_print (IEView *view,
	  gboolean master,
	  gboolean do_preview)
{
  GnomePrinter *printer = NULL;
  GnomePrintContext *pc;
  GnomePrintMaster *gpm = NULL;
  GnomePrintDialog *gpd;
  double matrix[6];
  int copies, collate;

  art_affine_scale (matrix, 150, 150);
  matrix[4] = 0;
  matrix[5] = 0;

  if (master == 1) {
    gpd = GNOME_PRINT_DIALOG (gnome_print_dialog_new ("Print icons",
						      GNOME_PRINT_DIALOG_COPIES));
    gnome_print_dialog_set_copies (gpd, 1, FALSE);
    
    switch (gnome_dialog_run (GNOME_DIALOG (gpd))){
    case GNOME_PRINT_PRINT:
      do_preview = 0;
      break;
    case GNOME_PRINT_PREVIEW:
      do_preview = 1;
      break;
    case GNOME_PRINT_CANCEL:
      return;
    }
    
    gpm = gnome_print_master_new ();
    gnome_print_dialog_get_copies (gpd, &copies, &collate);
    gnome_print_master_set_copies (gpm, copies, collate);
    gnome_print_master_set_printer (gpm, gnome_print_dialog_get_printer (gpd));
    gnome_dialog_close (GNOME_DIALOG (gpd));
    
    pc = gnome_print_master_get_context (gpm);

    gnome_print_beginpage (pc, "Icon");
    gnome_print_gsave (pc);
    gnome_print_translate (pc, 100.0, 100.0);
    gnome_print_scale (pc, view->model->width, view->model->height);
    gnome_print_rgbaimage (pc,
			   gdk_pixbuf_get_pixels (view->model->model),
			   view->model->width,
			   view->model->height,
			   gdk_pixbuf_get_rowstride (view->model->model));
    gnome_print_grestore (pc);
    gnome_print_showpage (pc);
  } else {
    if (do_preview) {
      GtkWidget *toplevel, *canvas, *sw;

      gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
      gtk_widget_set_default_visual (gdk_rgb_get_visual ());
      
      toplevel = gtk_window_new (GTK_WINDOW_TOPLEVEL);
      gtk_widget_set_usize (toplevel, 700, 700);
      sw = gtk_scrolled_window_new (NULL, NULL);
      canvas = gnome_canvas_new_aa ();
      gtk_container_add (GTK_CONTAINER (toplevel), sw);
      gtk_container_add (GTK_CONTAINER (sw), canvas);
      
      pc = gnome_print_preview_new (GNOME_CANVAS (canvas), "US-Letter");
      
      gtk_widget_show_all (toplevel);
      
    } else {
      printer = gnome_printer_dialog_new_modal ();
      
      if (!printer)
	return;
      
      pc = gnome_print_context_new_with_paper_size (printer, "US-Letter");
    }
  }

  if (master) {
    gnome_print_master_close (gpm);
    if (do_preview) {
      GnomePrintMasterPreview *pmp;
      pmp = gnome_print_master_preview_new (gpm, "GNOME-Iconedit Print Preview");
      gtk_signal_connect (GTK_OBJECT (pmp), "destroy", 
			 GTK_SIGNAL_FUNC (close_window), NULL);
      gtk_widget_show (GTK_WIDGET (pmp));
      gtk_main ();
    } else {
      gnome_print_master_print (gpm);
    }
    
  } else {
    gnome_print_context_close (pc);
    
    if (do_preview)
      gtk_main ();
    
    if (printer)
      gtk_object_unref (GTK_OBJECT (printer));
    gtk_object_unref (GTK_OBJECT (pc));
  }
}

static void
print_cb (GtkWidget *widget,
	  IEView *view)
{
  do_print (view, TRUE, FALSE);
}

#endif /* HAVE_GNOME_PRINT */
