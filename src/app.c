/* app.c
 *
 * Copyright (C) 1999 Havoc Pennington
 *               2000 Iain Holmes
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include "app.h"
#include "view.h"
#include "grid.h"
#include "menus.h"
#include "palette.h"
#include <gtkpalette.h>
#include <gtkpastebuffer.h>
#include <iestatus.h>
#include <ie-model.h>
#include <layers.h>

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <libgnomeui/gnome-window-icon.h>

/* The global paste buffer */
extern GtkObject *paste_buffer;

/* Keep a list of all open application windows */
static GSList* app_list = NULL;

void ie_app_set_tool(GtkWidget* view, 
		     IETool tool);
GdkPixbuf* ie_app_to_pixbuf(GtkWidget* app);
void ie_app_set_mode (GtkWidget *view,
		      IEMode mode);
void ie_app_set_from_pixbuf(Iconedit *app, 
			    GdkPixbuf *pb);
void ie_app_destroy (Iconedit *app);
void ie_app_view_destroyed (GtkWidget *view,
			    Iconedit *app);
void set_filename (IEView *view,
		   gchar *filename);

void
ie_app_destroy (Iconedit *app)
{
  /* Unref the model - should destroy it */
  ie_model_unref (app->model);

  /* Remove the app from the list */
  app_list = g_slist_remove (app_list, app);
  g_free (app);

  /* If there are no more apps, then quit */
  if (app_list == NULL)
    gtk_main_quit ();
}

void
ie_app_view_destroyed (GtkWidget *view,
		       Iconedit *app)
{
  /* Remove the view from the viewlist */
  app->views = g_list_remove (app->views, view);

  if (app->views == NULL) {
    /* All views are destroyed -- destroy model and app */
    ie_app_destroy (app);
  }
}
    
void
set_filename(IEView *view, 
	     gchar* filename)
{
  gchar *title;

  g_return_if_fail (filename != NULL);

  gtk_object_set_data_full (GTK_OBJECT (view), 
			    "filename", g_strdup (filename),
			    g_free);
  title = g_strdup_printf (_("GNOME Iconedit: %s"), filename);
  gtk_window_set_title (GTK_WINDOW (view), title);
  g_free (title);
}

void
ie_app_set_title (Iconedit *app,
		  char *filename)
{
  GList *views;

  for (views = app->views; views; views = views->next) 
    set_filename (views->data, filename);
}

Iconedit*
ie_app_new (guint width,
	    guint height,
	    GdkPixbuf *pb)
{
  Iconedit *app;
  GtkWidget *view;

  app = g_new (Iconedit, 1);

  app->model = IE_MODEL (ie_model_new (width, height, pb));
  ie_model_ref (app->model);
  view = ie_view_new (app->model);
  ((IEView *) view)->app = app;
  app->views = g_list_prepend (NULL, view);
  gtk_signal_connect (GTK_OBJECT (view), "destroy",
		      GTK_SIGNAL_FUNC (ie_app_view_destroyed), app);

  gtk_widget_show (view);
  gnome_window_icon_set_from_default (GTK_WINDOW (view));

  app_list = g_slist_prepend (app_list, app);
  return app;
}

void
ie_update_grid (GtkWidget *app)
{
}

static const char *tip[IET_END] = 
{
  N_("Select region mode."),
  N_("Paint brush mode."),
  N_("Flood fill mode."),
  N_("Eyedropper mode."),
  N_("Brush paster mode."),
  N_("Unfilled rectangle mode."),
  N_("Unfilled circle mode."),
  N_("Filled rectangle mode."),
  N_("Filled circle mode."),
  N_("Straight line mode.")
};

void       
ie_app_set_tool(GtkWidget* widget, 
		IETool tool)
{
  IEView *view = (IEView *) widget;
  Grid *grid = (Grid *)view->grid;
  view->tool = tool;

  ie_status_set_status(IE_STATUS(view->status), _(tip[tool]), 1);
  /* Kill any layers that exist */
  if (grid->layer) {
    /* Kill the grid */
    gtk_object_destroy (GTK_OBJECT (grid->layer));
    grid->layer = NULL;

    /* Kill off the sel */
    if (grid->sel) {
      gtk_object_destroy (GTK_OBJECT (grid->sel));
      grid->sel = NULL;
    }
  }

  switch (tool) {
  case IET_POINT:
  case IET_FG:
  case IET_FILL:
  case IET_EYEDROP:
  case IET_BRUSH:
    break;

  case IET_SQUARE:
  case IET_FILL_SQUARE:
  case IET_CIRCLE:
  case IET_FILL_CIRCLE:
  case IET_LINE:
    /* Generate new layer */
    ie_layer_new (view);
    break;

  default:
    break;
  }
}

void
ie_app_set_mode (GtkWidget *widget,
		 IEMode mode)
{
  IEView *view = (IEView *) widget;
  view->mode = mode;
}

void       
ie_app_set_from_pixbuf(Iconedit *app, 
		       GdkPixbuf *pb)
{
  ie_model_set_from_pixbuf (app->model, pb);
}

GdkPixbuf* 
ie_app_to_pixbuf(GtkWidget* app)
{
  return NULL;
}

void
ie_update_statusbar (GtkWidget *widget, 
		     gint x,
		     gint y)
{
  IEView *view = (IEView *) widget;
  IEModel *model;
  guchar *pixels, *pixel;
  gint rowstride;
  char *str;

  model = view->model;
  pixels = gdk_pixbuf_get_pixels (model->model);
  rowstride = gdk_pixbuf_get_rowstride (model->model);

  pixel = pixels + (y * rowstride) + (x * 4);
  str = g_strdup_printf (_("(%d,%d): %d,%d,%d,%d"), x, y, 
			 pixel[0], pixel[1], pixel[2], pixel[3]);
  ie_status_set_status (IE_STATUS (view->status), str, 0);
  g_free (str);
}
