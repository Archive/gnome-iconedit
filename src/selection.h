#ifndef __IE_SELECTION_H__
#define __IE_SELECTION_H__

typedef struct _IESelection IESelection;

struct _IESelection
{
  gint x, y;
  gint width, height;

  IEModel *model; /* Grid the selection belongs to. */
  GnomeCanvasItem *selitem;
};

#endif
