/*
  GtkBuffer - A generic object for handling buffers.
  Copyright (C) 1999 - Iain Holmes <ih@csd.abdn.ac.uk>
*/

#include <gtk/gtksignal.h>
#include "gtkbuffer.h"

enum {
	ITEM_ADDED,
	ITEM_REMOVED,
	LAST_SIGNAL
};

static void gtk_buffer_class_init (GtkBufferClass *klass);
static void gtk_buffer_init (GtkBuffer *buffer);
static void gtk_buffer_finalize (GtkObject *object);
void gtk_buffer_item_added (GtkBuffer *buffer);
void gtk_buffer_item_removed (GtkBuffer *buffer);

static guint buffer_signals[LAST_SIGNAL] = { 0 };

static GtkObject *parent_class = NULL;

GtkType
gtk_buffer_get_type (void)
{
	static GtkType buffer_type = 0;

	if (!buffer_type)
	{
		static const GtkTypeInfo buffer_info =
		{
			"GtkBuffer",
			sizeof (GtkBuffer),
			sizeof (GtkBufferClass),
			(GtkClassInitFunc) gtk_buffer_class_init,
			(GtkObjectInitFunc) gtk_buffer_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL,
		};

		buffer_type = gtk_type_unique (GTK_TYPE_DATA, 
					       &buffer_info);
	}

	return buffer_type;
}

static void
gtk_buffer_class_init (GtkBufferClass *klass)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass*) klass;

	parent_class = gtk_type_class (GTK_TYPE_OBJECT);

	buffer_signals[ITEM_ADDED] =
		gtk_signal_new ("item-added",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GtkBufferClass, 
						   item_added),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);
	buffer_signals[ITEM_REMOVED] = 
		gtk_signal_new ("item-removed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GtkBufferClass,
						   item_removed),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);

	gtk_object_class_add_signals (object_class, buffer_signals, 
				      LAST_SIGNAL);

	object_class->finalize = gtk_buffer_finalize;

	klass->item_added = NULL;
	klass->item_removed = NULL;
}

static void
gtk_buffer_finalize (GtkObject *object)
{
	GtkBuffer *buffer;
	g_return_if_fail (object != NULL);
	g_return_if_fail (GTK_IS_BUFFER (object));

	buffer = GTK_BUFFER (object);
	buffer->buffer_size = 0;

	GTK_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gtk_buffer_init (GtkBuffer *buffer)
{
	buffer->buffer_size = 0;
}

GtkObject*
gtk_buffer_new (void)
{
	return GTK_OBJECT (gtk_type_new (gtk_buffer_get_type ()));
}

gint
gtk_buffer_get_size (GtkBuffer *buffer)
{
	g_return_val_if_fail (buffer != NULL, 0);
	g_return_val_if_fail (GTK_IS_BUFFER (buffer), 0);
	
	return buffer->buffer_size;
}

void
gtk_buffer_item_added (GtkBuffer *buffer)
{
	++buffer->buffer_size;
	gtk_signal_emit (GTK_OBJECT (buffer), buffer_signals[ITEM_ADDED]);
}

void
gtk_buffer_item_removed (GtkBuffer *buffer)
{
	--buffer->buffer_size;
	gtk_signal_emit (GTK_OBJECT (buffer), buffer_signals[ITEM_REMOVED]);
}
