/*
  IconEditStatus: A super enhanced statusbar type thing for GNOME-Iconedit

  Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
*/

#include "iestatus.h"
#include <gtk/gtkentry.h>
#include <gtk/gtkmenuitem.h>
#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-app-helper.h>
#include <libgnomeui/gnome-uidefs.h>

static void ie_status_class_init (IEStatusClass *status);
static void ie_status_init (GtkObject *object);

static GtkContainerClass *parent_class;

GtkType
ie_status_get_type (void)
{
  static guint sstype = 0;

  if (!sstype)
    {
      static const GtkTypeInfo sstypeinfo = {
	"IEStatus",
	sizeof (IEStatus),
	sizeof (IEStatusClass),
	(GtkClassInitFunc)ie_status_class_init,
	(GtkObjectInitFunc)ie_status_init,
	NULL,
	NULL,
	(GtkClassInitFunc)NULL
      };

      sstype = gtk_type_unique (gtk_hbox_get_type (), &sstypeinfo);
    }

  return sstype;
}

static void
ie_status_class_init (IEStatusClass *status)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkContainerClass *container_class;
  
  object_class = (GtkObjectClass*) status;
  widget_class = (GtkWidgetClass*) status;
  container_class = (GtkContainerClass*) status;

  parent_class = gtk_type_class (gtk_hbox_get_type ());

/*   object_class->destroy = ie_status_destroy; */
}

static void
ie_status_init (GtkObject *object)
{
  IEStatus *status;

  status = IE_STATUS (object);
  status->status = NULL;
}

GtkWidget*
ie_status_new (int num_status,
	       gboolean has_progress)
{
  IEStatus *status;
  GtkBox *box;

  status = gtk_type_new (ie_status_get_type ());
  box = GTK_BOX (status);

  status->status = g_ptr_array_new ();

  for(;num_status > 0; --num_status)
    {
      GtkWidget *sbar;
      sbar = gtk_entry_new ();
      gtk_entry_set_editable (GTK_ENTRY (sbar), FALSE);
      g_ptr_array_add (status->status, sbar);
      gtk_widget_show (sbar);
      gtk_box_pack_start (box, sbar, TRUE, TRUE, 0);
    }

  if (has_progress)
    {
      status->progress = gtk_progress_bar_new ();
      gtk_box_pack_start (box, status->progress, FALSE, FALSE, 0);
      gtk_widget_show (status->progress);
    }

  return GTK_WIDGET (status);
}

void
ie_status_set_status (IEStatus *status,
		      char *string,
		      int number)
{
  GtkWidget *widget;

  widget = status->status->pdata[number];
  gtk_entry_set_text (GTK_ENTRY (widget), string);
}

/* Copied from gnome-libs */
static void
put_hint_in_iestatus(GtkWidget* menuitem, gpointer data)
{
  gchar* hint = gtk_object_get_data (GTK_OBJECT(menuitem),
                                     "apphelper_statusbar_hint");
  GtkWidget* bar = data;

  g_return_if_fail (hint != NULL);
  g_return_if_fail (bar != NULL);
  g_return_if_fail (IE_IS_STATUS (bar));

  ie_status_set_status (IE_STATUS (bar), hint, 0);
}

/* Callback to remove hint when the menu item is deactivated.
 */
static void
remove_hint_from_iestatus(GtkWidget* menuitem, gpointer data)
{
  GtkWidget* bar = data;

  g_return_if_fail (bar != NULL);
  g_return_if_fail (IE_IS_STATUS (bar));

  ie_status_set_status (IE_STATUS (bar), "", 0);
}

/* Install a hint for a menu item
 */
static void
install_menuitem_hint_to_statusbar(GnomeUIInfo* uiinfo, 
				   IEStatus* bar,
				   int number)
{
  g_return_if_fail (uiinfo != NULL);
  g_return_if_fail (uiinfo->widget != NULL);
  g_return_if_fail (GTK_IS_MENU_ITEM(uiinfo->widget));
  
  if (uiinfo->hint)
    {
      gtk_object_set_data (GTK_OBJECT(uiinfo->widget),
                           "apphelper_statusbar_hint",
                           uiinfo->hint);

      gtk_signal_connect_while_alive (GTK_OBJECT (uiinfo->widget),
				      "select",
				      GTK_SIGNAL_FUNC(put_hint_in_iestatus),
				      bar, 
				      GTK_OBJECT (bar));

      gtk_signal_connect_while_alive (GTK_OBJECT (uiinfo->widget),
				      "deselect",
				      GTK_SIGNAL_FUNC(remove_hint_from_iestatus),
				      bar, 
				      GTK_OBJECT (bar));
    }
}

void
ie_status_set_menu_hints (IEStatus *status,
			  GnomeUIInfo *uiinfo,
			  int number)
{
  g_return_if_fail (status != NULL);
  g_return_if_fail (uiinfo != NULL);
  g_return_if_fail (IE_IS_STATUS (status));

  while (uiinfo->type != GNOME_APP_UI_ENDOFINFO)
    {
      switch (uiinfo->type)
	{
	case GNOME_APP_UI_SUBTREE:
	case GNOME_APP_UI_SUBTREE_STOCK:
	  ie_status_set_menu_hints (status, uiinfo->moreinfo, number);

	case GNOME_APP_UI_ITEM:
	case GNOME_APP_UI_TOGGLEITEM:
	case GNOME_APP_UI_SEPARATOR:
	  install_menuitem_hint_to_statusbar(uiinfo, status, number);
	  break;
	
	  /*	case GNOME_APP_UI_RADIOITEMS:
	  gnome_app_install_statusbar_menu_hints(status, uiinfo->moreinfo,
						 number);
						 break;*/

	default:
	  ;
	  break;
	}
      
      ++uiinfo;
    }
}
  

