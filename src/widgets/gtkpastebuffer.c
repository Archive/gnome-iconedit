/*
  GtkPasteBuffer - An object to handle cut/copy/paste.
  Copyright (C) 1999 - Iain Holmes <ih@Csd.abdn.ac.uk>

*/

#include <gtk/gtksignal.h>
#include "gtkpastebuffer.h"

static void gtk_paste_buffer_class_init (GtkPasteBufferClass *klass);
static void gtk_paste_buffer_init (GtkPasteBuffer *pbuffer);
static void gtk_paste_buffer_finalize (GtkObject *object);

static GtkBufferClass *parent_class = NULL;

GtkType
gtk_paste_buffer_get_type (void)
{
	static GtkType paste_buffer_type = 0;

	if (!paste_buffer_type)
	{
		static const GtkTypeInfo paste_buffer_info =
		{
			"GtkPasteBuffer",
			sizeof (GtkPasteBuffer),
			sizeof (GtkPasteBufferClass),
			(GtkClassInitFunc) gtk_paste_buffer_class_init,
			(GtkObjectInitFunc) gtk_paste_buffer_init,
			NULL,
			NULL,
			(GtkClassInitFunc)NULL,
		};

		paste_buffer_type = gtk_type_unique (GTK_TYPE_BUFFER, 
						     &paste_buffer_info);
	}

	return paste_buffer_type;
}

static void
gtk_paste_buffer_class_init (GtkPasteBufferClass *klass)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass*) klass;

	parent_class = gtk_type_class (GTK_TYPE_BUFFER);

	object_class->finalize = gtk_paste_buffer_finalize;

	parent_class->item_added = NULL;
	parent_class->item_removed = NULL;
}

static void
gtk_paste_buffer_finalize (GtkObject *object)
{
	GtkPasteBuffer *pbuffer;
	g_return_if_fail (object != NULL);
	g_return_if_fail (GTK_IS_PASTE_BUFFER (object));

	pbuffer = GTK_PASTE_BUFFER (object);
	if (pbuffer->buffer)
		g_list_free (pbuffer->buffer);

	pbuffer->selection = 0;
	pbuffer->buffer = NULL;

	GTK_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gtk_paste_buffer_init (GtkPasteBuffer *pbuffer)
{
	pbuffer->buffer = NULL;
}

GtkObject*
gtk_paste_buffer_new (void)
{
	return GTK_OBJECT (gtk_type_new (gtk_paste_buffer_get_type ()));
}

gint
gtk_paste_buffer_get_size (GtkPasteBuffer *pbuffer)
{
	g_return_val_if_fail (pbuffer != NULL, 0);
	g_return_val_if_fail (GTK_IS_PASTE_BUFFER (pbuffer), 0);

	return gtk_buffer_get_size (GTK_BUFFER (pbuffer));
}

void
gtk_paste_buffer_add (GtkPasteBuffer *pbuffer,
		      gpointer data)
{
	g_return_if_fail (pbuffer != NULL);
	g_return_if_fail (GTK_IS_PASTE_BUFFER (pbuffer));

	pbuffer->buffer = g_list_prepend (pbuffer->buffer, data);
	
	gtk_buffer_item_added (GTK_BUFFER (pbuffer));
}

gpointer
gtk_paste_buffer_get (GtkPasteBuffer *pbuffer,
		      gint position)
{
	GList *stack;
	int i;

	g_return_val_if_fail (pbuffer != NULL, NULL);
	g_return_val_if_fail (GTK_IS_PASTE_BUFFER (pbuffer), NULL);
	g_return_val_if_fail (pbuffer->buffer != NULL, NULL);
	g_return_val_if_fail (position >= 0, NULL);
	g_return_val_if_fail (position <= GTK_BUFFER (pbuffer)->buffer_size, NULL);
			   
	stack = pbuffer->buffer;
	for (i = 0; i < position; i++)
		stack = stack->next;

	return stack->data;
}

gpointer
gtk_paste_buffer_remove (GtkPasteBuffer *pbuffer,
			 gint position)
{
	GList *stack;
	gpointer data;
	int i;

	g_return_val_if_fail (pbuffer != NULL, NULL);
	g_return_val_if_fail (GTK_IS_PASTE_BUFFER (pbuffer), NULL);
	g_return_val_if_fail (pbuffer->buffer != NULL, NULL);
	g_return_val_if_fail (position >= 0, NULL);
	g_return_val_if_fail (position <= GTK_BUFFER (pbuffer)->buffer_size, NULL);
			   
	stack = pbuffer->buffer;
	for (i = 0; i < position; i++)
		stack = stack->next;

	pbuffer->buffer = g_list_remove_link (pbuffer->buffer, stack);
	data = stack->data;
	g_list_free_1 (stack);
	
	gtk_buffer_item_removed (GTK_BUFFER (pbuffer));
	return data;
}
