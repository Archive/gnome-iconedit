/*
 * ie-canvas-layer: A custom canvas item to act as a layer to draw on.
 *
 * Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <math.h>
#include "ie-canvas-layer.h"
#include <libgnomeui/gnome-canvas-util.h>

#include <libart_lgpl/art_rgb.h>

enum {
  ARG_0,
  ARG_X,
  ARG_Y,
  ARG_WIDTH,
  ARG_HEIGHT,
  ARG_SQUARE_SIZE,
  ARG_PIXBUF,
  ARG_ROWSTRIDE,
  ARG_GREYSCALE
};

static void ie_canvas_layer_class_init (IECanvasLayerClass *klass);
static void ie_canvas_layer_init (IECanvasLayer *layer);
static void ie_canvas_layer_destroy (GtkObject *object);
static void ie_canvas_layer_set_arg (GtkObject *object,
				     GtkArg *arg,
				     guint arg_id);
static void ie_canvas_layer_get_arg (GtkObject *object,
				     GtkArg *arg,
				     guint arg_id);
static void ie_canvas_layer_update (GnomeCanvasItem *item,
				    double *affine,
				    ArtSVP *clip_path,
				    gint flags);
static double ie_canvas_layer_point (GnomeCanvasItem *item,
				     double x, 
				     double y,
				     int cx,
				     int cy,
				     GnomeCanvasItem **actual_item);
static void ie_canvas_layer_render (GnomeCanvasItem *item,
				    GnomeCanvasBuf *buf);

static GnomeCanvasItemClass *parent_class;

GtkType
ie_canvas_layer_get_type (void)
{
  static guint layer_type = 0;
  
  if (!layer_type) {
    static const GtkTypeInfo layer_info = {
      "IECanvasLayer",
      sizeof (IECanvasLayer),
      sizeof (IECanvasLayerClass),
      (GtkClassInitFunc) ie_canvas_layer_class_init,
      (GtkObjectInitFunc) ie_canvas_layer_init,
      NULL,
      NULL,
      (GtkClassInitFunc) NULL
    };
    
    layer_type = gtk_type_unique (gnome_canvas_item_get_type (),
				  &layer_info);
  }
  
  return layer_type;
}

static void
ie_canvas_layer_class_init (IECanvasLayerClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  
  object_class = (GtkObjectClass*) klass;
  item_class = (GnomeCanvasItemClass*) klass;
  
  parent_class = gtk_type_class (gnome_canvas_item_get_type ());
  
  gtk_object_add_arg_type ("IECanvasLayer::x", GTK_TYPE_DOUBLE, 
			   GTK_ARG_READWRITE, ARG_X);
  gtk_object_add_arg_type ("IECanvasLayer::y", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_Y);
  gtk_object_add_arg_type ("IECanvasLayer::width", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_WIDTH);
  gtk_object_add_arg_type ("IECanvasLayer::height", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_HEIGHT);
  gtk_object_add_arg_type ("IECanvasLayer::square_size", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_SQUARE_SIZE);
  gtk_object_add_arg_type ("IECanvasLayer::pixbuf", GTK_TYPE_POINTER,
			   GTK_ARG_READWRITE, ARG_PIXBUF);
  gtk_object_add_arg_type ("IECanvasLayer::rowstride", GTK_TYPE_INT,
			   GTK_ARG_READWRITE, ARG_ROWSTRIDE);
  gtk_object_add_arg_type ("IECanvasLayer::greyscale", GTK_TYPE_BOOL,
			   GTK_ARG_READWRITE, ARG_GREYSCALE);

  object_class->destroy = ie_canvas_layer_destroy;
  object_class->set_arg = ie_canvas_layer_set_arg;
  object_class->get_arg = ie_canvas_layer_get_arg;
  
  item_class->translate = NULL;
  item_class->point = ie_canvas_layer_point;
  item_class->update = ie_canvas_layer_update;
  item_class->render = ie_canvas_layer_render;
}

static void
ie_canvas_layer_init (IECanvasLayer *layer)
{
  layer->x = 0.0;
  layer->y = 0.0;
  layer->width = 0.0;
  layer->height = 0.0;
  layer->square_size = 1.0;
  layer->pixbuf = NULL;
}

static void
ie_canvas_layer_destroy (GtkObject *object)
{
  IECanvasLayer *layer;

  g_return_if_fail (object != NULL);
  g_return_if_fail (IE_IS_CANVAS_LAYER (object));

  layer = IE_CANVAS_LAYER (object);
  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
ie_canvas_layer_set_arg (GtkObject *object,
			 GtkArg *arg,
			 guint arg_id)
{
  GnomeCanvasItem *item;
  IECanvasLayer *layer;;
  
  item = GNOME_CANVAS_ITEM (object);
  layer = IE_CANVAS_LAYER (object);
  
  switch (arg_id) {

  case ARG_X:
    layer->x = GTK_VALUE_DOUBLE (*arg);
    gnome_canvas_item_request_update (item);
    break;
    
  case ARG_Y:
    layer->y = GTK_VALUE_DOUBLE (*arg);
    gnome_canvas_item_request_update (item);
    break;
    
  case ARG_WIDTH:
    layer->width = GTK_VALUE_DOUBLE (*arg);
    gnome_canvas_item_request_update (item);
    break;
    
  case ARG_HEIGHT:
    layer->height = GTK_VALUE_DOUBLE (*arg);
    gnome_canvas_item_request_update (item);
    break;
    
  case ARG_SQUARE_SIZE:
    layer->square_size = GTK_VALUE_DOUBLE (*arg);
    gnome_canvas_item_request_update (item);
    break;
    
  case ARG_PIXBUF:
    layer->pixbuf = GTK_VALUE_POINTER (*arg);
    gnome_canvas_item_request_update (item);
    break;
    
  case ARG_ROWSTRIDE:
    layer->rowstride = GTK_VALUE_INT (*arg);
    gnome_canvas_item_request_update (item);
    break;
    
  case ARG_GREYSCALE:
    layer->greyscale = GTK_VALUE_BOOL (*arg);
    gnome_canvas_item_request_update (item);
    break;
    
  default:
    break;
  }
}

static void
ie_canvas_layer_get_arg (GtkObject *object,
			 GtkArg *arg,
			 guint arg_id)
{
  IECanvasLayer *layer;;
  
  layer = IE_CANVAS_LAYER (object);
  
  switch (arg_id) {
      
  case ARG_X:
    GTK_VALUE_DOUBLE (*arg) = layer->x;
    break;
    
  case ARG_Y:
    GTK_VALUE_DOUBLE (*arg) = layer->y;
    break;
    
  case ARG_WIDTH:
    GTK_VALUE_DOUBLE (*arg) = layer->width;
    break;
    
  case ARG_HEIGHT:
    GTK_VALUE_DOUBLE (*arg) = layer->height;
    break;
    
  case ARG_SQUARE_SIZE:
    GTK_VALUE_DOUBLE (*arg) = layer->square_size;
    break;
    
  case ARG_PIXBUF:
    GTK_VALUE_POINTER (*arg) = layer->pixbuf;
    break;
    
  case ARG_ROWSTRIDE:
    GTK_VALUE_INT (*arg) = layer->rowstride;
    break;
    
  case ARG_GREYSCALE:
    GTK_VALUE_BOOL (*arg) = layer->greyscale;
    break;
    
  default:
    arg->type = GTK_TYPE_INVALID;
    break;
  }
}

static void
ie_canvas_layer_update (GnomeCanvasItem *item,
			double affine[6],
			ArtSVP *clip_path,
			gint flags)
{
  IECanvasLayer *layer;
  double x1, x2, y1, y2;

  if (parent_class->update)
    (* parent_class->update) (item, affine, clip_path, flags);
  
  layer = IE_CANVAS_LAYER (item);

  x1 = layer->x;
  x2 = x1 + (layer->width * layer->square_size * item->canvas->pixels_per_unit);
  y1 = layer->y;
  y2 = y1 + (layer->height * layer->square_size *  item->canvas->pixels_per_unit);

  gnome_canvas_update_bbox (item, x1, y1, x2, y2);
}

static double
ie_canvas_layer_point (GnomeCanvasItem *item,
		       double x,
		       double y,
		       int cx,
		       int cy,
		       GnomeCanvasItem **actual_item)
{
  *actual_item = item;

  return 0.0;
}

static void
ie_canvas_layer_render (GnomeCanvasItem *item,
			GnomeCanvasBuf *buf)
{
  IECanvasLayer *layer;
  
  int width;
  int height;
  int x, y;
  int lw, lh; 
  guchar *dest, *pixels;
  int cs, rowstride;

  layer = IE_CANVAS_LAYER (item);
  g_return_if_fail (layer->pixbuf != NULL);

  width = buf->rect.x1 - buf->rect.x0;
  height = buf->rect.y1 - buf->rect.y0;

  cs = layer->square_size * item->canvas->pixels_per_unit;
  lw = layer->width * cs;
  lh = layer->height * cs;
  pixels = layer->pixbuf;
  rowstride = layer->rowstride;

  for (y = 0; y < height; y++) {
    int ly, cy;
    dest = buf->buf + (y * buf->buf_rowstride);
    
    /* ly is relative to the origin of the layer. */
    ly = buf->rect.y0 + y - (layer->y - 1);
    
    cy = (ly / cs) * rowstride;
    
    x = 0;
    while (x < width) {
      int lx;
      int ox, cx;
      
      /* lx is relative to the origin of the layer */
      lx = buf->rect.x0 + x - (layer->x);
      
      cx = lx / cs;
      ox = cs - (lx % cs);
      ox = MIN (ox, width - x);
      
      if (buf->is_bg) {
	guchar r, g, b;
	r = (buf->bg_color >> 16) & 0xff;
	g = (buf->bg_color >> 8) & 0xff;
	b = (buf->bg_color) & 0xff;
	art_rgb_fill_run (dest, r, g, b, ox);
      }
      
      if (lx >= 0 && lx <= lw &&
	  ly >= 0 && ly <= lh) {
	guchar *colour, a, red, green, blue;
	
	/* Set to colour depending on the lx & ly co-ords */
	colour = pixels + cy + (cx * 4);
	
	red = colour[0];
	green = colour[1];
	blue = colour[2];
	a = colour[3];
	
	art_rgb_run_alpha (dest, red, green, blue, a, ox);
      }
      
      dest += (ox * 3);
      x += ox;
    }
  }
  
  if (buf->is_bg) {
    buf->is_buf = TRUE;
    buf->is_bg = FALSE;
  }
}

void
ie_canvas_layer_update_cell (IECanvasLayer *layer,
			     double x, 
			     double y)
{
  ArtPoint i1, i2;
  ArtPoint c1, c2;
  double i2c[6], i2w[6], w2c[6];
  
  g_return_if_fail (layer != NULL);
  g_return_if_fail (IE_IS_CANVAS_LAYER (layer));

  i1.x = x * layer->square_size;
  i2.x = i1.x + layer->square_size;
  i1.y = y * layer->square_size;
  i2.y = i1.y + layer->square_size;

  gnome_canvas_item_i2w_affine (GNOME_CANVAS_ITEM (layer), i2w);
  gnome_canvas_w2c_affine (GNOME_CANVAS_ITEM (layer)->canvas, w2c);
  art_affine_multiply (i2c, i2w, w2c);

  art_affine_point (&c1, &i1, i2c);
  art_affine_point (&c2, &i2, i2c);

  gnome_canvas_request_redraw (GNOME_CANVAS (GNOME_CANVAS_ITEM (layer)->canvas),
			       (int)c1.x, (int)c1.y, 
			       (int)c2.x + 1, (int)c2.y + 1);
}

void
ie_canvas_layer_update_area (IECanvasLayer *layer,
			     double x, 
			     double y,
			     double width,
			     double height)
{
  ArtPoint i1, i2;
  ArtPoint c1, c2;
  double i2c[6];
  int ox, oy;

  g_return_if_fail (layer != NULL);
  g_return_if_fail (IE_IS_CANVAS_LAYER (layer));

  i1.x = x * layer->square_size;
  i2.x = i1.x + (layer->square_size * width);
  i1.y = y * layer->square_size;
  i2.y = i1.y + (layer->square_size * width);

  gnome_canvas_item_i2c_affine (GNOME_CANVAS_ITEM (layer), i2c); 
  
  art_affine_point (&c1, &i1, i2c);
  art_affine_point (&c2, &i2, i2c);

  gtk_layout_freeze (GTK_LAYOUT (GNOME_CANVAS_ITEM (layer)->canvas));
  gnome_canvas_request_redraw (GNOME_CANVAS (GNOME_CANVAS_ITEM (layer)->canvas),
			       (int)c1.x, 
			       (int)c1.y, 
			       (int)c2.x + 1, 
			       (int)c2.y + 1);
  gtk_layout_thaw (GTK_LAYOUT (GNOME_CANVAS_ITEM (layer)->canvas));
}
