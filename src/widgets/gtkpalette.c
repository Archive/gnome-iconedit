/* GtkPalette - A colour palette widget 
   Copyright (C)1999 - Iain Holmes <ih@csd.abdn.ac.uk>

*/

#include <config.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkmain.h>
#include "gtkpalette.h"

#include <libart_lgpl/art_misc.h>
#include <libart_lgpl/art_rgb.h>

enum {
	SELECTION_CHANGED,
	LAST_SIGNAL
};

#define DEFAULT_WIDTH 104
#define DEFAULT_HEIGHT 256

static guint palette_signals[LAST_SIGNAL] = { 0 };
static GtkDrawingAreaClass *parent_class = NULL;

static void gtk_palette_class_init (GtkPaletteClass *klass);
static void gtk_palette_init (GtkPalette *palette);
static void gtk_palette_destroy (GtkObject *obj);
static void gtk_palette_size_request (GtkWidget *widget,
				      GtkRequisition *requisition);
static void gtk_palette_size_allocate (GtkWidget *widget,
				       GtkAllocation *allocation);
static void gtk_palette_draw (GtkWidget *widget,
			      GdkRectangle *area);
static void gtk_palette_draw_focus (GtkWidget *widget,
				    gint x,
				    gint y,
				    gint alt_selection);
static gint gtk_palette_expose (GtkWidget *widget,
				GdkEventExpose *event);
static gint gtk_palette_button_press (GtkWidget *widget,
				      GdkEventButton *event);
static gint gtk_palette_button_release (GtkWidget *widget,
					GdkEventButton *event);
static gint gtk_palette_enter_notify (GtkWidget *widget,
				      GdkEventCrossing *event);
static gint gtk_palette_leave_notify (GtkWidget *widget,
				      GdkEventCrossing *event);
void gtk_palette_selection_changed (GtkPalette *palette);

GtkType
gtk_palette_get_type (void)
{
  static GtkType palette_type = 0;
  
  if (!palette_type) {
    static const GtkTypeInfo palette_info = {
      "GtkPalette",
      sizeof (GtkPalette),
      sizeof (GtkPaletteClass),
      (GtkClassInitFunc) gtk_palette_class_init,
      (GtkObjectInitFunc) gtk_palette_init,
      NULL,
      NULL,
      (GtkClassInitFunc) NULL
    };
    palette_type = gtk_type_unique (GTK_TYPE_DRAWING_AREA, &palette_info);
  }
  
  return palette_type;
}

static void
gtk_palette_class_init (GtkPaletteClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  
  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;
  
  parent_class = gtk_type_class (GTK_TYPE_DRAWING_AREA);
  
  palette_signals[SELECTION_CHANGED] = 
    gtk_signal_new ("selection_changed",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GtkPaletteClass, selection_changed),
		    gtk_marshal_NONE__INT,
		    GTK_TYPE_NONE, 1,
		    GTK_TYPE_INT);
  
  gtk_object_class_add_signals (object_class, palette_signals, LAST_SIGNAL);
  
  object_class->destroy = gtk_palette_destroy;
  
  widget_class->draw = gtk_palette_draw;
  widget_class->size_request = gtk_palette_size_request;
  widget_class->size_allocate = gtk_palette_size_allocate;
  widget_class->expose_event = gtk_palette_expose;
  widget_class->button_press_event = gtk_palette_button_press;
  widget_class->button_release_event = gtk_palette_button_release;
  widget_class->enter_notify_event = gtk_palette_enter_notify;
  widget_class->leave_notify_event = gtk_palette_leave_notify;
  
  klass->selection_changed = NULL;
}

static void
gtk_palette_init (GtkPalette *palette)
{
  GdkEventMask event_mask;
  
  GTK_WIDGET_SET_FLAGS (palette, GTK_CAN_FOCUS | GTK_RECEIVES_DEFAULT);
  GTK_WIDGET_UNSET_FLAGS (palette, GTK_NO_WINDOW);
  
  /* Add the extra events the GdkDrawingArea window doesn't have */
  event_mask = (gtk_widget_get_events (GTK_WIDGET (palette)) |
		GDK_BUTTON_PRESS_MASK |
		GDK_BUTTON_RELEASE_MASK |
		GDK_LEAVE_NOTIFY_MASK |
		GDK_ENTER_NOTIFY_MASK);
  gtk_widget_add_events (GTK_WIDGET (palette), event_mask);
  
  gdk_rgb_init ();
  palette->numcolours = 0;
  palette->selection = -1; /* No colour selected */
  palette->colours = NULL;
  palette->palette = NULL;
}

/**
 * gtk_palette_new:
 * @numcolours: The number of colours this palette can hold.
 *
 * Creates a new palette widget with @numcolours number of colours.
 *
 * Returns: A pointer to the newly allocated #GtkPalette.
 */
GtkWidget*
gtk_palette_new (gint numcolours)
{
  GtkPalette *palette;
  
  g_return_val_if_fail (numcolours > 0 && numcolours <= 256, NULL);
  palette = gtk_type_new (gtk_palette_get_type ());
  
  palette->numcolours = numcolours;
  
  palette->colours = (gchar*) g_malloc (sizeof (gchar) * numcolours * 3);
  palette->palette = (gchar*) g_malloc (sizeof (gchar) * DEFAULT_WIDTH * 
					DEFAULT_HEIGHT * 3);
  return GTK_WIDGET (palette);
}

static void
gtk_palette_destroy (GtkObject *object)
{
  GtkPalette *palette;
  
  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_PALETTE (object));
  
  palette = GTK_PALETTE (object);
  
  /* Free the cache */
  g_free (palette->colours);
  g_free (palette->palette);
  
  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

void
gtk_palette_selection_changed (GtkPalette *palette)
{
  g_return_if_fail (palette != NULL);
  g_return_if_fail (GTK_IS_PALETTE (palette));
  
  gtk_signal_emit (GTK_OBJECT (palette), palette_signals[SELECTION_CHANGED],
		   palette->selection);
}

/**
 * gtk_palette_get_palette:
 * @palette: A pointer to the #GtkPalette widget.
 *
 * Get the array of colours that are in the palette.
 * Returns: A pointer to the array. This is not a copy of the array,
 * so it should not be freed or changed
 */
gchar*
gtk_palette_get_palette (GtkPalette *palette)
{
  g_return_val_if_fail (palette != NULL, NULL);
  g_return_val_if_fail (GTK_IS_PALETTE (palette), NULL);
  
  return palette->colours;
}

static void
gtk_palette_recalc (GtkPalette *palette)
{
  gint x, y, by;
  gint block_w, block_h, w, h;
  gchar *pos, *t_col, *col;
  
  pos = palette->palette;
  
  h = palette->numcolours / 8;
  if (h == 0)
    h = 1;
  w = palette->numcolours / h;
  block_w = DEFAULT_WIDTH / w;
  block_h = DEFAULT_HEIGHT / h;
  
  /* Generate a larger grid of colours. */
  for (y = 0; y < h; y++) {
    col = palette->colours + (y * (w * 3));
    for (by = 0; by < block_h; by++) {
      t_col = col;

      for (x = 0; x < w; x++, t_col += 3) {
	art_rgb_fill_run (pos, t_col[0], t_col[1],
			  t_col[2], block_w);
	pos += (block_w * 3);
      }
    }
  }
  gtk_widget_queue_draw (GTK_WIDGET (palette));
}

/**
 * gtk_palette_set_palette:
 * @palette: A pointer to the #GtkPalette widget.
 * @numcolours: The number of colours in the palette.
 * @colours: An array of colours. 
 * 
 * Sets the colours of the #GtkPalette. The colours are copied into the
 * #GtkPalette structure, so the array can be freed after this call.
 */
void
gtk_palette_set_palette (GtkPalette *palette,
			 gint numcolours,
			 gchar *colours)
{
  gchar *pos;
  int i;
  
  g_return_if_fail (palette != NULL);
  g_return_if_fail (GTK_IS_PALETTE (palette));
  g_return_if_fail (palette->colours != NULL);
  g_return_if_fail (palette->palette != NULL);
  g_return_if_fail (numcolours <= palette->numcolours);
  g_return_if_fail (numcolours != 0);
  g_return_if_fail (colours != NULL);
  
  pos = palette->colours;
  for (i = 0; i < numcolours; i++, pos += 3, colours += 3) {
    art_rgb_fill_run (pos, colours[0], colours[1], colours[2], 1);
  }

  gtk_palette_recalc (palette);
}

/**
 * gtk_palette_set_palette_default:
 * @palette: The #GtkPalette to work on.
 *
 * Sets the palette of @palette to be the default.
 */
void
gtk_palette_set_palette_default (GtkPalette *palette)
{
  gchar *rgb, *pos;
  int i;
  
  g_return_if_fail (palette != NULL);
  g_return_if_fail (GTK_IS_PALETTE (palette));
  
  rgb = (gchar*) g_malloc (sizeof (gchar) * 3 * palette->numcolours);
  pos = rgb;

  for (i = 0; i  <256; i++, pos += 3) {
    art_rgb_fill_run (pos, 255 - i, (i * 32) % 255, i - 255, 1);
  }
  
  gtk_palette_set_palette (palette, 256, rgb);
  g_free (rgb);
}

/**
 * gtk_palette_get_colour:
 * @palette: The #GtkPalette widget.
 * @selection: The colour number.
 * @red: A pointer to store the red value in.
 * @green: A pointer to store the green value in.
 * @blue: A pointer to store the blue value in.
 *
 * Returns the colour indicated by @selection.
 */
void
gtk_palette_get_colour (GtkPalette *palette,
			gint selection,
			gint *r,
			gint *g,
			gint *b)
{
  gchar *pos;
  
  g_return_if_fail (palette != NULL);
  g_return_if_fail (GTK_IS_PALETTE (palette));
  g_return_if_fail (palette->colours != NULL);
  g_return_if_fail (selection >= 0);
  g_return_if_fail (selection <= palette->numcolours);
  g_return_if_fail (r && g && b);
  
  pos = palette->colours;
  *r = pos[selection * 3];
  *g = pos[selection * 3 + 1];
  *b = pos[selection * 3 + 2];
}
  
void
gtk_palette_set_colour (GtkPalette *palette,
			gint selection,
			gint r,
			gint g,
			gint b)
{
  gchar *colours;
  
  g_return_if_fail (palette != NULL);
  g_return_if_fail (GTK_IS_PALETTE (palette));
  g_return_if_fail (selection <= palette->numcolours);
  
  colours = palette->colours;
  colours += (selection * 3);
  art_rgb_fill_run (colours, r, g, b, 1);
  
  gtk_palette_recalc (palette);
  if (GTK_WIDGET_DRAWABLE (GTK_WIDGET (palette)))
    gtk_widget_queue_draw_area (GTK_WIDGET (palette),
				0, 0, 
				DEFAULT_WIDTH, DEFAULT_HEIGHT);
}

static void
gtk_palette_size_request (GtkWidget *widget,
			  GtkRequisition *requisition)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_PALETTE (widget));
  g_return_if_fail (requisition != NULL);
  
  requisition->width = DEFAULT_WIDTH;
  requisition->height = DEFAULT_HEIGHT;
}

static void
gtk_palette_size_allocate (GtkWidget *widget,
			   GtkAllocation *allocation)
{
  GtkPalette *palette;
  gint w, h;
  
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_PALETTE (widget));
  g_return_if_fail (allocation != NULL);
  
  widget->allocation = *allocation;
  
  palette = GTK_PALETTE (widget);
  h = palette->numcolours / 8;
  if (h == 0)
    h = 1;
  w = palette->numcolours / h;
  
  palette->w = w;
  palette->h = h;
  
  if (GTK_WIDGET_REALIZED (widget))
    gdk_window_move_resize (widget->window,
			    (widget->allocation.x +
			     ((widget->allocation.width -
			       DEFAULT_WIDTH) / 2)),
			    (widget->allocation.y +
			     ((widget->allocation.height -
			       DEFAULT_HEIGHT) / 2)),
			    DEFAULT_WIDTH,
			    DEFAULT_HEIGHT);
}

static void
gtk_palette_paint (GtkWidget *widget,
		   GdkRectangle *area)
{
  GtkPalette *palette;
  
  palette = GTK_PALETTE (widget);
  g_return_if_fail (palette->colours != NULL);
  
  if (GTK_WIDGET_DRAWABLE (widget)) {
    gdk_draw_rgb_image (widget->window, 
			widget->style->fg_gc[GTK_STATE_NORMAL],
			0, 0, DEFAULT_WIDTH,
			DEFAULT_HEIGHT,
			GDK_RGB_DITHER_MAX, palette->palette,
			DEFAULT_WIDTH * 3);
  }
  
  if (palette->selection != -1)
    gtk_palette_draw_focus (widget, -1, -1, palette->selection);
}

static void
gtk_palette_draw (GtkWidget *widget,
		  GdkRectangle *area)
{
  GtkPalette *palette;
  
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_PALETTE (widget));
  g_return_if_fail (area != NULL);
  
  if (GTK_WIDGET_DRAWABLE (widget)) {
    palette = GTK_PALETTE (widget);
    gtk_palette_paint (widget, area);
  }
}

static gint
gtk_palette_expose (GtkWidget *widget,
		    GdkEventExpose *event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_PALETTE (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  if (GTK_WIDGET_DRAWABLE (widget))
    gtk_palette_paint (widget, &event->area);
  return FALSE;
}

static void
gtk_palette_draw_focus (GtkWidget *widget,
			gint x,
			gint y,
			gint alt_selection)
{
  GtkPalette *palette;
  GdkGC *gc;
  gint old_selection, selection;
  gint block_w, block_h;
  gint px, py, ox, oy;
  gboolean redraw;
  
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_PALETTE (widget));
  
  palette = GTK_PALETTE (widget);
  
  redraw = (x == -1 && y == -1);
  
  block_w = DEFAULT_WIDTH / palette->w;
  block_h = DEFAULT_HEIGHT / palette->h;
  
  if(!redraw) {
    px = (x / block_w);
    py = (y / block_h);
    selection = px + (py * palette->w);
  } else {
    selection = alt_selection;
    px = selection % palette->w;
    py = selection / palette->w;
  }
  
  if (selection > palette->numcolours)
    return;
  
  old_selection = palette->selection;
  
  if (old_selection == selection && !redraw)
    return;
  
  gc = gdk_gc_new (widget->window);
  gdk_gc_set_line_attributes (gc, 1, GDK_LINE_DOUBLE_DASH,
			      GDK_CAP_BUTT, GDK_JOIN_ROUND);
  gdk_gc_set_function (gc, GDK_INVERT);
  
  if (old_selection != -1 && !redraw) {
    /* Erase the old focus */
    ox = old_selection % palette->w;
    oy = old_selection / palette->w;
    
    gdk_draw_rectangle (widget->window, gc, FALSE,
			ox * block_w, oy * block_h,
			block_w - 1, block_h - 1);
    
  }
  
  palette->selection = selection;
  
  gdk_draw_rectangle (widget->window, gc,
		      FALSE, px * block_w, py * block_h,
		      block_w - 1, block_h - 1);
  gdk_gc_unref (gc);
}

static gint
gtk_palette_button_press (GtkWidget *widget,
			  GdkEventButton *event)
{
  GtkPalette *palette;
  
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_PALETTE (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  if (event->type == GDK_BUTTON_PRESS) {
    palette = GTK_PALETTE (widget);
    if (!GTK_WIDGET_HAS_FOCUS (widget))
      gtk_widget_grab_focus (widget);
    
    if (event->button == 1) {
      gtk_grab_add (widget);
      if (palette->in_palette)
	palette->palette_clicked = TRUE;
    }
  }
  
  return TRUE;
}

static gint
gtk_palette_button_release (GtkWidget *widget,
			    GdkEventButton *event)
{
  GtkPalette *palette;
  
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_PALETTE (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  if (event->type == GDK_BUTTON_RELEASE) {
    palette = GTK_PALETTE (widget);
    if (event->button == 1) {
      gtk_grab_remove (widget);
      if (palette->in_palette &&
	  palette->palette_clicked) {
	gint x, y;
	x = event->x;
	y = event->y;
	
	gtk_palette_draw_focus (widget, x, y, -1);
	gtk_palette_selection_changed (palette);
      }
      palette->palette_clicked = FALSE;
    }
  }
  
  return TRUE;
}

static gint
gtk_palette_enter_notify (GtkWidget *widget,
			  GdkEventCrossing *event)
{
  GtkPalette *palette;
  GtkWidget *event_widget;
  
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_PALETTE (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  palette = GTK_PALETTE (widget);
  event_widget = (GtkWidget*) gtk_get_event_widget ((GdkEvent*) event);
  
  if ((event_widget == widget) &&
      (event->detail != GDK_NOTIFY_INFERIOR)) {
    palette->in_palette = TRUE;
  }
  
  return TRUE;
}

static gint
gtk_palette_leave_notify (GtkWidget *widget,
			  GdkEventCrossing *event)
{
  GtkPalette *palette;
  GtkWidget *event_widget;
  
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_PALETTE (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);
  
  palette = GTK_PALETTE (widget);
  event_widget = (GtkWidget*) gtk_get_event_widget ((GdkEvent*) event);
  
  if ((event_widget == widget) &&
      (event->detail != GDK_NOTIFY_INFERIOR)) {
    palette->in_palette = FALSE;
  }
  
  return FALSE;
}
