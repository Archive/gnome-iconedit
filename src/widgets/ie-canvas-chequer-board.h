/*
 * ie-canvas-grid: A custom canvas item to act as a grid.
 *
 * Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __IE_CANVAS_CHEQUER_BOARD_H__
#define __IE_CANVAS_CHEQUER_BOARD_H__

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>

BEGIN_GNOME_DECLS

#define IE_TYPE_CANVAS_CHEQUER_BOARD (ie_canvas_chequer_board_get_type ())
#define IE_CANVAS_CHEQUER_BOARD(obj) (GTK_CHECK_CAST ((obj), IE_TYPE_CANVAS_CHEQUER_BOARD, IECanvasChequerBoard))
#define IE_CANVAS_CHEQUER_BOARD_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), IE_TYPE_CANVAS_CHEQUER_BOARD, IECanvasChequerBoardClass))
#define IE_IS_CANVAS_CHEQUER_BOARD(obj) (GTK_CHECK_TYPE ((obj), IE_TYPE_CANVAS_CHEQUER_BOARD))
#define IE_IS_CANVAS_CHEQUER_BOARD_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), IE_TYPE_CANVAS_CHEQUER_BOARD))

typedef struct _IECanvasChequerBoard IECanvasChequerBoard;
typedef struct _IECanvasChequerBoardClass IECanvasChequerBoardClass;

struct _IECanvasChequerBoard
{
  GnomeCanvasItem item;
  
  double x, y;
  double width, height; /* width/height in canvas units */
  double cheque_size; /* Cheque size in pixels */
};

struct _IECanvasChequerBoardClass
{
  GnomeCanvasItemClass parent_class;
};

GtkType ie_canvas_chequer_board_get_type (void);

END_GNOME_DECLS

#endif /* __GNOME_CANVAS_GRID_H__ */
