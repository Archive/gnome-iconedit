/* GtkPalette - A colour palette widget 
   Copyright (C)1999 - Iain Holmes <ih@csd.abdn.ac.uk

*/

#ifndef __GTKPALETTE_H__
#define __GTKPALETTE_H__

#include <gtk/gtkdrawingarea.h>

#ifdef __cplusplus
extern "C" {
#endif
	
#define GTK_TYPE_PALETTE (gtk_palette_get_type ())
#define GTK_PALETTE(obj) (GTK_CHECK_CAST ((obj), GTK_TYPE_PALETTE, GtkPalette))
#define GTK_PALETTE_CLASS (klass) (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_PALETTE, GtkPaletteClass))
#define GTK_IS_PALETTE(obj) (GTK_CHECK_TYPE ((obj), GTK_TYPE_PALETTE))
#define GTK_IS_PALETTE_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass) GTK_TYPE_PALETTE))

typedef struct _GtkPalette GtkPalette;
typedef struct _GtkPaletteClass GtkPaletteClass;

struct _GtkPalette
{
	GtkDrawingArea area;

	gint in_palette : 1;
	gint palette_clicked : 1;

	gint numcolours; /* Max number of colours this can display */
	gint selection; /* Number of the colour currently selected */
	gint w, h; /* Height of the palette in colour block terms */

	gchar *colours; /* Array of the colours */
	gchar *palette; /* The colour palette */
};

struct _GtkPaletteClass
{
	GtkDrawingAreaClass parent_class;
	
	void (* selection_changed) (GtkPalette *palette,
				    gint colour);
};

GtkType gtk_palette_get_type (void);
GtkWidget *gtk_palette_new (gint numcolours);
void gtk_palette_set_palette (GtkPalette *palette,
			      gint numcolours,
			      gchar *array);
gchar *gtk_palette_get_palette (GtkPalette *palette);
void gtk_palette_set_palette_default (GtkPalette *palette);
void gtk_palette_get_colour (GtkPalette *palette,
			     gint selection,
			     gint *r,
			     gint *g,
			     gint *b);
void gtk_palette_set_colour (GtkPalette *palette,
			     gint selection,
			     gint r,
			     gint g,
			     gint b);

#ifdef __cplusplus
}
#endif

#endif /* __GTKPALETTE_H__ */
