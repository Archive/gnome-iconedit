/*
 * ie-canvas-layer: A custom canvas item to act as a layer to draw on.
 *
 * Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __IE_CANVAS_LAYER_H__
#define __IE_CANVAS_LAYER_H__

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>

BEGIN_GNOME_DECLS

#define IE_TYPE_CANVAS_LAYER (ie_canvas_layer_get_type ())
#define IE_CANVAS_LAYER(obj) (GTK_CHECK_CAST ((obj), IE_TYPE_CANVAS_LAYER, IECanvasLayer))
#define IE_CANVAS_LAYER_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), IE_TYPE_CANVAS_LAYER, IECanvasLayerClass))
#define IE_IS_CANVAS_LAYER(obj) (GTK_CHECK_TYPE ((obj), IE_TYPE_CANVAS_LAYER))
#define IE_IS_CANVAS_LAYER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), IE_TYPE_CANVAS_LAYER))

typedef struct _IECanvasLayer IECanvasLayer;
typedef struct _IECanvasLayerClass IECanvasLayerClass;

struct _IECanvasLayer
{
  GnomeCanvasItem item;
  
  double x, y;
  double width, height; /* width/height in cells */
  double square_size; /* Width/height of cell in canvas units */

  guchar *pixbuf;
  int rowstride;
  gboolean greyscale;
};

struct _IECanvasLayerClass
{
  GnomeCanvasItemClass parent_class;
};

GtkType ie_canvas_layer_get_type (void);
void ie_canvas_layer_update_cell (IECanvasLayer *layer,
				  double x, 
				  double y);
void ie_canvas_layer_update_area (IECanvasLayer *layer,
				  double x, 
				  double y,
				  double width,
				  double height);

END_GNOME_DECLS

#endif /* __GNOME_CANVAS_LAYER_H__ */
