#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include "ie-canvas-grid.h"
#include "ie-canvas-chequer-board.h"
#include "ie-canvas-layer.h"

gint
event_cb (GnomeCanvasItem *item,
	  GdkEvent *event,
	  gpointer data)
{
  static double x, y;
  double new_x, new_y;
  GdkCursor *fleur;
  static int dragging;
  double item_x, item_y;

  item_x = event->button.x;
  item_y = event->button.y;
  gnome_canvas_item_w2i (item->parent, &item_x, &item_y);

  switch (event->type)
    {
    case GDK_BUTTON_PRESS:
      {
	x = item_x;
	y = item_y;

	fleur = gdk_cursor_new (GDK_FLEUR);
	gnome_canvas_item_grab (item,
				GDK_POINTER_MOTION_MASK |
				GDK_BUTTON_RELEASE_MASK,
				fleur,
				event->button.time);
	gdk_cursor_destroy (fleur);
	dragging = TRUE;
      }
      break;
      
    case GDK_MOTION_NOTIFY:
      if (dragging && (event->motion.state & GDK_BUTTON1_MASK))
	{
	  new_x = item_x;
	  new_y = item_y;

	  gnome_canvas_item_move (item, new_x - x, new_y - y);
	  gnome_canvas_item_request_update (item);
	  x = new_x;
	  y = new_y;
	}
      break;

    case GDK_BUTTON_RELEASE:
      gnome_canvas_item_ungrab (item, event->button.time);
      dragging = FALSE;
      break;
    }

  return FALSE;
}

void
quit_cb (GtkWidget *widget,
	 gpointer data)
{
  gtk_main_quit ();
}

int
main (int argc, char **argv)
{
  GtkWidget *window, *canvas, *scroller;
  GtkObject *item;
  GdkPixbuf *pixbuf;

  gnome_init ("test", "1.0", argc, argv);
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title (GTK_WINDOW (window), "Grid Tester");
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (quit_cb), NULL);
  gtk_widget_push_visual (gdk_rgb_get_visual ());
  gtk_widget_push_colormap (gdk_rgb_get_cmap ());
  canvas = gnome_canvas_new_aa ();
  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();

  pixbuf = gdk_pixbuf_new_from_file ("/home/iain/gnome-iconedit/pixmaps/gnome-iconedit.png");
  scroller = gtk_scrolled_window_new (NULL, NULL);
  gtk_container_add (GTK_CONTAINER (window), scroller);
  gtk_container_add (GTK_CONTAINER (scroller), canvas);
  gnome_canvas_set_scroll_region (canvas, 0.0, 0.0, 1000.0, 1000.0);

/*   gnome_canvas_item_new (gnome_canvas_root (canvas), */
/* 			 ie_canvas_grid_get_type (), */
/* 			 "x", 40.0, */
/* 			 "y", 0.0, */
/* 			 "width", 10.0, */
/* 			 "height", 5.0, */
/* 			 "square_size", 17.0, */
/* 			 "thickness", 6.0, */
/* 			 "fill_color_rgba", 0x00FF008C, */
/* 			 NULL); */

  gnome_canvas_item_new (gnome_canvas_root (canvas),
			 ie_canvas_chequer_board_get_type (),
			 "x", 40.0,
			 "y", 40.0,
			 "width", 480.0,
			 "height", 480.0,
			 "square_size", 16.0,
			 NULL);

  gnome_canvas_item_new (gnome_canvas_root (canvas),
			 ie_canvas_layer_get_type (),
			 "x", 40.0,
			 "y", 40.0,
			 "width", 48.0,
			 "height", 48.0,
			 "square_size", 10.0,
			 "pixbuf", gdk_pixbuf_get_pixels (pixbuf),
			 "rowstride", gdk_pixbuf_get_rowstride (pixbuf),
			 NULL);

  item = gnome_canvas_item_new (gnome_canvas_root (canvas),
				gnome_canvas_rect_get_type (),
				"x1", 10.0,
				"y1", 10.0,
				"x2", 400.0,
				"y2", 400.0,
				"fill_color_rgba", 0xff8888AA,
				"outline_color_rgba", 0x000000FF,
				NULL);

  gnome_canvas_item_new (gnome_canvas_root (canvas),
			 ie_canvas_grid_get_type (),
			 "x", 40.0,
			 "y", 40.0,
			 "width", 48.0,
			 "height", 48.0,
			 "square_size", 10.0,
			 "thickness", 2.0,
			 "fill_color_rgba", 0x000000FF,
			 NULL);

  gtk_signal_connect (item, "event",
		      GTK_SIGNAL_FUNC (event_cb), NULL);

/*    gnome_canvas_set_pixels_per_unit (canvas, 5.0); */
  gtk_widget_show_all (window);
  gtk_main ();
  return 0;
}
