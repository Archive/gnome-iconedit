/*
 * ie-canvas-chequer-board: A custom canvas item to act as a chequerboard.
 *
 * Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <math.h>
#include "ie-canvas-chequer-board.h"
#include <libgnomeui/gnome-canvas-util.h>

#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_rgb_svp.h>
#include <libart_lgpl/art_svp_vpath_stroke.h>

enum {
  ARG_0,
  ARG_X,
  ARG_Y,
  ARG_WIDTH,
  ARG_HEIGHT,
  ARG_SQUARE_SIZE
};

static void ie_canvas_chequer_board_class_init (IECanvasChequerBoardClass *klass);
static void ie_canvas_chequer_board_init (IECanvasChequerBoard *chequer);
static void ie_canvas_chequer_board_destroy (GtkObject *object);
static void ie_canvas_chequer_board_set_arg (GtkObject *object,
					     GtkArg *arg,
					     guint arg_id);
static void ie_canvas_chequer_board_get_arg (GtkObject *object,
					     GtkArg *arg,
					     guint arg_id);
static void ie_canvas_chequer_board_update (GnomeCanvasItem *item,
					    double *affine,
					    ArtSVP *clip_path,
					    gint flags);
static double ie_canvas_chequer_board_point (GnomeCanvasItem *item,
					     double x, 
					     double y,
					     int cx,
					     int cy,
					     GnomeCanvasItem **actual_item);
static void ie_canvas_chequer_board_render (GnomeCanvasItem *item,
					    GnomeCanvasBuf *buf);

static GnomeCanvasItemClass *parent_class;

GtkType
ie_canvas_chequer_board_get_type (void)
{
  static guint cb_type = 0;
  
  if (!cb_type)
    {
      static const GtkTypeInfo cb_info = 
      {
	"IECanvasChequerBoard",
	sizeof (IECanvasChequerBoard),
	sizeof (IECanvasChequerBoardClass),
	(GtkClassInitFunc) ie_canvas_chequer_board_class_init,
	(GtkObjectInitFunc) ie_canvas_chequer_board_init,
	NULL,
	NULL,
	(GtkClassInitFunc) NULL
      };
      
      cb_type = gtk_type_unique (gnome_canvas_item_get_type (),
				 &cb_info);
    }
  
  return cb_type;
}

static void
ie_canvas_chequer_board_class_init (IECanvasChequerBoardClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  
  object_class = (GtkObjectClass*) klass;
  item_class = (GnomeCanvasItemClass*) klass;
  
  parent_class = gtk_type_class (gnome_canvas_item_get_type ());
  
  gtk_object_add_arg_type ("IECanvasChequerBoard::x", GTK_TYPE_DOUBLE, 
			   GTK_ARG_READWRITE, ARG_X);
  gtk_object_add_arg_type ("IECanvasChequerBoard::y", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_Y);
  gtk_object_add_arg_type ("IECanvasChequerBoard::width", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_WIDTH);
  gtk_object_add_arg_type ("IECanvasChequerBoard::height", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_HEIGHT);
  gtk_object_add_arg_type ("IECanvasChequerBoard::square_size", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_SQUARE_SIZE);

  object_class->destroy = ie_canvas_chequer_board_destroy;
  object_class->set_arg = ie_canvas_chequer_board_set_arg;
  object_class->get_arg = ie_canvas_chequer_board_get_arg;
  
  item_class->translate = NULL;
  /*	item_class->bounds = gnome_canvas_grid_bounds;*/
  item_class->point = ie_canvas_chequer_board_point;
  item_class->update = ie_canvas_chequer_board_update;
  item_class->render = ie_canvas_chequer_board_render;
}

static void
ie_canvas_chequer_board_init (IECanvasChequerBoard *grid)
{
  grid->x = 0.0;
  grid->y = 0.0;
  grid->width = 0.0;
  grid->height = 0.0;
  grid->cheque_size = 16.0;
}

static void
ie_canvas_chequer_board_destroy (GtkObject *object)
{
  g_return_if_fail (object != NULL);
  g_return_if_fail (IE_IS_CANVAS_CHEQUER_BOARD (object));
  
  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
ie_canvas_chequer_board_set_arg (GtkObject *object,
				 GtkArg *arg,
				 guint arg_id)
{
  GnomeCanvasItem *item;
  IECanvasChequerBoard *cboard;
  
  item = GNOME_CANVAS_ITEM (object);
  cboard = IE_CANVAS_CHEQUER_BOARD (object);
  
  switch (arg_id)
    {
    case ARG_X:
      cboard->x = GTK_VALUE_DOUBLE (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_Y:
      cboard->y = GTK_VALUE_DOUBLE (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_WIDTH:
      cboard->width = GTK_VALUE_DOUBLE (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_HEIGHT:
      cboard->height = GTK_VALUE_DOUBLE (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_SQUARE_SIZE:
      cboard->cheque_size = GTK_VALUE_DOUBLE (*arg);
      gnome_canvas_item_request_update (item);
      break;
    default:
      break;
    }
}

static void
ie_canvas_chequer_board_get_arg (GtkObject *object,
				 GtkArg *arg,
				 guint arg_id)
{
  IECanvasChequerBoard *cboard;
  
  cboard = IE_CANVAS_CHEQUER_BOARD (object);
  
  switch (arg_id)
    {
      
    case ARG_X:
      GTK_VALUE_DOUBLE (*arg) = cboard->x;
      break;
      
    case ARG_Y:
      GTK_VALUE_DOUBLE (*arg) = cboard->y;
      break;
      
    case ARG_WIDTH:
      GTK_VALUE_DOUBLE (*arg) = cboard->width;
      break;
      
    case ARG_HEIGHT:
      GTK_VALUE_DOUBLE (*arg) = cboard->height;
      break;
      
    case ARG_SQUARE_SIZE:
      GTK_VALUE_DOUBLE (*arg) = cboard->cheque_size;
      break;

    default:
      arg->type = GTK_TYPE_INVALID;
      break;
    }
}

static void
ie_canvas_chequer_board_update (GnomeCanvasItem *item,
				double affine[6],
				ArtSVP *clip_path,
				gint flags)
{
  IECanvasChequerBoard *cboard;
  double x1, x2, y1, y2;

  if (parent_class->update)
    (* parent_class->update) (item, affine, clip_path, flags);
  
  cboard = IE_CANVAS_CHEQUER_BOARD (item);

  x1 = cboard->x;;
  x2 = x1 + (cboard->width * item->canvas->pixels_per_unit);
  y1 = cboard->y;
  y2 = y1 + (cboard->height * item->canvas->pixels_per_unit);

  gnome_canvas_update_bbox (item, x1, y1, x2, y2);
}

static double
ie_canvas_chequer_board_point (GnomeCanvasItem *item,
			       double x,
			       double y,
			       int cx,
			       int cy,
			       GnomeCanvasItem **actual_item)
{
  *actual_item = item;

  return 0.0;
}

#define LIGHT 0xAA
#define DARK 0x88

static void
ie_canvas_chequer_board_render (GnomeCanvasItem *item,
				GnomeCanvasBuf *buf)
{
  IECanvasChequerBoard *cboard;
  
  int width = buf->rect.x1 - buf->rect.x0;
  int height = buf->rect.y1 - buf->rect.y0;
  int xlimit, ylimit;
  int x, y;
  guchar *bufptr;
  guchar r, g, b;
  int cs;

  cboard = IE_CANVAS_CHEQUER_BOARD (item);

  /* Values used as constant in the loops, so they don't have to be
     repeatedly calculated */
  cs = (int)cboard->cheque_size;
  r = (buf->bg_color >> 16) & 0xFF;
  g = (buf->bg_color >> 8) & 0xFF;
  b = buf->bg_color & 0xFF;
  xlimit = cboard->x + (cboard->width * item->canvas->pixels_per_unit);
  ylimit = cboard->y + (cboard->height * item->canvas->pixels_per_unit);

  for (y = 0; y < height; y++)
    {
      bufptr = buf->buf + (y * buf->buf_rowstride);
      for (x = 0; x < width; x++)
	{
	  int ix, iy;
	  int side;
	  char v;

	  ix = x + buf->rect.x0;
	  iy = y + buf->rect.y0;

	  side = (iy / cs) % 2;
	  if (ix >= cboard->x && iy >= cboard->y && 
	      ix <= xlimit && 
	      iy <= ylimit)
	    {
	      if ( (((ix / cs) % 2) + side) % 2 == 0)
		v = LIGHT;
	      else
		v = DARK;
	      
	      memset (bufptr, v, 3);
	    }	 
	  else
	    {
	      if (buf->is_bg)
		{
		  bufptr[0] = r;
		  bufptr[1] = g;
		  bufptr[2] = b;
		}
	    }
	  bufptr += 3;
	}
    }
  buf->is_buf = 1;
  buf->is_bg = 0;
}
