/* 
   IE-Model - A model object for handling MVC set ups.
   Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
*/

#ifndef __IE_MODEL_H__
#define __IE_MODEL_H__

#include <gtk/gtkdata.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
	
#define IE_TYPE_MODEL (ie_model_get_type ())
#define IE_MODEL(obj) (GTK_CHECK_CAST ((obj), IE_TYPE_MODEL, IEModel))
#define IE_MODEL_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), IE_TYPE_MODEL, IEModelClass))
#define IE_IS_MODEL(obj) (GTK_CHECK_TYPE ((obj), IE_TYPE_MODEL))
#define IE_IS_MODEL_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), IE_TYPE_MODEL))
	
typedef struct _IEModel IEModel;
typedef struct _IEModelClass IEModelClass;

  /* For the signal marshaller */
typedef void (*IESignal_NONE__INT_INT_INT_INT) (GtkObject *object,
						gint x1,
						gint y1,
						gint width,
						gint height,
						gpointer user_data);
struct _IEModel
{
  GtkData data;

  gboolean dirty; /* Dirty? */
  guint width, height; /* Width/height of data model */
  guint rowstride;

  guint ref_count; /* Ref count of the number of views attached to this model */
  GdkPixbuf *model; /* pixbuf of the icon being editted */
};

struct _IEModelClass
{
  GtkDataClass parent_class;

  void (*model_updated) (IEModel *model);
};

GtkType ie_model_get_type (void);
GtkObject* ie_model_new (guint width,
			 guint height,
			 GdkPixbuf *pb);
void ie_model_ref (IEModel *model);
void ie_model_unref (IEModel *model);

void ie_model_set_from_pixbuf (IEModel *model,
			       GdkPixbuf *pixbuf);
void ie_model_set_pixel (IEModel *model,
			 gint x,
			 gint y,
			 guchar red,
			 guchar green,
			 guchar blue,
			 guchar alpha);

void ie_model_model_updated (IEModel *model,
			     gint x,
			     gint y,
			     gint width,
			     gint height);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTKBUFFER_H__ */
