/*
  IEStatus: A super enhanced statusbar type thing

  Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
*/

#ifndef __GTK_SUPER_STATUS_BAR_H__
#define __GTK_SUPER_STATUS_BAR_H__

#include <libgnome/gnome-defs.h>
#include <gtk/gtkstatusbar.h>
#include <gtk/gtkprogressbar.h>
#include <gtk/gtkhbox.h>
#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-app-helper.h>

BEGIN_GNOME_DECLS

#define IE_TYPE_STATUS (ie_status_get_type ())
#define IE_STATUS(obj) (GTK_CHECK_CAST ((obj), IE_TYPE_STATUS, IEStatus))
#define IE_STATUS_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), IE_TYPE_STATUS, IEStatusClass))
#define IE_IS_STATUS(obj) (GTK_CHECK_TYPE ((obj), IE_TYPE_STATUS))
#define IE_IS_STATUS_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), IE_TYPE_STATUS))

typedef struct _IEStatus IEStatus;
typedef struct _IEStatusClass IEStatusClass;

struct _IEStatus
{
  GtkHBox appbar;

  GPtrArray *status;
  GtkWidget *progress;
};

struct _IEStatusClass
{
  GtkHBoxClass parent_class;
};

GtkType ie_status_get_type (void);
GtkWidget* ie_status_new (int numstatus, 
			  gboolean has_progress);
void ie_status_set_status (IEStatus *status,
			   char *string,
			   int number);
void ie_status_set_menu_hints (IEStatus *status,
			       GnomeUIInfo *uiinfo,
			       int number);
END_GNOME_DECLS

#endif 
