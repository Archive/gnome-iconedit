/*
  gnome-canvas-grid: A custom canvas item to act as a grid.

  Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
*/

#ifndef __GNOME_CANVAS_GRID_H__
#define __GNOME_CANVAS_GRID_H__

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>

#include <libart_lgpl/art_svp.h>

BEGIN_GNOME_DECLS

#define GNOME_TYPE_CANVAS_GRID (gnome_canvas_grid_get_type ())
#define GNOME_CANVAS_GRID(obj) (GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_GRID, GnomeCanvasGrid))
#define GNOME_CANVAS_GRID_KLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_GRID, GnomeCanvasGridClass))
#define GNOME_IS_CANVAS_GRID(obj) (GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_GRID))
#define GNOME_IS_CANVAS_GRID_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_GRID))

typedef struct _GnomeCanvasGrid GnomeCanvasGrid;
typedef struct _GnomeCanvasGridClass GnomeCanvasGridClass;

struct _GnomeCanvasGrid
{
  GnomeCanvasItem item;
  
  double x, y;
  double width, height; /* width/height in cells */
  double square_size;

  gint p_width, p_height; /* width/height in pixels */

  guchar *pixbuf;
  gint checkerboard : 1; /* Checkerboard on or off? */
  guint32 paper_colour; /* Colour of the paper */
  guint32 grid_colour; /*Colour of the grid */
};

struct _GnomeCanvasGridClass
{
  GnomeCanvasItemClass parent_class;
};

GtkType gnome_canvas_grid_get_type (void);
void gnome_canvas_grid_update_region (GnomeCanvasGrid *grid,
				      double x,
				      double y);
END_GNOME_DECLS

#endif /* __GNOME_CANVAS_GRID_H__ */
