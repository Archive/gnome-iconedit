/* 
   UndoBuffer - An object for handling undo/redo operations.
   Copyright (C) 1999 - Iain Holmes <ih@csd.abdn.ac.uk>
*/

#ifndef __GTKUNDOBUFFER_H__
#define __GTKUNDOBUFFER_H__

#include "gtkbuffer.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
	
#define GTK_TYPE_UNDO_BUFFER (gtk_undo_buffer_get_type ())
#define GTK_UNDO_BUFFER(obj) (GTK_CHECK_CAST ((obj), GTK_TYPE_UNDO_BUFFER, GtkUndoBuffer))
#define GTK_UNDO_BUFFER_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_UNDO_BUFFER, GtkUndoBufferClass))
#define GTK_IS_UNDO_BUFFER(obj) (GTK_CHECK_TYPE ((obj), GTK_TYPE_UNDO_BUFFER))
#define GTK_IS_UNDO_BUFFER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_UNDO_BUFFER))
	
typedef struct _GtkUndoBuffer GtkUndoBuffer;
typedef struct _GtkUndoBufferClass GtkUndoBufferClass;

struct _GtkUndoBuffer
{
  GtkBuffer parent_buffer;
  
  GNode *buffer;
  GNode *pointer;
};

struct _GtkUndoBufferClass
{
	GtkBufferClass parent_class;
};

GtkType gtk_undo_buffer_get_type (void);
GtkObject* gtk_undo_buffer_new (void);
void gtk_undo_buffer_add_item (GtkUndoBuffer *undo,
			       gpointer item);
void gtk_undo_buffer_start_branch (GtkUndoBuffer *undo,
				   gpointer item);
void gtk_undo_buffer_end_branch (GtkUndoBuffer *undo);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTKUNDOBUFFER_H__ */
