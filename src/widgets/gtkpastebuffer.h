/* GtkPasteBuffer - An object for handling cut/copy/paste operations.
   Copyright (C) 1999 - Iain Holmes <ih@csd.abdn.ac.uk>

*/

#ifndef __GTKPASTEBUFFER_H__
#define __GTKPASTEBUFFER_H__

#include "gtkbuffer.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
	
#define GTK_TYPE_PASTE_BUFFER (gtk_paste_buffer_get_type ())
#define GTK_PASTE_BUFFER(obj) (GTK_CHECK_CAST ((obj), GTK_TYPE_PASTE_BUFFER, GtkPasteBuffer))
#define GTK_PASTE_BUFFER_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_PASTE_BUFFER, GtkPasteBufferClass))
#define GTK_IS_PASTE_BUFFER(obj) (GTK_CHECK_TYPE ((obj), GTK_TYPE_PASTE_BUFFER))
#define GTK_IS_PASTE_BUFFER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_PASTE_BUFFER))
	
typedef struct _GtkPasteBuffer GtkPasteBuffer;
typedef struct _GtkPasteBufferClass GtkPasteBufferClass;

struct _GtkPasteBuffer
{
	GtkBuffer parent_buffer;
	
	gint selection;
	GList *buffer;
};

struct _GtkPasteBufferClass
{
	GtkBufferClass parent_class;
};

GtkType gtk_paste_buffer_get_type (void);
GtkObject* gtk_paste_buffer_new (void);
void gtk_paste_buffer_add (GtkPasteBuffer *pbuffer,
			   gpointer data);
gpointer gtk_paste_buffer_get (GtkPasteBuffer *pbuffer,
			       gint position);
gpointer gtk_paste_buffer_remove (GtkPasteBuffer *pbuffer,
				  gint position);
gint gtk_paste_buffer_get_size (GtkPasteBuffer *pbuffer);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTKPASTEBUFFER_H__ */
