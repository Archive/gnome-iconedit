/*
 * ie-canvas-grid: A custom canvas item to act as a grid with lines.
 *
 * Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
 * Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <math.h>
#include "ie-canvas-grid.h"
#include <libgnomeui/gnome-canvas-util.h>

#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_rgb_svp.h>
#include <libart_lgpl/art_svp_vpath_stroke.h>

enum {
  ARG_0,
  ARG_X,
  ARG_Y,
  ARG_WIDTH,
  ARG_HEIGHT,
  ARG_SQUARE_SIZE,
  ARG_THICKNESS,
  ARG_FILL_COLOUR_RGBA
};

#define SQUARE_SIZE 17.0
#define GRID_SPACING 3.0
#define CELL_SIZE (gint)(SQUARE_SIZE + GRID_SPACING)

static void ie_canvas_grid_class_init (IECanvasGridClass *klass);
static void ie_canvas_grid_init (IECanvasGrid *grid);
static void ie_canvas_grid_destroy (GtkObject *object);
static void ie_canvas_grid_set_arg (GtkObject *object,
				    GtkArg *arg,
				    guint arg_id);
static void ie_canvas_grid_get_arg (GtkObject *object,
				    GtkArg *arg,
				    guint arg_id);
static void ie_canvas_grid_update (GnomeCanvasItem *item,
				   double *affine,
				   ArtSVP *clip_path,
				   gint flags);
static double ie_canvas_grid_point (GnomeCanvasItem *item,
				    double x, 
				    double y,
				    int cx,
				    int cy,
				    GnomeCanvasItem **actual_item);
static void ie_canvas_grid_render (GnomeCanvasItem *item,
				   GnomeCanvasBuf *buf);

static GnomeCanvasItemClass *parent_class;

GtkType
ie_canvas_grid_get_type (void)
{
  static guint grid_type = 0;
  
  if (!grid_type)
    {
      static const GtkTypeInfo grid_info = 
      {
	"IECanvasGrid",
	sizeof (IECanvasGrid),
	sizeof (IECanvasGridClass),
	(GtkClassInitFunc) ie_canvas_grid_class_init,
	(GtkObjectInitFunc) ie_canvas_grid_init,
	NULL,
	NULL,
	(GtkClassInitFunc) NULL
      };
      
      grid_type = gtk_type_unique (gnome_canvas_item_get_type (),
				   &grid_info);
    }
  
  return grid_type;
}

static void
ie_canvas_grid_class_init (IECanvasGridClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  
  object_class = (GtkObjectClass*) klass;
  item_class = (GnomeCanvasItemClass*) klass;
  
  parent_class = gtk_type_class (gnome_canvas_item_get_type ());
  
  gtk_object_add_arg_type ("IECanvasGrid::x", GTK_TYPE_DOUBLE, 
			   GTK_ARG_READWRITE, ARG_X);
  gtk_object_add_arg_type ("IECanvasGrid::y", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_Y);
  gtk_object_add_arg_type ("IECanvasGrid::width", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_WIDTH);
  gtk_object_add_arg_type ("IECanvasGrid::height", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_HEIGHT);
  gtk_object_add_arg_type ("IECanvasGrid::square_size", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_SQUARE_SIZE);
  gtk_object_add_arg_type ("IECanvasGrid::thickness", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_THICKNESS);
  gtk_object_add_arg_type ("IECanvasGrid::fill_color_rgba", GTK_TYPE_UINT,
			   GTK_ARG_READWRITE, ARG_FILL_COLOUR_RGBA);
  object_class->destroy = ie_canvas_grid_destroy;
  object_class->set_arg = ie_canvas_grid_set_arg;
  object_class->get_arg = ie_canvas_grid_get_arg;
  
  item_class->translate = NULL;
  /*	item_class->bounds = gnome_canvas_grid_bounds;*/
  item_class->point = ie_canvas_grid_point;
  item_class->update = ie_canvas_grid_update;
  item_class->render = ie_canvas_grid_render;
}

static void
ie_canvas_grid_init (IECanvasGrid *grid)
{
  grid->x = 0.0;
  grid->y = 0.0;
  grid->width = 0.0;
  grid->height = 0.0;
  grid->square_size = 1.0;
  grid->thickness = 2.0;
  /* Black, fully opaque */
  grid->colour = 0x000000FF;

  grid->lines = NULL;
}

static void
ie_art_svp_free (struct _IEArtSVP *lines)
{
  int i;

  for (i = 0; i < lines->h; i++)
    art_svp_free (lines->horizontal[i]);
  for (i = 0; i < lines->v; i++)
    art_svp_free (lines->vertical[i]);
}

static void
ie_canvas_grid_destroy (GtkObject *object)
{
  IECanvasGrid *grid;
  
  g_return_if_fail (object != NULL);
  g_return_if_fail (IE_IS_CANVAS_GRID (object));
  
  grid = IE_CANVAS_GRID (object);
  if (grid->lines)
    {
      ie_art_svp_free (grid->lines);
      g_free (grid->lines);
    }

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
ie_canvas_grid_set_arg (GtkObject *object,
			   GtkArg *arg,
			   guint arg_id)
{
  GnomeCanvasItem *item;
  IECanvasGrid *grid;
  
  item = GNOME_CANVAS_ITEM (object);
  grid = IE_CANVAS_GRID (object);
  
  switch (arg_id)
    {
    case ARG_X:
      grid->x = GTK_VALUE_DOUBLE (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_Y:
      grid->y = GTK_VALUE_DOUBLE (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_WIDTH:
      grid->width = GTK_VALUE_DOUBLE (*arg);
      grid->p_width = (gint)(grid->width * grid->square_size);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_HEIGHT:
      grid->height = GTK_VALUE_DOUBLE (*arg);
      grid->p_height = (gint)(grid->height * grid->square_size);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_SQUARE_SIZE:
      grid->square_size = GTK_VALUE_DOUBLE (*arg);
      grid->p_width = (gint)(grid->width * grid->square_size);
      grid->p_height = (gint)(grid->height * grid->square_size);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_THICKNESS:
      grid->thickness = GTK_VALUE_DOUBLE (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_FILL_COLOUR_RGBA:
      grid->colour = GTK_VALUE_UINT (*arg);
      /* Changing the colour doesn't require an update
	 only a redraw */
      gnome_canvas_request_redraw (GNOME_CANVAS (item->canvas),
				   grid->x, grid->y, 
				   grid->x + grid->p_width + 1.0,
				   grid->y + grid->p_height + 1.0);
      break;
    default:
      break;
    }
}

static void
ie_canvas_grid_get_arg (GtkObject *object,
			GtkArg *arg,
			guint arg_id)
{
  IECanvasGrid *grid;
  
  grid = IE_CANVAS_GRID (object);
  
  switch (arg_id)
    {
      
    case ARG_X:
      GTK_VALUE_DOUBLE (*arg) = grid->x;
      break;
      
    case ARG_Y:
      GTK_VALUE_DOUBLE (*arg) = grid->y;
      break;
      
    case ARG_WIDTH:
      GTK_VALUE_DOUBLE (*arg) = grid->width;
      break;
      
    case ARG_HEIGHT:
      GTK_VALUE_DOUBLE (*arg) = grid->height;
      break;
      
    case ARG_SQUARE_SIZE:
      GTK_VALUE_DOUBLE (*arg) = grid->square_size;
      break;

    case ARG_THICKNESS:
      GTK_VALUE_DOUBLE (*arg) = grid->thickness;
      break;

    case ARG_FILL_COLOUR_RGBA:
      GTK_VALUE_UINT (*arg) = grid->colour;
      break;

    default:
      arg->type = GTK_TYPE_INVALID;
      break;
    }
}

/* Based on code from gLife by Ali Abdin */
static ArtSVP*
get_line_svp (gint x1,
	      gint y1,
	      gint x2,
	      gint y2,
	      double line_width)
{
  ArtVpath vpath[3];
  ArtSVP *svp;

  vpath[0].code = ART_MOVETO_OPEN;
  vpath[0].x = x1;
  vpath[0].y = y1;
  vpath[1].code = ART_LINETO;
  vpath[1].x = x2;
  vpath[1].y = y2;
  vpath[2].code = ART_END;
  vpath[2].x = 0;
  vpath[2].y = 0;

  svp = art_svp_vpath_stroke (vpath,
			      ART_PATH_STROKE_JOIN_MITER,
			      ART_PATH_STROKE_CAP_BUTT,
			      line_width, 4.0, 1.0);
  return svp;
}

static void
ie_canvas_grid_update (GnomeCanvasItem *item,
		       double affine[6],
		       ArtSVP *clip_path,
		       gint flags)
{
  IECanvasGrid *grid;
  double x1, x2, y1, y2;
  double cellsize;
  int i;

  if (parent_class->update)
    (* parent_class->update) (item, affine, clip_path, flags);
  
  grid = IE_CANVAS_GRID (item);

  if (grid->lines)
    ie_art_svp_free (grid->lines);
  else
    grid->lines = g_new (struct _IEArtSVP, 1);  

  /* Rebuild lines struct */
  grid->lines->h = (gint)grid->width + 1;
  grid->lines->horizontal = g_new (ArtSVP*, (gint)grid->width + 1);
  grid->lines->v = (gint)grid->height + 1;
  grid->lines->vertical = g_new (ArtSVP*, (gint)grid->height + 1);

  x1 = grid->x;;
  x2 = x1 + (grid->p_width * item->canvas->pixels_per_unit);
  y1 = grid->y;
  y2 = y1 + (grid->p_height * item->canvas->pixels_per_unit);

  cellsize = grid->square_size * item->canvas->pixels_per_unit;
  for (i = 0; i < grid->width + 1; i++)
    grid->lines->horizontal[i] = get_line_svp ((gint)(i * cellsize) + x1, y1,
					       (gint)(i * cellsize) + x1, 
					       y2, grid->thickness);
  for (i = 0; i < grid->height + 1; i++)
    grid->lines->vertical[i] = get_line_svp (0 + x1, (gint)(i * cellsize) + y1, 
					     x2, (gint)(i * cellsize) + y1, 
					     grid->thickness);

  gnome_canvas_update_bbox (item, x1, y1, x2, y2);
}

static double
ie_canvas_grid_point (GnomeCanvasItem *item,
		      double x,
		      double y,
		      int cx,
		      int cy,
		      GnomeCanvasItem **actual_item)
{
  /* Grid item cannot receive events
     Uncomment below line and return 0.0 
     to *EXCLUSIVELY* receive events */
  /*   *actual_item = item; */

  return 10.0;
}

static void
ie_canvas_grid_render (GnomeCanvasItem *item,
		       GnomeCanvasBuf *buf)
{
  IECanvasGrid *grid;
  int xlower, xhigher, ylower, yhigher;
  int cellsize;
  int i;

  grid = IE_CANVAS_GRID (item);

  if (buf->is_bg)
    {
      gnome_canvas_buf_ensure_buf (buf);
      buf->is_bg = FALSE;
    }

  /* Should really only draw lines that lie
     within buf->rect for speed */
  cellsize = grid->square_size * item->canvas->pixels_per_unit;
  xlower = CLAMP ((int)((buf->rect.x0 - grid->x) / cellsize), 
		  0, grid->lines->h);
  xhigher =  CLAMP ((int)((buf->rect.x1 - grid->x) / cellsize), 
		    xlower, grid->lines->h);
  ylower = CLAMP ((int)((buf->rect.y0 - grid->y) / cellsize), 
		  0, grid->lines->v);
  yhigher = CLAMP ((int)((buf->rect.y1 - grid->y) / cellsize), 
		   ylower, grid->lines->v);

  if (xlower == xhigher)
    gnome_canvas_render_svp (buf, grid->lines->horizontal[xlower], grid->colour);
  else
    for (i = xlower; i <= MIN (xhigher, grid->lines->h - 1); i++)
      gnome_canvas_render_svp (buf, grid->lines->horizontal[i], grid->colour);

  if (ylower == yhigher)
    gnome_canvas_render_svp (buf, grid->lines->vertical[ylower], grid->colour);
  else
    for (i = ylower; i <= MIN (yhigher, grid->lines->v - 1); i++)
      gnome_canvas_render_svp (buf, grid->lines->vertical[i], grid->colour);
}
