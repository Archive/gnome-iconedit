/*
  gnome-canvas-grid: A custom canvas item to act as a grid.

  Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
*/

#include <config.h>
#include <math.h>
#include "gnome-canvas-grid.h"
#include <libgnomeui/gnome-canvas-util.h>

#include <libart_lgpl/art_vpath.h>
#include <libart_lgpl/art_svp.h>
#include <libart_lgpl/art_svp_vpath.h>
#include <libart_lgpl/art_rgb_svp.h>

enum {
  ARG_0,
  ARG_X,
  ARG_Y,
  ARG_WIDTH,
  ARG_HEIGHT,
  ARG_SQUARE_SIZE,
  ARG_PIXBUF,
  ARG_CHECKERBOARD,
  ARG_PAPER_COLOUR_GDK,
  ARG_PAPER_COLOUR,
  ARG_GRID_COLOUR_GDK,
  ARG_GRID_COLOUR
};

#define SQUARE_SIZE 17.0
#define GRID_SPACING 3.0
#define CELL_SIZE (gint)(SQUARE_SIZE + GRID_SPACING)

static void gnome_canvas_grid_class_init (GnomeCanvasGridClass *klass);
static void gnome_canvas_grid_init (GnomeCanvasGrid *grid);
static void gnome_canvas_grid_destroy (GtkObject *object);
static void gnome_canvas_grid_set_arg (GtkObject *object,
				       GtkArg *arg,
				       guint arg_id);
static void gnome_canvas_grid_get_arg (GtkObject *object,
				       GtkArg *arg,
				       guint arg_id);
static void gnome_canvas_grid_update (GnomeCanvasItem *item,
				      double *affine,
				      ArtSVP *clip_path,
				      gint flags);
static void gnome_canvas_grid_draw (GnomeCanvasItem *item,
				    GdkDrawable *drawable,
				    int x,
				    int y,
				    int width,
				    int height);
static double gnome_canvas_grid_point (GnomeCanvasItem *item,
				       double x, 
				       double y,
				       int cx,
				       int cy,
				       GnomeCanvasItem **actual_item);

static GnomeCanvasItemClass *parent_class;

GtkType
gnome_canvas_grid_get_type (void)
{
  static guint grid_type = 0;
  
  if (!grid_type)
    {
      static const GtkTypeInfo grid_info = 
      {
	"GnomeCanvasGrid",
	sizeof (GnomeCanvasGrid),
	sizeof (GnomeCanvasGridClass),
	(GtkClassInitFunc) gnome_canvas_grid_class_init,
	(GtkObjectInitFunc) gnome_canvas_grid_init,
	NULL,
	NULL,
	(GtkClassInitFunc) NULL
      };
      
      grid_type = gtk_type_unique (gnome_canvas_item_get_type (),
				   &grid_info);
    }
  
  return grid_type;
}

static void
gnome_canvas_grid_class_init (GnomeCanvasGridClass *klass)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  
  object_class = (GtkObjectClass*) klass;
  item_class = (GnomeCanvasItemClass*) klass;
  
  parent_class = gtk_type_class (gnome_canvas_item_get_type ());
  
  gtk_object_add_arg_type ("GnomeCanvasGrid::x", GTK_TYPE_DOUBLE, 
			   GTK_ARG_READWRITE, ARG_X);
  gtk_object_add_arg_type ("GnomeCanvasGrid::y", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_Y);
  gtk_object_add_arg_type ("GnomeCanvasGrid::width", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_WIDTH);
  gtk_object_add_arg_type ("GnomeCanvasGrid::height", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_HEIGHT);
  gtk_object_add_arg_type ("GnomeCanvasGrid::square_size", GTK_TYPE_DOUBLE,
			   GTK_ARG_READWRITE, ARG_SQUARE_SIZE);
  gtk_object_add_arg_type ("GnomeCanvasGrid::pixbuf", GTK_TYPE_POINTER,
			   GTK_ARG_READWRITE, ARG_PIXBUF);
  gtk_object_add_arg_type ("GnomeCanvasGrid::checkerboard", GTK_TYPE_BOOL,
			   GTK_ARG_READWRITE, ARG_CHECKERBOARD);
  gtk_object_add_arg_type ("GnomeCanvasGrid::paper_color_gdk", GTK_TYPE_GDK_COLOR,
			   GTK_ARG_READWRITE, ARG_PAPER_COLOUR_GDK);
  gtk_object_add_arg_type ("GnomeCanvasGrid::paper_color", GTK_TYPE_UINT,
			   GTK_ARG_READWRITE, ARG_PAPER_COLOUR);
  gtk_object_add_arg_type ("GnomeCanvasGrid::grid_color_gdk", GTK_TYPE_GDK_COLOR,
			   GTK_ARG_READWRITE, ARG_GRID_COLOUR_GDK);
  gtk_object_add_arg_type ("GnomeCanvasGrid::grid_color", GTK_TYPE_UINT,
			   GTK_ARG_READWRITE, ARG_GRID_COLOUR);

  object_class->destroy = gnome_canvas_grid_destroy;
  object_class->set_arg = gnome_canvas_grid_set_arg;
  object_class->get_arg = gnome_canvas_grid_get_arg;
  
  item_class->translate = NULL;
  /*	item_class->bounds = gnome_canvas_grid_bounds;*/
  item_class->point = gnome_canvas_grid_point;
  item_class->update = gnome_canvas_grid_update;
  item_class->draw = gnome_canvas_grid_draw;
}

static void
gnome_canvas_grid_init (GnomeCanvasGrid *grid)
{
  grid->x = 0.0;
  grid->y = 0.0;
  grid->width = 0.0;
  grid->height = 0.0;
  grid->square_size = SQUARE_SIZE;
  grid->pixbuf = NULL;
  grid->checkerboard = 1;
  grid->grid_colour = 0x00000000;
  grid->paper_colour = 0xFFFFFFFF;
}

static void
gnome_canvas_grid_destroy (GtkObject *object)
{
  GnomeCanvasGrid *grid;
  
  g_return_if_fail (object != NULL);
  g_return_if_fail (GNOME_IS_CANVAS_GRID (object));
  
  grid = GNOME_CANVAS_GRID (object);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gnome_canvas_grid_set_arg (GtkObject *object,
			   GtkArg *arg,
			   guint arg_id)
{
  GnomeCanvasItem *item;
  GnomeCanvasGrid *grid;
  
  item = GNOME_CANVAS_ITEM (object);
  grid = GNOME_CANVAS_GRID (object);
  
  switch (arg_id)
    {
    case ARG_X:
      grid->x = GTK_VALUE_DOUBLE (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_Y:
      grid->y = GTK_VALUE_DOUBLE (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_WIDTH:
      grid->width = GTK_VALUE_DOUBLE (*arg);
      grid->p_width = (gint)(grid->width * grid->square_size);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_HEIGHT:
      grid->height = GTK_VALUE_DOUBLE (*arg);
      grid->p_height = (gint)(grid->height * grid->square_size);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_SQUARE_SIZE:
      grid->square_size = GTK_VALUE_DOUBLE (*arg);
      grid->p_width = (gint)(grid->width * grid->square_size);
      grid->p_height = (gint)(grid->height * grid->square_size);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_PIXBUF:
      grid->pixbuf = GTK_VALUE_POINTER (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_CHECKERBOARD:
      grid->checkerboard = GTK_VALUE_BOOL (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_PAPER_COLOUR_GDK:
      g_warning ("GnomeCanvasGrid::paper_color_gdk is not implemented yet.");
      break;
    case ARG_PAPER_COLOUR:
      grid->paper_colour = GTK_VALUE_UINT (*arg);
      gnome_canvas_item_request_update (item);
      break;
    case ARG_GRID_COLOUR_GDK:
      g_warning ("GnomeCanvasGrid::grid_color_gdk is not implemented yet.");
      break;
    case ARG_GRID_COLOUR:
      grid->grid_colour = GTK_VALUE_UINT (*arg);
      gnome_canvas_item_request_update (item);
      break;
    default:
      break;
    }
}

static void
gnome_canvas_grid_get_arg (GtkObject *object,
			   GtkArg *arg,
			   guint arg_id)
{
  GnomeCanvasGrid *grid;
  
  grid = GNOME_CANVAS_GRID (object);
  
  switch (arg_id)
    {
      
    case ARG_X:
      GTK_VALUE_DOUBLE (*arg) = grid->x;
      break;
      
    case ARG_Y:
      GTK_VALUE_DOUBLE (*arg) = grid->y;
      break;
      
    case ARG_WIDTH:
      GTK_VALUE_DOUBLE (*arg) = grid->width;
      break;
      
    case ARG_HEIGHT:
      GTK_VALUE_DOUBLE (*arg) = grid->height;
      break;
      
    case ARG_SQUARE_SIZE:
      GTK_VALUE_DOUBLE (*arg) = grid->square_size;
      break;

    case ARG_PIXBUF:
      GTK_VALUE_POINTER (*arg) = grid->pixbuf;
      break;
      
    case ARG_CHECKERBOARD:
      GTK_VALUE_BOOL (*arg) = grid->checkerboard;
      break;

    case ARG_PAPER_COLOUR_GDK:
      arg->type = GTK_TYPE_INVALID;
      break;

    case ARG_PAPER_COLOUR:
      GTK_VALUE_UINT (*arg) = grid->paper_colour;
      break;

    case ARG_GRID_COLOUR_GDK:
      arg->type = GTK_TYPE_INVALID;
      break;

    case ARG_GRID_COLOUR:
      GTK_VALUE_UINT (*arg) = grid->grid_colour;
      break;

    default:
      arg->type = GTK_TYPE_INVALID;
      break;
    }
}

static void
gnome_canvas_grid_update (GnomeCanvasItem *item,
			  double affine[6],
			  ArtSVP *clip_path,
			  gint flags)
{
  GnomeCanvasGrid *grid;
  guchar *pixels, *tp, *buf, *bufptr;
  double x1, x2, y1, y2;
  gint bx, by, iy, ix;

  if (parent_class->update)
    (* parent_class->update) (item, affine, clip_path, flags);
  
  grid = GNOME_CANVAS_GRID (item);

  x1 = 0;
  x2 = grid->p_width;
  y1 = 0;
  y2 = grid->p_height;

  gnome_canvas_update_bbox (item, x1, y1, x2, y2);
}

/* How it works...
   Badly explained by Iain.

   When a redraw is asked for
   we draw an area bigger than the required redraw.
   
   |a|b|c|d|
   |e|f|g|h|
   |i|j|k|l|

   If the required redraw is f->g
   We render a->l on the buffer
   then shift the buffer to seem as if f is the 0,0 cell, instead of a.
   This avoids problems where half a cell is to be redrawn.
   If the redraw was a->d,
   we would end up rendering a->l on the buffer, 
   but only redrawing a->d.
*/

static void
gnome_canvas_grid_draw (GnomeCanvasItem *item,
			GdkDrawable *drawable,
			int x,
			int y,
			int width,
			int height)
{
  GnomeCanvasGrid *grid;
  GdkGC *gc;
  guchar *buf, *bufptr;
  int by, bx, iy, ix;
  double i2w[6], w2c[6], i2c[6];
  ArtPoint i1, c1;
  guchar *colours, *tp;
  gint cell_size;
  gint f_x, f_y, f_h, f_w, f_c; 
  gint r_w, r_h;
  gint d_x, d_y;

  grid = GNOME_CANVAS_GRID (item);
  g_return_if_fail (grid);

  if ( (x > grid->p_width) || (y > grid->p_height))
    return;

  cell_size = grid->square_size;
  /* Get canvas co-ordinates */
  gnome_canvas_item_i2w_affine (item, i2w);
  gnome_canvas_w2c_affine (item->canvas, w2c);
  art_affine_multiply (i2c, i2w, w2c);

  i1.x = grid->x;
  i1.y = grid->y;
  art_affine_point (&c1, &i1, i2c);

  /* Restrict to the size of the area */
  r_h = height = MIN (height, grid->p_height - (y < 0 ? 0 : y));
  r_w = width = MIN (width, grid->p_width - (x < 0 ? 0 : x));
  
  /* Get the x,y co-ordinates of the fake area to redraw */
  f_x = MAX((x / cell_size) - 1, 0);
  f_y = MAX((y / cell_size) - 1, 0);

  f_c = f_x + f_y * (gint)grid->width;
  colours = grid->pixbuf + (f_c * 4);

  /* Calculate the number of complete squares in the redraw,
     then redraw the row of squares around it as well */
  f_w = MIN((gint)grid->width, (width / cell_size) + 3);
  f_h = MIN((gint)grid->height, (height / cell_size) + 3);

  /* Make sure the area doesn't leave the grid */
  f_w = MIN (f_w, (gint)grid->width - f_x);
  f_h = MIN (f_h, (gint)grid->height - f_y);

  /* Calculate the actual width of the buffer...
     Making sure it doesn't exceed the grid limits. */
  width = MIN((f_w * cell_size), grid->p_width);
  height = MIN((f_h * cell_size), grid->p_height);

  bufptr = buf = g_new (guchar, width * height * 3);
  memset (buf, 0x00, width * height * 3); 
 
/*   bufptr += (gint)(width * 3 * GRID_SPACING); */
  for (by = 0; by < f_h; by++)
    {
      for (iy = 0; iy < (gint)grid->square_size; iy++)
	{
/* 	  bufptr += (gint)GRID_SPACING * 3; */

	  for (bx = 0; bx < f_w; bx++)
	    {	      
	      tp = colours + (gint)(grid->width * by * 4) + (bx * 4);  
	      for (ix = 0; ix < (gint)grid->square_size; ix++)
		{
		  guchar r = tp[0];
		  guchar g = tp[1];
		  guchar b = tp[2];
		  guchar a = tp[3];
		  int v;

		  /* Work out the chequerboard if we can see it */ 
		  if (grid->checkerboard && a < 255)
		    {
		      gint top, side;
		      gint teo, seo;

		      top = (f_x + bx) * cell_size + ix;
		      side = (f_y + by) * cell_size + iy;

/* 		      g_print ("%d,%d\n", top, side); */
		      teo = (top / 5) % 2; /* Is the pixel in an 
					       even or odd 16*16 
					       square? */
		      seo = (side / 5) % 2;

		      if ((teo + seo) % 2 == 0)
			v = 0xAA;
		      else
			v = 0x55;
		    }
		  else
		    v = 0xFF;
		  
		  *bufptr++ = v + (((r - v) * a + 0x80) >> 8);
		  *bufptr++ = v + (((g - v) * a + 0x80) >> 8);
		  *bufptr++ = v + (((b - v) * a + 0x80) >> 8);
		}
	    }
/* 	  bufptr += ((gint)GRID_SPACING * 3); */
	}
    }
/*   bufptr += (gint)(width * 3 * GRID_SPACING); */

  d_x = MAX (x - (f_x * cell_size), 0);
  d_y = MAX (y - (f_y * cell_size), 0);

  bufptr = buf + (d_y * width * 3) + d_x * 3;

  gc = gdk_gc_new (drawable);
  gdk_draw_rgb_image_dithalign(drawable,
			       gc,
			       c1.x - (x < 0 ? x : 0), 
			       c1.y - (y < 0 ? y : 0),
			       r_w, r_h,
			       GDK_RGB_DITHER_NORMAL,
			       bufptr,
			       width * 3,
			       c1.x, c1.y);
  gdk_gc_unref (gc);
  g_free (buf);
}

static double
gnome_canvas_grid_point (GnomeCanvasItem *item,
			 double x,
			 double y,
			 int cx,
			 int cy,
			 GnomeCanvasItem **actual_item)
{
  *actual_item = item;

  return 0.0;
}

void
gnome_canvas_grid_update_region (GnomeCanvasGrid *grid,
				 double x,
				 double y)
{
  ArtPoint i1, i2;
  ArtPoint c1, c2;
  double i2c[6];
  g_return_if_fail (grid != NULL);

  i1.x = x * grid->square_size;
  i2.x = i1.x + grid->square_size;
  i1.y = y * grid->square_size;
  i2.y = i1.y + grid->square_size;

  gnome_canvas_item_i2c_affine (GNOME_CANVAS_ITEM (grid), i2c);
  art_affine_point (&c1, &i1, i2c);
  art_affine_point (&c2, &i2, i2c);

  gnome_canvas_request_redraw (GNOME_CANVAS (GNOME_CANVAS_ITEM (grid)->canvas),
			       (int)c1.x, (int)c1.y, (int)c2.x, (int)c2.y);
}
