/*
  GtkBuffer - A generic object for handling buffers.
  Copyright (C) 1999 - Iain Holmes <ih@csd.abdn.ac.uk>
*/

#include <gtk/gtksignal.h>
#include "ie-model.h"

enum {
  MODEL_UPDATED,
  LAST_SIGNAL
};

static void ie_model_class_init (IEModelClass *klass);
static void ie_model_init (IEModel *model);
static void ie_model_finalize (GtkObject *object);

static guint model_signals[LAST_SIGNAL] = { 0 };

static GtkObject *parent_class = NULL;

static void ie_marshal_NONE__INT_INT_INT_INT (GtkObject *object,
					      GtkSignalFunc func,
					      gpointer func_data,
					      GtkArg *args);

/* Marshaller for 4 int arguments */
static void
ie_marshal_NONE__INT_INT_INT_INT (GtkObject *object,
				  GtkSignalFunc func,
				  gpointer func_data,
				  GtkArg *args)
{
  IESignal_NONE__INT_INT_INT_INT rfunc;
  
  rfunc = (IESignal_NONE__INT_INT_INT_INT) func;
  (*rfunc) (object,
	    GTK_VALUE_INT (args[0]),
	    GTK_VALUE_INT (args[1]),
	    GTK_VALUE_INT (args[2]),
	    GTK_VALUE_INT (args[3]),
	    func_data);
}

GtkType
ie_model_get_type (void)
{
  static GtkType model_type = 0;
  
  if (!model_type)
    {
      static const GtkTypeInfo model_info =
      {
	"IEModel",
	sizeof (IEModel),
	sizeof (IEModelClass),
	(GtkClassInitFunc) ie_model_class_init,
	(GtkObjectInitFunc) ie_model_init,
	NULL,
	NULL,
	(GtkClassInitFunc)NULL,
      };

      model_type = gtk_type_unique (GTK_TYPE_DATA, 
				    &model_info);
    }
  
  return model_type;
}

static void
ie_model_class_init (IEModelClass *klass)
{
  GtkObjectClass *object_class;
  
  object_class = (GtkObjectClass*) klass;
  
  parent_class = gtk_type_class (GTK_TYPE_OBJECT);
  
  model_signals[MODEL_UPDATED] =
    gtk_signal_new ("model-updated",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (IEModelClass, 
				       model_updated),
		    ie_marshal_NONE__INT_INT_INT_INT,
		    GTK_TYPE_NONE, 4,
		    GTK_TYPE_INT,
		    GTK_TYPE_INT,
		    GTK_TYPE_INT,
		    GTK_TYPE_INT);
  
  gtk_object_class_add_signals (object_class, model_signals, 
				LAST_SIGNAL);
  
  object_class->finalize = ie_model_finalize;
}

static void
ie_model_finalize (GtkObject *object)
{
  g_return_if_fail (object != NULL);
  g_return_if_fail (IE_IS_MODEL (object));
  
  GTK_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
ie_model_init (IEModel *model)
{
  model->dirty = FALSE;

  model->width = 0;
  model->height = 0;
  model->rowstride = 0;

  /* Init ref_count to 0 */
  model->ref_count = 0;

  model->model = NULL;
}

GtkObject*
ie_model_new (guint width,
	      guint height,
	      GdkPixbuf *pb)
{
  IEModel *model;

  model = gtk_type_new (ie_model_get_type ());
  model->width = width;
  model->height = height;

  if (pb == NULL) {
    /* Create the pixbuf */
    model->model = gdk_pixbuf_new (GDK_COLORSPACE_RGB, TRUE, 8, width, height);
    
    /* Clear the pixbuf */
    model->rowstride = gdk_pixbuf_get_rowstride (model->model);
    memset (gdk_pixbuf_get_pixels (model->model), 0x00, model->rowstride * height);

    g_assert (model->rowstride % 4 == 0);
  } else {
    model->model = gdk_pixbuf_add_alpha (pb, FALSE, 0, 0, 0);
    model->rowstride = gdk_pixbuf_get_rowstride (model->model);
  }

  return GTK_OBJECT (model);
}

void
ie_model_model_updated (IEModel *model,
			gint x,
			gint y,
			gint width,
			gint height)
{
  g_return_if_fail (model != NULL);
  g_return_if_fail (IE_IS_MODEL (model));

  gtk_signal_emit (GTK_OBJECT (model), model_signals[MODEL_UPDATED], 
		   x, y, width, height);
}

void 
ie_model_ref (IEModel *model)
{
  g_return_if_fail (model != NULL);
  g_return_if_fail (IE_IS_MODEL (model));

  model->ref_count++;
}

void
ie_model_unref (IEModel *model)
{
  g_return_if_fail (model != NULL);
  g_return_if_fail (IE_IS_MODEL (model));

  model->ref_count--;

  if (model->ref_count <= 0)
    gtk_object_destroy (GTK_OBJECT (model));
}

void
ie_model_set_from_pixbuf (IEModel *model,
			  GdkPixbuf *pixbuf)
{
  GdkPixbuf *newpb;
  gint rowstride;
  guchar *pixels;

  g_return_if_fail (model != NULL);
  g_return_if_fail (IE_IS_MODEL (model));
  g_return_if_fail (pixbuf != NULL);
  g_return_if_fail (model->width == gdk_pixbuf_get_width (pixbuf));
  g_return_if_fail (model->height == gdk_pixbuf_get_height (pixbuf));

  newpb = gdk_pixbuf_add_alpha (pixbuf, FALSE, 0, 0, 0);
  pixels = gdk_pixbuf_get_pixels (model->model);
  rowstride = gdk_pixbuf_get_rowstride (newpb);
  g_assert (rowstride == model->rowstride);

  memcpy (pixels, gdk_pixbuf_get_pixels (newpb), rowstride * model->height);
  gdk_pixbuf_unref (newpb);

  ie_model_model_updated (model, 0, 0, model->width, model->height);
}

void
ie_model_set_pixel (IEModel *model,
		    gint x,
		    gint y,
		    guchar red,
		    guchar green,
		    guchar blue,
		    guchar alpha)
{
  guchar *pixels;

  g_return_if_fail (model != NULL);
  g_return_if_fail (IE_IS_MODEL (model));
  g_return_if_fail (x >= 0 && x <= model->width);
  g_return_if_fail (y >= 0 && y <= model->height);

  pixels = gdk_pixbuf_get_pixels (model->model);
  pixels += ((y * model->rowstride) + x * 4);
  pixels[0] = red;
  pixels[1] = green;
  pixels[2] = blue;
  pixels[3] = alpha;
  
  /* Emit the updated signal */
  ie_model_model_updated (model, x, y, 1, 1);
}

const guchar*
ie_model_get_pixel(IEModel *model, 
		   guint x, 
		   guint y)
{
  guchar *pixels;
  guint i = y * model->rowstride + x * 4;
  
  pixels = gdk_pixbuf_get_pixels (model->model);
  return pixels + i;
}

