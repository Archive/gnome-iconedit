/*
  GtkUndoBuffer - An object to handle undo/redo functions.
  Copyright (C) 1999 - Iain Holmes <ih@csd.abdn.ac.uk>
*/

#include <gtk/gtksignal.h>
#include "gtkundobuffer.h"

static void gtk_undo_buffer_class_init (GtkUndoBufferClass *klass);
static void gtk_undo_buffer_init (GtkUndoBuffer *ubuffer);
static void gtk_undo_buffer_finalize (GtkObject *object);

static GtkBufferClass *parent_class = NULL;

GtkType
gtk_paste_buffer_get_type (void)
{
  static GtkType undo_buffer_type = 0;
  
  if (!undo_buffer_type)
    {
      static const GtkTypeInfo undo_buffer_info =
      {
	"GtkUndoBuffer",
	sizeof (GtkUndoBuffer),
	sizeof (GtkUndoBufferClass),
	(GtkClassInitFunc) gtk_undo_buffer_class_init,
	(GtkObjectInitFunc) gtk_undo_buffer_init,
	NULL, 
	NULL,
	(GtkClassInitFunc)NULL
      };
      
      undo_buffer_type = gtk_type_unique (GTK_TYPE_BUFFER,
					  &undo_buffer_info);
    }
  
  return undo_buffer_type;
}

static void
gtk_undo_buffer_class_init (GtkUndoBufferClass *klass)
{
  GtkObjectClass *object_class;
  
  object_class = (GtkObjectClass*) klass;
  
  parent_class = gtk_type_class (GTK_TYPE_BUFFER);
  
  object_class->finalize = gtk_undo_buffer_finalize;
  
  parent_class->item_added = NULL;
  parent_class->item_removed = NULL;
}

static void
gtk_undo_buffer_finalize (GtkObject *object)
{
  GtkUndoBuffer *ubuffer;
  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_UNDO_BUFFER (object));
  
  ubuffer = GTK_UNDO_BUFFER (object);
  
  ubuffer->buffer = NULL;
  
  GTK_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gtk_undo_buffer_init (GtkUndoBuffer *ubuffer)
{
  ubuffer->buffer = g_node_new (NULL);
  ubuffer->pointer = ubuffer->pointer;
}

GtkObject*
gtk_undo_buffer_new (void)
{
  return GTK_OBJECT (gtk_type_new (gtk_undo_buffer_get_type ()));
}

void
gtk_undo_buffer_add_item (GtkUndoBuffer *undo,
			  gpointer item)
{
  GNode *leaf;

  g_return_if_fail (undo != NULL);
  g_return_if_fail (GTK_IS_UNDO_BUFFER (undo));

  leaf = g_node_new (item);
  g_node_prepend (undo->pointer, leaf);

  gtk_buffer_item_added (GTK_BUFFER (undo));
}

void
gtk_undo_buffer_start_branch (GtkUndoBuffer *undo,
			      gpointer item)
{
  GNode *branch;

  g_return_if_fail (undo != NULL);
  g_return_if_fail (GTK_IS_UNDO_BUFFER (undo));

  branch = g_node_new (item);
  undo->pointer = g_node_prepend (undo->pointer, branch);

  gtk_buffer_item_added (GTK_BUFFER (undo));
}

void
gtk_undo_buffer_end_branch (GtkUndoBuffer *undo)
{
  g_return_if_fail (undo != NULL);
  g_return_if_fail (GTK_IS_UNDO_BUFFER (undo));

  /* Up a level */
  undo->pointer = undo->pointer->parent;
}
