/*
 * ie-canvas-grid: A custom canvas item to act as a grid.
 *
 * Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
 * Copyright (C) 2000 - Iain Holmes <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __IE_CANVAS_GRID_H__
#define __IE_CANVAS_GRID_H__

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-canvas.h>

#include <libart_lgpl/art_svp.h>

BEGIN_GNOME_DECLS

#define IE_TYPE_CANVAS_GRID (ie_canvas_grid_get_type ())
#define IE_CANVAS_GRID(obj) (GTK_CHECK_CAST ((obj), IE_TYPE_CANVAS_GRID, IECanvasGrid))
#define IE_CANVAS_GRID_KLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), IE_TYPE_CANVAS_GRID, IECanvasGridClass))
#define IE_IS_CANVAS_GRID(obj) (GTK_CHECK_TYPE ((obj), IE_TYPE_CANVAS_GRID))
#define IE_IS_CANVAS_GRID_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), IE_TYPE_CANVAS_GRID))

typedef struct _IECanvasGrid IECanvasGrid;
typedef struct _IECanvasGridClass IECanvasGridClass;

struct _IEArtSVP
{
  int h, v; /* number of lines horizontally and vertically */
  ArtSVP **vertical, **horizontal;
};

struct _IECanvasGrid
{
  GnomeCanvasItem item;
  
  double x, y;
  double width, height; /* width/height in cells */
  double square_size;
  double thickness;

  guint32 colour; /* Colour to draw in */
  gint p_width, p_height; /* width/height in pixels */

  /* --PRIVATE-- */
  struct _IEArtSVP *lines; /* ArtSVPs for drawing the grid lines */
};

struct _IECanvasGridClass
{
  GnomeCanvasItemClass parent_class;
};

GtkType ie_canvas_grid_get_type (void);

END_GNOME_DECLS

#endif /* __GNOME_CANVAS_GRID_H__ */
