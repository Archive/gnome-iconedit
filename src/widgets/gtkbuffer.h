/* 
   GtkBuffer - A generic object for handling buffers.
   Copyright (C) 1999 - Iain Holmes <ih@csd.abdn.ac.uk>
*/

#ifndef __GTKBUFFER_H__
#define __GTKBUFFER_H__

#include <gtk/gtkdata.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
	
#define GTK_TYPE_BUFFER (gtk_buffer_get_type ())
#define GTK_BUFFER(obj) (GTK_CHECK_CAST ((obj), GTK_TYPE_BUFFER, GtkBuffer))
#define GTK_BUFFER_CLASS(klass) (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_BUFFER, GtkBufferClass))
#define GTK_IS_BUFFER(obj) (GTK_CHECK_TYPE ((obj), GTK_TYPE_BUFFER))
#define GTK_IS_BUFFER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_BUFFER))
	
typedef struct _GtkBuffer GtkBuffer;
typedef struct _GtkBufferClass GtkBufferClass;

struct _GtkBuffer
{
	GtkData data;

	gint buffer_size;
};

struct _GtkBufferClass
{
	GtkDataClass parent_class;

	void (* item_added) (GtkBuffer *buffer);
	void (* item_removed) (GtkBuffer *buffer);
};

GtkType gtk_buffer_get_type (void);
GtkObject* gtk_buffer_new (void);
gint gtk_buffer_get_size (GtkBuffer *buffer);
void gtk_buffer_item_added (GtkBuffer *buffer);
void gtk_buffer_item_removed (GtkBuffer *buffer);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GTKBUFFER_H__ */
