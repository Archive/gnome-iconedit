/* menus.h
 *
 * Copyright (C) 1999 Havoc Pennington
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef ICONED_MENUS_H
#define ICONED_MENUS_H

#include <config.h>
#include <gnome.h>

#include "view.h"

enum {
  EDIT_UNDO,
  EDIT_REDO,
  EDIT_SEPARATOR,
  EDIT_CUT,
  EDIT_COPY,
  EDIT_PASTE,
  EDIT_PASTE_INTO,
  EDIT_CLEAR,
  EDIT_SELECT_ALL,
  EDIT_END
};

enum {
  TOOLBAR_NEW,
  TOOLBAR_OPEN,
  TOOLBAR_SAVE,
  TOOLBAR_SAVE_AS,
  TOOLBAR_SEPARATOR2,
  TOOLBAR_UNDO,
  TOOLBAR_REDO,
  TOOLBAR_SEPARATOR,
  TOOLBAR_CUT,
  TOOLBAR_COPY,
  TOOLBAR_PASTE,
  TOOLBAR_END
};

void ie_install_menus_and_toolbar(IEView *view);
void save_cb   (GtkWidget* widget, IEView *view);      

#endif

