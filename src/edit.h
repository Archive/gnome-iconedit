/* edit.h
 *
 * Copyright (C) 1999 Iain Holmes <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __EDIT_H__
#define __EDIT_H__

#include "app.h"
#include "selection.h"
#include "view.h"
#include <ie-model.h>

typedef struct _IEPasteBuffer IEPasteBuffer;

struct _IEPasteBuffer
{
  gint x, y;
  gint width, height;
  gint rowstride; /* Rowstride of the pbuffer */

  GdkPixbuf *pixbuf;
};

void ie_selection_destroy (IEView *view);
IESelection *ie_selection_get (IEView *view);
void edit_clear_cb (GtkWidget *widget,
		    IEView *view);
void edit_select_all_cb (GtkWidget* widget,
			 IEView *view);
void edit_cut_cb (GtkWidget *widget,
		  IEView *view);
void edit_copy_cb (GtkWidget *widget,
		   IEView *view);
void edit_paste_cb (GtkWidget *widget,
		    IEView *view);
void edit_paste_into_cb (GtkWidget *widget,
			 IEView *view);
void edit_paste_at (IEView *view,
		    gint p_x,
		    gint p_y);
#endif /* __EDIT_H__ */
