/* gnome-iconedit-component.c
 *
 * Copyright (C) 2000 Iain Holmes
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>

#include <liboaf/liboaf.h>
#include <bonobo.h>
#include <gdk-pixbuf/gdk-pixbuf-loader.h>

#include <ie-model.h>
#include <gtkpastebuffer.h>
#include "view.h"
#include "grid.h"
#include "selection.h"

static int running_objects = 0;
static BonoboGenericFactory *icon_factory = NULL;
GtkPasteBuffer *paste_buffer = NULL;

/*
 * BonoboObject data 
 */
typedef struct {
  BonoboEmbeddable *bonobo_object;
  IEModel *model;
} bonobo_object_data_t;

/*
 * View data 
 */

typedef struct {
  bonobo_object_data_t *bod;
  BonoboView *bview;
  GtkWidget *canvas;
  GtkWidget *scroller;
  Grid *grid;
  gboolean size_allocated;
  IEBonoboView *view;
} view_data_t;

void
ie_update_statusbar (GtkWidget *widget,
		     int x,
		     int y)
{
  /* Dummy function to satisfy grid.c */
}

void
ie_install_menus_and_toolbar (GtkWidget *widget)
{
  /* Dummy function to satisfy view.c */
}

static void
bod_destroy_cb (BonoboEmbeddable *embeddable,
		bonobo_object_data_t *bod)
{
  g_return_if_fail (bod != NULL);

  gtk_object_destroy (GTK_OBJECT (bod->model));

  running_objects--;
  if (running_objects > 0)
    return;

  /* When no objects left, destroy and quit */
  bonobo_object_unref (BONOBO_OBJECT (icon_factory));
  gtk_main_quit ();
}

static void
destroy_view (BonoboView *view, 
	      view_data_t *view_data)
{
  g_return_if_fail (view_data != NULL);

  g_free (view_data);
}

static void
resize_all (BonoboView *view,
	    gpointer data)
{
  view_data_t *view_data;
  bonobo_object_data_t *bod = data;
  Grid *grid;
  int width, height;

  g_return_if_fail (view != NULL);
  view_data = gtk_object_get_data (GTK_OBJECT (view),
				   "view_data");

  width = bod->model->width;
  height = bod->model->height;
  grid = view_data->grid;
  view_data->view->model = bod->model;

  gnome_canvas_set_scroll_region (GNOME_CANVAS (view_data->canvas),
				  0.0, 0.0,
				  1.0 * width,
				  1.0 * height);
  gnome_canvas_item_set (grid->cboard, 
			 "width", (gdouble)width,
			 "height", (gdouble)height,
			 NULL);
  gnome_canvas_item_set (grid->mainlayer,
			 "width", (double)width,
			 "height", (double)height,
			 "pixbuf", gdk_pixbuf_get_pixels (bod->model->model),
			 "rowstride", gdk_pixbuf_get_rowstride (bod->model->model),
			 NULL);
  gnome_canvas_item_set (grid->gridlines,
			 "width", (gdouble)width,
			 "height", (gdouble)height,
			 NULL);
}

static Bonobo_Persist_ContentTypeList *
get_content_types (BonoboPersistStream *ps,
		   gpointer closure,
		   CORBA_Environment *ev)
{
  return bonobo_persist_generate_content_types (1, "image/*");
}

static void
load_image_from_stream (BonoboPersistStream *ps,
			Bonobo_Stream stream,
			Bonobo_Persist_ContentType type,
			gpointer data,
			CORBA_Environment *ev)
{
  bonobo_object_data_t *bod = data;
  GdkPixbufLoader *loader = gdk_pixbuf_loader_new ();
  Bonobo_Stream_iobuf *buffer;
  GdkPixbuf *pixbuf;

  CORBA_exception_init (ev);

  do {
    buffer = Bonobo_Stream_iobuf__alloc ();
    Bonobo_Stream_read (stream, 4096, &buffer, ev);
    if (buffer->_buffer &&
	(ev->_major != CORBA_NO_EXCEPTION ||
	 !gdk_pixbuf_loader_write (loader,
				   buffer->_buffer,
				   buffer->_length))) {
      if (ev->_major != CORBA_NO_EXCEPTION)
	g_warning ("Fatal error loading from stream");
      else
	g_warning ("Fatal image format error");

      gdk_pixbuf_loader_close (loader);
      gtk_object_destroy (GTK_OBJECT (loader));
      CORBA_free (buffer);
      CORBA_exception_free (ev);
      return;
    }
    CORBA_free (buffer);
  } while (buffer->_length > 0);

  pixbuf = gdk_pixbuf_loader_get_pixbuf (loader);
  gdk_pixbuf_ref (pixbuf);
  gdk_pixbuf_loader_close (loader);
  gtk_object_destroy (GTK_OBJECT (loader));

  bod->model = IE_MODEL (ie_model_new (gdk_pixbuf_get_width (pixbuf), 
				       gdk_pixbuf_get_height (pixbuf), pixbuf));
  bonobo_embeddable_foreach_view (bod->bonobo_object, 
  				  resize_all, bod); 

  CORBA_exception_free (ev);
  return;
}

static void
save_image_to_stream (BonoboPersistStream *ps,
		      const Bonobo_Stream stream,
		      Bonobo_Persist_ContentType type,
		      gpointer data,
		      CORBA_Environment *ev)
{
  bonobo_object_data_t *bod = data;

  g_return_if_fail (bod != NULL);
  g_return_if_fail (bod->model != NULL);

  if (*type && g_strcasecmp (type, "image/*") != 0) {
    CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
			 ex_Bonobo_Persist_WrongDataType, NULL);
    return;
  }
}

static void
render_icon (GnomePrintContext *ctx,
	     double width,
	     double height,
	     const Bonobo_PrintScissor *opt_scissor,
	     gpointer user_data)
{
  bonobo_object_data_t *bod = user_data;
  double matrix[6];

  art_affine_scale (matrix, width, height);
  matrix[4] = 0;
  matrix[5] = 0;

  gnome_print_gsave (ctx);
  gnome_print_concat (ctx, matrix);

  gnome_print_rgbaimage (ctx,
			 gdk_pixbuf_get_pixels (bod->model->model),
			 gdk_pixbuf_get_width (bod->model->model),
			 gdk_pixbuf_get_height (bod->model->model),
			 gdk_pixbuf_get_rowstride (bod->model->model));
  gnome_print_grestore (ctx);
}

static void
init_server_factory (int argc,
		     char **argv)
{
  CORBA_Environment ev;

  CORBA_exception_init (&ev);
  gnome_init_with_popt_table ("gnome-iconedit-component", VERSION,
			      argc, argv,
			      oaf_popt_options, 0, NULL);
  gdk_rgb_init ();
  oaf_init (argc, argv);

  if (!bonobo_init (CORBA_OBJECT_NIL, CORBA_OBJECT_NIL, CORBA_OBJECT_NIL))
    g_error (_("Unable to initialise Bonobo"));

  CORBA_exception_free (&ev);
}

static void
view_paint_tool (BonoboView *view,
		 view_data_t *view_data,
		 const char *path)
{
  IEBonoboView *bview = view_data->view;
  bview->tool = IET_FG;
}

static void
view_fill_tool (BonoboView *view,
		view_data_t *view_data,
		const char *path)
{
  IEBonoboView *bview = view_data->view;
  bview->tool = IET_FILL;
}

static void
view_dropper_tool (BonoboView *view,
		   view_data_t *view_data,
		   const char *path)
{
  IEBonoboView *bview = view_data->view;
  bview->tool = IET_EYEDROP;
}

static void
view_create_menus (view_data_t *view_data)
{
  BonoboUIHandler *uih;
  Bonobo_UIHandler remote_uih;
  BonoboView *view = view_data->bview;

  uih = bonobo_view_get_ui_handler (view);
  remote_uih = bonobo_view_get_remote_ui_handler (view);

  if (remote_uih == CORBA_OBJECT_NIL) {
    return;
  }

  bonobo_ui_handler_set_container (uih, remote_uih);
  bonobo_ui_handler_create_toolbar (uih, "Gnome-Iconedit");
  bonobo_ui_handler_toolbar_new_item (uih, "/Gnome-Iconedit/foreground", 
				      "", _("Paint brush mode"), -1,
				      BONOBO_UI_HANDLER_PIXMAP_FILENAME,
				      "gnome-iconedit/paint.png", 0,
				      (GdkModifierType) 0,
				      view_paint_tool,
				      view_data);
  bonobo_ui_handler_toolbar_new_item (uih, "/Gnome-Iconedit/fill",
				      "", _("Flood fill mode"), -1,
				      BONOBO_UI_HANDLER_PIXMAP_FILENAME,
				      "gnome-iconedit/fill.png", 0,
				      (GdkModifierType) 0,
				      view_fill_tool,
				      view_data);
  bonobo_ui_handler_toolbar_new_item (uih, "/Gnome-Iconedit/dropper",
				      "", _("Eye-dropper mode"), -1,
				      BONOBO_UI_HANDLER_PIXMAP_FILENAME,
				      "gnome-iconedit/dropper.png", 0,
				      (GdkModifierType) 0,
				      view_dropper_tool,
				      view_data);
				      
}

static void
view_remove_menus (view_data_t *view_data)
{
  BonoboView *view = view_data->bview;
  BonoboUIHandler *uih;

  uih = bonobo_view_get_ui_handler (view);
  bonobo_ui_handler_unset_container (uih);
}

static void
view_activate_cb (BonoboView *view,
		  gboolean activate,
		  gpointer data)
{
  bonobo_view_activate_notify (view, activate);
  if (activate)
    view_create_menus (data);
  else
    view_remove_menus (data);
}

static void
view_update (IEModel *model,
	     int x,
	     int y,
	     int width,
	     int height,
	     view_data_t *view_data)
{
  grid_update_item (view_data->grid, x, y, width, height);
}

static BonoboView *
gie_create_view (BonoboEmbeddable *bonobo_object,
		 GtkWidget *scroller,
		 const Bonobo_ViewFrame view_frame,
		 gpointer data)
{
  BonoboView *view;
  bonobo_object_data_t *bod = data;
  view_data_t *view_data = g_new (view_data_t, 1);
  IEBonoboView *view_details = g_new0 (IEBonoboView, 1);

 view_data->bod = bod;
  view_data->canvas = gnome_canvas_new_aa ();
  view_data->size_allocated = TRUE;
  view_data->scroller = scroller;
  view_data->view = view_details;

  view_details->canvas = view_data->canvas;
  view_details->model = bod->model;
  view_details->black.red = 0;
  view_details->black.green = 0;
  view_details->black.blue = 0;
  view_details->black.pixel = gdk_rgb_xpixel_from_rgb (0x000000);
  view_details->current_colour = view_details->black;
  view_details->tool = IET_FG;
  
  view_data->grid = grid_new_bonoboview (view_data->canvas, view_details);
  view_details->grid = view_data->grid;

  gtk_container_add (GTK_CONTAINER (scroller), view_data->canvas);
  gtk_widget_set_usize (scroller, 400, 400);
  gtk_widget_show_all (scroller);

  view = bonobo_view_new (scroller);
  view_data->bview = view;

  gtk_object_set_data (GTK_OBJECT (view), "view_data", view_data);
  gtk_signal_connect (GTK_OBJECT (view), "destroy", 
		      GTK_SIGNAL_FUNC (destroy_view), view_data);
  gtk_signal_connect (GTK_OBJECT (view), "activate",
		      GTK_SIGNAL_FUNC (view_activate_cb), view_data);
  gtk_signal_connect (GTK_OBJECT (bod->model), "model-updated",
		      GTK_SIGNAL_FUNC (view_update), view_data);

  return view;
}

static BonoboView *
gie_view_factory (BonoboEmbeddable *bonobo_object,
		  const Bonobo_ViewFrame view_frame,
		  gpointer data)
{
  BonoboView *view;
  view_data_t *view_data;
  GtkWidget *scroller;

  scroller = gtk_scrolled_window_new (NULL, NULL);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroller),
				  GTK_POLICY_AUTOMATIC,
				  GTK_POLICY_AUTOMATIC);

  view = gie_create_view (bonobo_object, scroller, view_frame, data);

  view_data = gtk_object_get_data (GTK_OBJECT (view), "view_data");

  view_data->size_allocated = TRUE;
  return view;
}

static BonoboObject *
bonobo_object_factory (BonoboGenericFactory *this,
		       const char *goad_id,
		       gpointer data)
{
  BonoboEmbeddable *bonobo_object;
  BonoboPersistStream *stream;
  BonoboPrint *print;
  bonobo_object_data_t *bod;

  g_return_val_if_fail (this != NULL, NULL);
  g_return_val_if_fail (goad_id != NULL, NULL);

  bod = g_new0 (bonobo_object_data_t, 1);
  if (!strcmp (goad_id, "OAFIID:gie-generic:0d77ee99-ce0d-4463-94ec-99969f567f33"))
    bonobo_object = bonobo_embeddable_new (gie_view_factory, bod);
  else {
    g_free (bod);
    return NULL;
  }

  if (bonobo_object == NULL) {
    g_free (bod);
    return NULL;
  }

  bod->bonobo_object = bonobo_object;
  bod->model = ie_model_new (48, 48, NULL);
  
  stream = bonobo_persist_stream_new (load_image_from_stream, 
				      save_image_to_stream, 
				      NULL,
				      get_content_types, bod);
  if (stream == NULL) {
    gtk_object_unref (GTK_OBJECT (bonobo_object));
    g_free (bod);
    return NULL;
  }
  bonobo_object_add_interface (BONOBO_OBJECT (bonobo_object),
			       BONOBO_OBJECT (stream));

  print = bonobo_print_new (render_icon, bod);
  bonobo_object_add_interface (BONOBO_OBJECT (bonobo_object),
			       BONOBO_OBJECT (print));

  gtk_signal_connect (GTK_OBJECT (bonobo_object), "destroy",
		      GTK_SIGNAL_FUNC (bod_destroy_cb), bod);

  running_objects++;
  return BONOBO_OBJECT (bonobo_object);
}

static void
init_bonobo_icon_factory (void)
{
  icon_factory = bonobo_generic_factory_new_multi
    ("OAFIID:gie_factory:777e0cdf-2b79-4e36-93d8-e9d490c9c4b8",
     bonobo_object_factory, NULL);
}

int
main (int argc,
      char **argv)
{
  init_server_factory (argc, argv);
  init_bonobo_icon_factory ();

  gtk_widget_set_default_colormap (gdk_rgb_get_cmap ());
  gtk_widget_set_default_visual (gdk_rgb_get_visual ());

  bonobo_main ();
  return 0;
}
