/* layers.c - Layer handling code.
 *
 * Copyright (C) 2000 Iain Holmes  <terrorist@gegl.org>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include <ie-canvas-layer.h>

#ifdef USE_LOCAL_ART_RGBA
#include "art_rgba.h"
#else
#include <libart_lgpl/art_rgba.h>
#endif

#include <libart_lgpl/art_alphagamma.h>
#include <math.h>

#include "layers.h"
#include "app.h"
#include "view.h"
#include "grid.h"
#include "edit.h"

gint ie_layer_event (GnomeCanvasItem *layer,
		     GdkEvent *event,
		     IEView *view);
void ie_layer_destroy (IEView *view,
		       GnomeCanvasItem *layer);
void ie_primitive_clear_area (IECanvasLayer *layer,
			      int x1, 
			      int y1,
			      int x2,
			      int y2);
void ie_primitive_draw (IECanvasLayer *layer,
			IETool tool,
			int x1,
			int y1,
			int x2,
			int y2,
			GdkColor *colour,
			guchar alpha);
void ie_primitive_draw_line (guchar *pixels,
			     int rowstride,
			     int x1,
			     int y1,
			     int x2,
			     int y2,
			     guchar r,
			     guchar g,
			     guchar b,
			     guchar alpha);
GnomeCanvasItem* ie_layer_new (IEView *view);
void ie_layer_write_pixel (guchar *pixels,
			   int rowstride,
			   int x, 
			   int y,
			   guchar r,
			   guchar g,
			   guchar b,
			   guchar alpha);
void ie_layer_circle_pixel (guchar *pixels,
			    int rowstride,
			    int x,
			    int y,
			    int ox,
			    int oy,
			    guchar r,
			    guchar g,
			    guchar b,
			    guchar alpha,
			    gboolean fill);
void ie_primitive_draw_circle (guchar *pixels, 
			       int rowstride,
			       int ox,
			       int oy,
			       int radius,
			       guchar red,
			       guchar green,
			       guchar blue,
			       guchar alpha,
			       gboolean fill);
void ie_primitive_clear_all (IECanvasLayer *layer);
void ie_layer_draw_row (guchar *pixels,
			int rowstride,
			int x1,
			int x2,
			int y,
			guchar r,
			guchar g,
			guchar b,
			guchar alpha);

GnomeCanvasItem*
ie_layer_new (IEView *view)
{
  int width, height;
  guchar *pixels;
  GnomeCanvasItem *layer;
  Grid *grid;

  width = view->model->width;
  height = view->model->height;
  pixels = g_new0 (guchar, width * 4 * height);

  grid = (Grid *) view->grid;
  layer = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (view->canvas)),
				 ie_canvas_layer_get_type (),
				 "width", (gdouble) width,
				 "height", (gdouble) height,
				 "pixbuf", pixels,
				 "rowstride", width * 4,
				 NULL);
  /* Put the new itm under the grid */
  gnome_canvas_item_raise_to_top (grid->gridlines);

  gtk_signal_connect (GTK_OBJECT (layer), "event",
		      GTK_SIGNAL_FUNC (ie_layer_event), view);

  grid->layer = layer;

  /* Remove the selector on the lower layer */
  if (grid->sel) {
    gtk_object_destroy (GTK_OBJECT (grid->sel));
    grid->sel = NULL;
  }

  return layer;
}

void
ie_layer_destroy (IEView *view,
		  GnomeCanvasItem *layer)
{
  guchar *m_pixels, *l_pixels;
  IECanvasLayer *gi;

  gi = IE_CANVAS_LAYER (layer);
  m_pixels = gdk_pixbuf_get_pixels (view->model->model);
  l_pixels = gi->pixbuf;

  art_rgba_rgba_composite (m_pixels, l_pixels, 
			   view->model->width * view->model->height);

  /* Blank layer */
  memset (l_pixels, 0x00, view->model->width * 4 * view->model->height);
  ie_canvas_layer_update_area (gi, 0, 0, view->model->width, view->model->height);

  ie_model_model_updated (view->model, 0, 0, view->model->width, 
			  view->model->height);
}

static gint
ie_layer_rect_cb (GnomeCanvasItem *item,
		  GdkEvent *event, 
		  gpointer data)
{
  int result;
  Grid *grid;

  grid = (Grid *) data;
  g_return_val_if_fail (grid != NULL, FALSE);

  /* Pass all events upwards */
  gtk_signal_emit_by_name (GTK_OBJECT (grid->layer), "event",
			   event, &result);
  
  return result;
}  

gint
ie_layer_event (GnomeCanvasItem *layer,
		GdkEvent *event,
		IEView *view)
{
  IECanvasLayer *gi;
  IETool tool = view->tool;
  Grid *grid;
  gint r_x, r_y;
  static gint s_x = 0, s_y = 0;
  static int x1, x2, y1, y2;
  static gint drawing = FALSE;
  double size;
  GtkArg arg[1];

  gi = IE_CANVAS_LAYER (layer);
  grid = (Grid *) view->grid;

  /* Get the square size */
  arg[0].name = "IECanvasLayer::square_size";
  gtk_object_getv (GTK_OBJECT (layer), 1, arg);
  g_assert (arg[0].type == GTK_TYPE_DOUBLE);
  size = GTK_VALUE_DOUBLE (arg[0]);

  if (view->selection != NULL)
    ie_selection_destroy (view);

  switch (event->type) {
  case GDK_BUTTON_PRESS: {
    GdkCursor *crosshairs;
    
    r_x = (gint)(event->button.x / size);
    r_y = (gint)(event->button.y / size);
    drawing = TRUE;
    
    crosshairs = gdk_cursor_new (GDK_CROSS);
    gdk_window_set_cursor (view->canvas->window, crosshairs);
    gdk_cursor_destroy (crosshairs);

    x1 = r_x;
    y1 = r_y;
    break;
  }

  case GDK_BUTTON_RELEASE:
    ie_layer_destroy (view, layer);
    gdk_window_set_cursor (view->canvas->window, NULL); 
    drawing = FALSE;
    break;
    
  case GDK_MOTION_NOTIFY: {
    r_x = (gint)(event->motion.x / size);
    r_y = (gint)(event->motion.y / size);
    
    if (drawing) {
      if (x2 != r_x || y2 != r_y) {
	int ox = MIN (x1, x2);
	int dx = MAX (x1, x2);
	int oy = MIN (y1, y2);
	int dy = MAX (y1, y2);

	if (tool != IET_CIRCLE && tool != IET_FILL_CIRCLE)
	  ie_primitive_clear_area (gi, ox, oy, dx, dy);
        else
	  ie_primitive_clear_all (gi);

	ie_primitive_draw (gi, tool, x1, y1, r_x, r_y,
			   &view->current_colour, 
			   (gint)GTK_ADJUSTMENT (view->alpha)->value);
	x2 = r_x; 
	y2 = r_y;
      }
    }

    if (r_x < 0 || r_y < 0 ||
	r_x > (gi->width - 1) || r_y > (gi->height - 1))
      return TRUE;

    ie_update_statusbar (GTK_WIDGET (view), r_x, r_y);

    if (grid->sel == NULL) {
      grid->sel = gnome_canvas_item_new (gnome_canvas_root (layer->canvas),
					 gnome_canvas_rect_get_type (),
					 "x1", (double) r_x * size,
					 "y1", (double) r_y * size,
					 "x2", (double) (r_x + 1) * size,
					 "y2", (double) (r_y + 1) * size,
					 "fill_color", NULL,
					 "outline_color_rgba", 0xFFFFFFFF,
					 "width_pixels", 2,
					 NULL);
      gtk_signal_connect(GTK_OBJECT (grid->sel), "event",
			 GTK_SIGNAL_FUNC (ie_layer_rect_cb), 
			 grid);
    } else {
      gnome_canvas_item_set (grid->sel,
			     "x1", (double) r_x * size,
			     "y1", (double) r_y * size,
			     "x2", (double) (r_x + 1) * size,
			     "y2", (double) (r_y + 1) * size,
			     NULL);
    }

    s_x = r_x;
    s_y = r_y;
    return TRUE;
  }

  default:
    break;

  }

  return FALSE;
}

/* Primitive drawing routines */
void
ie_primitive_clear_all (IECanvasLayer *layer)
{
  memset (layer->pixbuf, 0x00, (int)(layer->rowstride * layer->height));
}

void
ie_primitive_clear_area (IECanvasLayer *layer,
			 int x1, 
			 int y1,
			 int x2,
			 int y2)
{
  int ox, oy;
  int dx, dy;
  int y;
  guchar *pixels;

  ox = MIN (x1, x2) - 2;
  oy = MIN (y1, y2) - 2;
  dx = MAX (x1, x2) + 2;
  dy = MAX (y1, y2) + 2;

  if (ox < 0) ox = 0;
  if (dx > layer->width) dx = layer->width;
  if (oy < 0) oy = 0;
  if (dy > layer->height) dy = layer->height;

  pixels = layer->pixbuf + (oy * layer->rowstride) + (ox * 4);

  for (y = oy; y <= dy; y++, pixels += layer->rowstride)
    memset (pixels, 0x00, ((dx - ox) + 1) * 4);
}

void
ie_primitive_draw (IECanvasLayer *layer,
		   IETool tool,
		   int x1,
		   int y1,
		   int x2,
		   int y2,
		   GdkColor *colour,
		   guchar alpha)
{
  int ox, oy;
  int dx, dy;
  int y;
  int rowstride;
  int radius = 0;
  guchar *pixels, *p;
  guchar r, g, b;

  ox = MIN (x1, x2);
  oy = MIN (y1, y2);
  dx = MAX (x1, x2);
  dy = MAX (y1, y2);

  r = colour->red / 256;
  g = colour->green / 256;
  b = colour->blue / 256;

  pixels = layer->pixbuf;
  rowstride = layer->rowstride;

  pixels += oy * rowstride + ox * 4;

  switch (tool) {
  case IET_SQUARE:
    p = pixels;
    art_rgba_run_alpha (pixels, r, g, b, alpha, (dx - ox) + 1);

    pixels += rowstride;
    for (y = oy; y < dy - 1; y++, pixels += rowstride) {
      p = pixels;
      art_rgba_run_alpha (p, r, g, b, alpha, 1);

      p += ((dx - ox) * 4);
      art_rgba_run_alpha (p, r, g, b, alpha, 1);
    }

    art_rgba_run_alpha (pixels, r, g, b, alpha, (dx - ox) + 1);
    break;

  case IET_FILL_SQUARE:
    for (y = oy; y <= dy; y++, pixels += rowstride) {
      art_rgba_run_alpha (pixels, r, g, b, alpha, (dx - ox) + 1);
    }
    break;

  case IET_LINE:
    ie_primitive_draw_line (layer->pixbuf, rowstride,
			    x1, y1, x2, y2, r, g, b, alpha);
    break;
    
  case IET_CIRCLE:
    if (x1 > x2)
      radius = x1 - x2;
    else 
      radius = x2 - x1;
    ie_primitive_draw_circle (layer->pixbuf, rowstride,
			      x1, y1, radius, r, g, b, alpha, FALSE);
    break;
    
  case IET_FILL_CIRCLE:
    if (x1 > x2)
      radius = x1 - x2;
    else
      radius = x2 - x1;

    ie_primitive_draw_circle (layer->pixbuf, rowstride,
			      x1, y1, radius, r, g, b, alpha, TRUE);
    break;

  default:
    break;
  }
  
  if (tool != IET_FILL_CIRCLE && tool != IET_CIRCLE) 
    ie_canvas_layer_update_area (layer, (double)ox, (double)oy, 
				 (double)(dx - ox) + 1, (double)(dy - oy) + 1);
  else
    ie_canvas_layer_update_area (layer, (double) x1 - radius, 
				 (double) y1 - radius,
				 (double) radius * 2 + 1,
				 (double) radius * 2 + 1);
}

void
ie_layer_write_pixel (guchar *pixels,
		      int rowstride,
		      int x, 
		      int y,
		      guchar r,
		      guchar g,
		      guchar b,
		      guchar alpha)
{
  if (x < 0 || y < 0)
    return;

  pixels += (x * 4) + (y * rowstride);
  art_rgba_run_alpha (pixels, r, g, b, alpha, 1);
}

void
ie_layer_draw_row (guchar *pixels,
		   int rowstride,
		   int x1,
		   int x2,
		   int y,
		   guchar r,
		   guchar g,
		   guchar b,
		   guchar alpha)
{
  if (x1 < 0)
    x1 = 0;

  if (y < 0)
    y = 0;

  pixels += y * rowstride + x1 * 4;

  art_rgba_run_alpha (pixels, r, g, b, alpha, (x2 - x1));
}

void
ie_layer_circle_pixel (guchar *pixels,
		       int rowstride,
		       int x,
		       int y,
		       int ox,
		       int oy,
		       guchar r,
		       guchar g,
		       guchar b,
		       guchar alpha,
		       gboolean fill)
{
  if (!fill) {
    ie_layer_write_pixel (pixels, rowstride, x + ox, y + oy, r, g, b, alpha);
    ie_layer_write_pixel (pixels, rowstride, x + ox, -y + oy, r, g, b, alpha);
    ie_layer_write_pixel (pixels, rowstride, -x + ox, -y + oy, r, g, b, alpha);
    ie_layer_write_pixel (pixels, rowstride, -x + ox, y + oy, r, g, b, alpha);
    
    if (x != y) {
      ie_layer_write_pixel (pixels, rowstride, y + ox, x + oy, r, g, b, alpha);
      ie_layer_write_pixel (pixels, rowstride, y + ox, -x + oy, r, g, b, alpha);
      ie_layer_write_pixel (pixels, rowstride, -y + ox, -x + oy, r, g, b, alpha);
      ie_layer_write_pixel (pixels, rowstride, -y + ox, x + oy, r, g, b, alpha);
    }
  } else {
    ie_layer_draw_row (pixels, rowstride, (-x + ox), x + ox, y + oy,
		       r, g, b, alpha);
    ie_layer_draw_row (pixels, rowstride, (-x + ox), x + ox, -y + oy,
		       r, g, b, alpha);

    if (x != y) {
      ie_layer_draw_row (pixels, rowstride, (-y + ox), y + ox, x + oy,
			 r, g, b, alpha);
      ie_layer_draw_row (pixels, rowstride, (-y + ox), y + ox, -x + oy,
			 r, g, b, alpha);
    }
  }
}

/* Sucky incremental algorithm line drawing
   Really should use midpoint algorithm. */
void
ie_primitive_draw_line (guchar *pixels,
			int rowstride,
			int x1,
			int y1,
			int x2,
			int y2,
			guchar r,
			guchar g,
			guchar b,
			guchar alpha)
{
  double im = 0.0, m = 0.0;
  double var;
  int t;
  int y;
  int mx, my, dx, dy;

  /* Optimisations */
  if (y1 == y2) {
    int mx = MIN (x1, x2);

    pixels += (mx * 4) + (y1 * rowstride);
    art_rgba_run_alpha (pixels, r, g, b, alpha, (MAX (x1, x2) - mx) + 1);
    return;
  }

  if (x1 == x2) {
    int my = MIN (y1, y2);

    pixels += (x1 * 4) + (my * rowstride);
    for (y = my; y <= MAX (y1, y2); y++, pixels += rowstride)
      art_rgba_run_alpha (pixels, r, g, b, alpha, 1);

    return;
  }

  if (x1 > x2 || y1 > y2) {
    mx = x2;
    my = y2;
    dx = x1;
    dy = y1;
  } else {
    mx = x1;
    my = y1;
    dx = x2;
    dy = y2;
  }

  m = ((double)(dy - my)) / ((double)(dx - mx));
  im = 1 / m;

  if (m <= 1.0 && m >= -1.0) {
    if (mx < dx) {
      var = (double)my;
      for (t = mx; t <= dx; t++) {
	ie_layer_write_pixel (pixels, rowstride, t, (int)floor (var), 
			      r, g, b, alpha);
	var += m;
      }
    } else {
      var = (double)dy;
      for (t = dx; t <= mx; t++) {
	ie_layer_write_pixel (pixels, rowstride, t, (int)floor (var),
			      r, g, b, alpha);
	var += m;
      }
    }
  } else {
    if (my < dy) {
      var = (double)mx;  

      for (t = my; t <= dy; t++) {
	ie_layer_write_pixel (pixels, rowstride, (int)floor (var), t,
			      r, g, b, alpha);
	var += im;
      }
    } else {
      var = (double)dx;

      for (t = dy; t <= my; t++) {
	ie_layer_write_pixel (pixels, rowstride, (int)floor (var), t,
			      r, g, b, alpha);
	var += im;
      }
    }
  }

}

void
ie_primitive_draw_circle (guchar *pixels, 
			  int rowstride,
			  int ox,
			  int oy,
			  int radius,
			  guchar red,
			  guchar green,
			  guchar blue,
			  guchar alpha,
			  gboolean fill)
{
  int x = 0;
  int aradius = abs (radius);
  int y = aradius;
  int d = 1 - aradius;

  ie_layer_circle_pixel (pixels, rowstride, x, y, ox, oy, 
			 red, green, blue, alpha, fill);

  while (y > x) {
    if (d < 0) {
      d += 2 * x + 3;
    } else {
      d += 2 * (x - y) + 5;
      y--;
    }

    x++;
    ie_layer_circle_pixel (pixels, rowstride, x, y, ox, oy,
			   red, green, blue, alpha, fill);
  }
}
