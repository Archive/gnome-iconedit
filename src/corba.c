/* 
   corba.c: CORBA functions for GNOME-Iconedit.
   Copyright (C)2000 - Iain Holmes <ih@csd.abdn.ac.uk>
   Adapted from gnome-terminal: corba.c by John Harper.

   This code is licensed under the GNU Public License. 
   See COPYING for full details.
*/
#include <config.h>

#ifdef USING_OAF
#include <liboaf/liboaf.h>
#else
#include <libgnorba/gnorba.h>
#endif

#include "Iconedit.h"
#include "app.h"
#include "iconedit.h"

int has_iconedit_factory = FALSE;
static CORBA_ORB orb = CORBA_OBJECT_NIL;
static PortableServer_POA poa = CORBA_OBJECT_NIL;

#define GIE_FACTORY_OAF_ID "OAFIID:IconeditFactory:301d4c2f-3f2b-404d-99e5-3fde72a1e601"

/* Iconedit Servant */
typedef struct 
{
  POA_GNOME_Iconedit_Iconedit servant;
  Iconedit *iconedit;
} IconeditServant;

static PortableServer_ServantBase__epv iconedit_base_epv;
static POA_GNOME_Iconedit_Iconedit__epv iconedit_epv;
static POA_GNOME_Iconedit_Iconedit__vepv iconedit_vepv;

/* Map from Iconedit widgets to IconeditServants; 
   the reverse mapping is handled through the IconeditServant structure */
static GHashTable *iconedit_servants;

/* IconeditFactory servant */

typedef struct 
{
  POA_GNOME_Iconedit_IconeditFactory servant;
} IconeditFactoryServant;

static PortableServer_ServantBase__epv iconedit_factory_base_epv;
static POA_GNOME_GenericFactory__epv iconedit_factory_generic_factory_epv;
static POA_GNOME_Iconedit_IconeditFactory__epv iconedit_factory_epv;
static POA_GNOME_Iconedit_IconeditFactory__vepv iconedit_factory_vepv;

static GNOME_Iconedit_IconeditFactory iconedit_factory_server = CORBA_OBJECT_NIL;

/* Prototypes */
int corba_init_server (CORBA_ORB _orb);
int corba_activate_server (void);
CORBA_Object create_iconedit_via_factory (char *filename,
					  CORBA_Environment *ev);
CORBA_Object create_iconedit_at_size_via_factory (int width, 
						  int height,
						  CORBA_Environment *ev);

/* Iconedit implementation */

/* XXX define Iconedit method implementations here.. */

/* Fills the vepv structure for the Iconedit object */
static void
Iconedit_class_init (void)
{
  static int inited = FALSE;

  if (inited)
    return;

  inited = TRUE;

  iconedit_servants = g_hash_table_new ((GHashFunc) g_direct_hash,
					(GCompareFunc) g_direct_equal);

  /* XXX fill in Iconedit method vectors here.. */

  iconedit_vepv._base_epv = &iconedit_base_epv;
  iconedit_vepv.GNOME_Iconedit_Iconedit_epv = &iconedit_epv;
}

/* Destroys the servant for an Iconedit */
static void
iconedit_destroy (GtkWidget *iconedit, 
		  IconeditServant *is, 
		  CORBA_Environment *ev)
{
  PortableServer_ObjectId *objid;

  objid = PortableServer_POA_servant_to_id (poa, is, ev);
  PortableServer_POA_deactivate_object (poa, objid, ev);
  CORBA_free (objid);

  POA_GNOME_Iconedit_Iconedit__fini (is, ev);
  g_hash_table_remove (iconedit_servants, iconedit);
  g_free (is);
}

/* Called when an iconedit with a CORBA servant is destroyed */
static void
iconedit_destroyed (GtkObject *object,
		    gpointer data)
{
  IconeditServant *is;
  CORBA_Environment ev;

  is = data;

  CORBA_exception_init (&ev);
  iconedit_destroy (GTK_WIDGET (object), is, &ev);
  CORBA_exception_free (&ev);
}

/* Returns a servant for an iconeditor, creating one if necessary */
static IconeditServant*
iconedit_servant_from_iconedit (Iconedit *iconedit,
				CORBA_Environment *ev)
{
  IconeditServant *is;
  PortableServer_ObjectId *objid;

  is = (iconedit_servants ? g_hash_table_lookup (iconedit_servants, iconedit) : 0);
  if (is != 0)
    return is;

  Iconedit_class_init ();

  is = g_new0 (IconeditServant, 1);
  is->servant.vepv = &iconedit_vepv;

  POA_GNOME_Iconedit_Iconedit__init ((PortableServer_Servant) is, ev);
  objid = PortableServer_POA_activate_object (poa, is, ev);
  CORBA_free (objid);

  is->iconedit = iconedit;
  g_hash_table_insert (iconedit_servants, iconedit, (gpointer) is);
  /* Attach to the model destory signal */
  gtk_signal_connect (GTK_OBJECT (iconedit->model), "destroy",
		      GTK_SIGNAL_FUNC (iconedit_destroyed), is);

  return is;
}

/* IconeditFactory implementation */
/* IconeditFactory::create_iconedit method */
static GNOME_Iconedit_Iconedit
IconeditFactory_create_iconedit (PortableServer_Servant servant,
				 const CORBA_char *filename,
				 CORBA_Environment *ev)
{
  Iconedit *iconedit;
  IconeditServant *is;

  iconedit = iconedit_new_app ((gchar *)filename);
  if (iconedit == CORBA_OBJECT_NIL)
    return CORBA_OBJECT_NIL;

  is = iconedit_servant_from_iconedit (iconedit, ev);
  
  return PortableServer_POA_servant_to_reference (poa, is, ev);
}

/* IconeditFactory::create_iconedit_at_size method */
static GNOME_Iconedit_Iconedit
IconeditFactory_create_iconedit_at_size (PortableServer_Servant servant,
					 CORBA_short w,
					 CORBA_short h,
					 CORBA_Environment *ev)
{
  Iconedit *iconedit;
  IconeditServant *is;

  iconedit = iconedit_new_app_at_size (w, h);
  if (iconedit == CORBA_OBJECT_NIL)
    return CORBA_OBJECT_NIL;

  is = iconedit_servant_from_iconedit (iconedit, ev);
  
  return PortableServer_POA_servant_to_reference (poa, is, ev);
}

/* IconeditFactory GenericFactory::supports method */
static CORBA_boolean
IconeditFactory_supports (PortableServer_Servant servant,
			  const CORBA_char *obj_goad_id,
			  CORBA_Environment *ev)
{
  if (strcmp (obj_goad_id, "IDL:GNOME:Iconedit:Iconedit:1.0") == 0)
    return CORBA_TRUE;
  else
    return CORBA_FALSE;
}

/* IconeditFactory GenericFactory::create_object method */
static CORBA_Object
IconeditFactory_create_object (PortableServer_Servant servant,
			       const CORBA_char *goad_id,
			       const GNOME_stringlist *params,
			       CORBA_Environment *ev)
{
  if (strcmp (goad_id, "IDL:GNOME:Iconedit:Iconedit:1.0") != 0)
    return IconeditFactory_create_iconedit (servant,
					    params->_length != 0 ? params->_buffer[0] : 0,
					    ev);
  else
    {
      CORBA_exception_set (ev, CORBA_USER_EXCEPTION,
			   ex_GNOME_GenericFactory_CannotActivate,
			   NULL);
      return CORBA_OBJECT_NIL;
    }
}

/* Fills the vepv structure for the iconedit factory object */
static void
IconeditFactory_class_init (void)
{
  static int inited = FALSE;

  if (inited)
    return;

  inited = TRUE;

  iconedit_factory_generic_factory_epv.supports = IconeditFactory_supports;
  iconedit_factory_generic_factory_epv.create_object = IconeditFactory_create_object;

  iconedit_factory_epv.create_iconedit = IconeditFactory_create_iconedit;
  iconedit_factory_epv.create_iconedit_at_size = IconeditFactory_create_iconedit_at_size;

  iconedit_factory_vepv._base_epv = &iconedit_factory_base_epv;
  iconedit_factory_vepv.GNOME_GenericFactory_epv = &iconedit_factory_generic_factory_epv;
  iconedit_factory_vepv.GNOME_Iconedit_IconeditFactory_epv = &iconedit_factory_epv;
}

/* Creates a reference for the iconedit factory object */
static GNOME_Iconedit_IconeditFactory
IconeditFactory_create (PortableServer_POA poa, 
			CORBA_Environment *ev)
{
  IconeditFactoryServant *ifs;
  PortableServer_ObjectId *objid;

  IconeditFactory_class_init ();

  ifs = g_new0 (IconeditFactoryServant, 1);
  ifs->servant.vepv = &iconedit_factory_vepv;

  POA_GNOME_Iconedit_IconeditFactory__init ((PortableServer_Servant) ifs, ev);
  objid = PortableServer_POA_activate_object (poa, ifs, ev);
  CORBA_free (objid);

  return PortableServer_POA_servant_to_reference (poa, ifs, ev);
}

static void
IconeditFactory_destroy (GNOME_Iconedit_IconeditFactory factory,
			 PortableServer_POA destroy_poa,
			 CORBA_Environment *ev)
{
  IconeditFactoryServant *ifs;
  PortableServer_ObjectId *objid;

  objid = PortableServer_POA_reference_to_id (destroy_poa, factory, ev);

  PortableServer_POA_deactivate_object (destroy_poa, objid, ev);
  CORBA_free (objid);

  ifs = PortableServer_POA_reference_to_servant (destroy_poa, factory, ev);
  POA_GNOME_Iconedit_IconeditFactory__fini (ifs, ev);

  g_free (ifs);
}

/* Initialisation */

static CORBA_Object get_iconedit_factory (void);

/* Creates and registers the CORBA servers. */
#ifdef USING_OAF

static gboolean
register_oaf (CORBA_Object obj)
{
  OAF_RegistrationResult result;
  CORBA_Environment ev;

  result = oaf_active_server_register ("OAFIID:IconeditFactory:301d4c2f-3f2b-404d-99e5-3fde72a1e601", obj);

  switch (result) {
  case OAF_REG_SUCCESS:
    return FALSE;

  case OAF_REG_NOT_LISTED:
    g_message ("Error registering the factory: not listed");
    return FALSE;

  case OAF_REG_ALREADY_ACTIVE:
    CORBA_exception_init (&ev);
    IconeditFactory_destroy (obj, poa, &ev);
    iconedit_factory_server = get_iconedit_factory ();
    if (iconedit_factory_server != CORBA_OBJECT_NIL) {
      CORBA_exception_free (&ev);
      return TRUE;
    } else {
      CORBA_exception_free (&ev);
      return FALSE;
    }
    
  case OAF_REG_ERROR:
  default:
    g_message ("Error registering the factory: generic error");
    return FALSE;
  }
}

#else

static gboolean
register_goad (CORBA_Object obj)
{
  int result;
  CORBA_Environment ev;

  CORBA_exception_init (&ev);
  result = goad_server_register (CORBA_OBJECT_NIL, obj,
				 "IDL:GNOME:Iconedit:IconeditFactory:1.0",
				 "object", &ev);
  CORBA_exception_free (&ev);

  switch (result) {
  case 0:
    g_message ("Here");
    return FALSE;
  case -2:
    IconeditFactory_destroy (obj, poa, &ev);
    iconedit_factory_server = get_iconedit_factory ();
    if (iconedit_factory_server != CORBA_OBJECT_NIL) {
      CORBA_exception_free (&ev);
      return TRUE;
    } else {
      CORBA_exception_free (&ev);
      return FALSE;
    }
  case -1:
  default:
    return FALSE;
  }
}
#endif

static int
register_servers (void)
{
  CORBA_Environment ev;
  int retval;

  retval = FALSE;
  CORBA_exception_init (&ev);

  /* Register the iconedit factory and see if it was already there */
  iconedit_factory_server = IconeditFactory_create (poa, &ev);
  if (ev._major != CORBA_NO_EXCEPTION)
    goto out;

#ifdef USING_OAF
  has_iconedit_factory = register_oaf (iconedit_factory_server);
#else
  has_iconedit_factory = register_goad (iconedit_factory_server);
#endif

  retval = has_iconedit_factory;

 out:
  CORBA_exception_free (&ev);
  return retval;
}


int
corba_init_server (CORBA_ORB _orb)
{
  int retval;
  CORBA_Environment ev;

  orb = _orb;
  
  retval = FALSE;
  CORBA_exception_init (&ev);

  poa = (PortableServer_POA) CORBA_ORB_resolve_initial_references (orb, "RootPOA", &ev);
  if (ev._major != CORBA_NO_EXCEPTION)
    goto out;

  CORBA_exception_free (&ev);

  /* see if the servers are there */
  iconedit_factory_server = get_iconedit_factory ();

  if (iconedit_factory_server != CORBA_OBJECT_NIL) {
    has_iconedit_factory = TRUE;
    retval = TRUE;
  }
  else
    retval = register_servers ();
  
 out:
  return retval;
}

/* Activates the POA manager and thus makes the services available to the world */
int
corba_activate_server (void)
{
  int retval;
  CORBA_Environment ev;
  PortableServer_POAManager poa_manager;

  /* Do nothing if the server is already running */
  if (has_iconedit_factory)
    return TRUE;

  CORBA_exception_init (&ev);

  poa_manager = PortableServer_POA__get_the_POAManager (poa, &ev);
  if (ev._major != CORBA_NO_EXCEPTION)
    {
      retval = FALSE;
      goto out;
    }

  PortableServer_POAManager_activate (poa_manager, &ev);
  if (ev._major != CORBA_NO_EXCEPTION)
    {
      retval = FALSE;
      goto out;
    }

  retval = TRUE;
 out:
  CORBA_exception_free (&ev);
  return retval;
}

/* Client-side helper functions */

/* Tries to contact the iconedit factory */
static CORBA_Object
get_iconedit_factory (void)
{
  CORBA_Object obj;
  CORBA_Environment ev;

#ifdef USING_OAF
  
  CORBA_exception_init (&ev);
  obj = oaf_activate_from_id (GIE_FACTORY_OAF_ID,
			      OAF_FLAG_EXISTING_ONLY, NULL, &ev);
  CORBA_exception_free (&ev);
#else

  obj = goad_server_activate_with_id (NULL,
				      "IDL:GNOME:Iconedit:IconeditFactory:1.0",
				      GOAD_ACTIVATE_EXISTING_ONLY,
				      NULL);
#endif

  return obj;
}

/* Creates a new iconedit with the specified filename */
CORBA_Object
create_iconedit_via_factory (char *filename,
			     CORBA_Environment *ev)
{
  CORBA_Object obj, icon;

  obj = get_iconedit_factory ();
  has_iconedit_factory = (obj != CORBA_OBJECT_NIL);

  if (obj == CORBA_OBJECT_NIL)
    return CORBA_OBJECT_NIL;

  g_assert (filename != NULL);
  icon = GNOME_Iconedit_IconeditFactory_create_iconedit (obj, filename, ev);
  CORBA_Object_release (obj, ev);
  return icon;
}

/* Creates a new iconedit at the specified size */
CORBA_Object
create_iconedit_at_size_via_factory (int width, 
				     int height,
				     CORBA_Environment *ev)
{
  CORBA_Object obj, icon;

  obj = get_iconedit_factory ();
  has_iconedit_factory = (obj != CORBA_OBJECT_NIL);

  if (obj == CORBA_OBJECT_NIL)
    return CORBA_OBJECT_NIL;

  g_assert (width > 0);
  g_assert (height > 0);
  icon = GNOME_Iconedit_IconeditFactory_create_iconedit_at_size (obj, width, height, ev);
  CORBA_Object_release (obj, ev);
  return icon;
}
