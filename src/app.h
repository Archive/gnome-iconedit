/* app.h
 *
 * Copyright (C) 1999 Havoc Pennington
 *               2000 Iain Holmes
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef ICONEDIT_APP_H
#define ICONEDIT_APP_H

#include <config.h>
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <ie-model.h>

typedef enum {
  IET_POINT,
  IET_FG,
  IET_FILL,
  IET_EYEDROP,
  IET_BRUSH,
  IET_SQUARE,
  IET_CIRCLE,
  IET_FILL_SQUARE,
  IET_FILL_CIRCLE,
  IET_LINE,
  IET_END
} IETool;

typedef enum {
  IEM_NORMAL,
  IEM_ADDITIVE,
  IEM_SUBTRACTIVE,
  /*  IEM_BURN,
      IEM_DODGE,*/
  IEM_ALPHA,
  IEM_END
} IEMode;

typedef struct _Iconedit Iconedit;
struct _Iconedit {
  IEModel *model; /* Icon model */

  GList *views; /* List of icon views */
};

Iconedit* ie_app_new(guint width, 
		     guint height, 
		     GdkPixbuf *pb);

void       ie_app_close(GtkWidget* app);
   
void       ie_app_set_tool(GtkWidget *view, IETool tool);

void       ie_app_set_from_pixbuf(Iconedit *app, GdkPixbuf *pb);

GdkPixbuf* ie_app_to_pixbuf(GtkWidget *view); 
void       ie_update_grid (GtkWidget *view);

void ie_app_set_mode (GtkWidget *view, 
  		      IEMode mode); 

void ie_app_set_title (Iconedit *app,
		       char *filename);
void ie_update_statusbar (GtkWidget *view, 
			  gint x,
			  gint y);

void ie_app_clean (GtkWidget *app);
void ie_app_dirty (GtkWidget *app);

#endif
