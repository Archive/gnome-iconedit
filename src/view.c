/* view.c - View widget for GNOME-Iconedit
 *
 * Copyright (C) 2000 Iain Holmes
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include <ie-model.h>
#include <iestatus.h>
#include <gtkpalette.h>
#include <gtkpastebuffer.h>

#include "view.h"
#include "grid.h"
#include "menus.h"

static void ie_view_class_init (IEViewClass *klass);
static void ie_view_init (IEView *view);
static void ie_view_destroy (GtkObject *class);

void ie_view_model_updated_cb (IEModel *model,
			       gint x,
			       gint y,
			       gint width,
			       gint height,
			       IEView *view);
void alpha_value_changed_cb (GtkAdjustment *adj,
			     GtkLabel *label);

/* Callbacks */
static void fg_colour_set (GnomeColorPicker *cp,
			   guint r,
			   guint g,
			   guint b,
			   guint a,
			   IEView *view);
static void colour_changed (GtkPalette *palette,
			    gint selection,
			    IEView *view);
static void item_added_cb (GtkPasteBuffer *pbuffer,
			   IEView *view);
static void item_removed_cb (GtkPasteBuffer *pbuffer,
			     IEView *view);
static void preview_update_cb (IEModel *model,
			       gint x,
			       gint y,
			       gint width,
			       gint height,
			       GtkWidget *da);
static void preview_expose (GtkWidget *da,
			    GdkEventExpose *event,
			    IEView *view);
static void mini_expose (GtkWidget *da,
			 GdkEventExpose *event,
			 IEView *view);
static void mini_update_cb (IEModel *model,
			    gint x,
			    gint y,
			    gint width,
			    gint height,
			    GtkWidget *da);

static GnomeAppClass *parent_class = NULL;

extern GtkObject *paste_buffer;

GtkType
ie_view_get_type (void)
{
  static GtkType view_type = 0;

  if (!view_type)
    {
      static const GtkTypeInfo view_info = 
      {
	"IEView",
	sizeof (IEView),
	sizeof (IEViewClass),
	(GtkClassInitFunc) ie_view_class_init,
	(GtkObjectInitFunc) ie_view_init,
	NULL, NULL,
	(GtkClassInitFunc) NULL
      };
      
      view_type = gtk_type_unique (gnome_app_get_type (), &view_info);
    }

  return view_type;
}

static void
ie_view_class_init (IEViewClass *klass)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) klass;
  widget_class = (GtkWidgetClass*) klass;

  parent_class = gtk_type_class (gnome_app_get_type ());

  object_class->destroy = ie_view_destroy;
}

static void
ie_view_init (IEView *view)
{
  /* Set up colours */
  view->current_colour.red = 0;
  view->current_colour.green = 0;
  view->current_colour.blue = 0;
  view->current_colour.pixel = gdk_rgb_xpixel_from_rgb (0x000000);

  view->black = view->current_colour;

  /* Default values */
  view->canvas = NULL;
  view->preview = NULL;
  view->mini = NULL;
  view->status = NULL;
  view->picker = NULL;
  view->alpha = NULL;
  view->palette = NULL;
  view->floater = NULL;

  view->grid = NULL;

  view->selection = NULL;

  view->tool = IET_FG;
  view->mode = IEM_NORMAL;
  view->menus = NULL;
}

/* View callbacks */

void 
ie_view_destroy (GtkObject *object)
{
  IEView *view;

  view = (IEView *) object;
  ie_model_unref (view->model);

  if (view->floater)
    gtk_widget_destroy (view->floater);
  grid_destroy ((Grid *) view->grid);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

void
ie_view_model_updated_cb (IEModel *model,
			  gint x,
			  gint y,
			  gint width,
			  gint height,
			  IEView *view)
{
  grid_update_item((Grid *) view->grid, x,  y, width, height);
}
 
void
alpha_value_changed_cb (GtkAdjustment *adj,
			GtkLabel *label)
{
  gchar *text;
  gdouble percentage;

  percentage = (adj->value / 255) * 100;
  text = g_strdup_printf (_("Level: %d%%"), (gint)percentage);

  gtk_label_set_text (label, text);
  g_free (text);
}

GtkWidget*
ie_view_new (IEModel *model)
{
  IEView *view;

  guint width, height;
  GtkWidget *sw;
  GtkWidget *frame, *alpha_vbox, *alpha_label;
  GtkWidget *colour_vbox, *alpha;

  g_return_val_if_fail (model != NULL, NULL);
  g_return_val_if_fail (IE_IS_MODEL (model), NULL);

  width = model->width;
  height = model->height;

  view = gtk_type_new (ie_view_get_type ());
  gnome_app_construct (GNOME_APP (view), "Iconedit", "GNOME-Iconedit");

  view->model = model;
  ie_model_ref (model);

  gtk_signal_connect (GTK_OBJECT (model), "model-updated",
		      GTK_SIGNAL_FUNC (ie_view_model_updated_cb), view);

  gtk_window_set_policy (GTK_WINDOW (view), FALSE, TRUE, FALSE);
  gtk_window_set_default_size (GTK_WINDOW (view), 575, 475);
  gtk_window_set_wmclass (GTK_WINDOW (view), "iconedit", "IconEdit");

  gtk_widget_push_visual (gdk_rgb_get_visual());
  gtk_widget_push_colormap (gdk_rgb_get_cmap());
  view->canvas = gnome_canvas_new_aa ();
  view->preview = gtk_drawing_area_new ();
  /* Connect the previews to the model-updated signal */
  gtk_signal_connect (GTK_OBJECT (model), "model-updated",
		      GTK_SIGNAL_FUNC (preview_update_cb), view->preview);

  gtk_drawing_area_size(GTK_DRAWING_AREA(view->preview), width+5, height+5);
  gtk_widget_queue_resize(view->preview); /* workaround bug in some 1.2.x */

  if (width <= 64 && height <= 64)
    {
      view->mini = gtk_drawing_area_new ();
      gtk_signal_connect (GTK_OBJECT (model), "model-updated",
			  GTK_SIGNAL_FUNC (mini_update_cb), view->mini);
      gtk_drawing_area_size (GTK_DRAWING_AREA (view->mini), 60, 20);
    }

  gtk_widget_pop_visual();
  gtk_widget_pop_colormap();

  sw = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);

  gtk_container_add(GTK_CONTAINER(sw), view->canvas);
  
  gnome_app_set_contents(GNOME_APP(view), sw);

  /* Add to dock */
  if (width <= 64 && height <= 64)
    {
      frame = gtk_frame_new(_("Preview"));
      gtk_container_add(GTK_CONTAINER(frame), view->preview);

      gnome_app_add_docked (GNOME_APP (view),
			    frame, "Preview",
			    GNOME_DOCK_ITEM_BEH_NORMAL,
			    GNOME_DOCK_RIGHT, 0, 0, 0);
      gtk_widget_show_all (frame);
    }
  else
    {
      /* Toplevel window to show preview */
      GtkWidget *scroller;

      view->floater = gtk_window_new (GTK_WINDOW_TOPLEVEL);
      gtk_window_set_title (GTK_WINDOW (view->floater), _("GNOME-Iconedit: Preview"));
      gtk_container_set_border_width (GTK_CONTAINER (view->floater), 2);
      scroller = gtk_scrolled_window_new (NULL, NULL);
      gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scroller), 
					     view->preview);
      gtk_container_add (GTK_CONTAINER (view->floater), scroller);
    }

  if (width <= 64 && height <= 64)
    {
      frame = gtk_frame_new (_("Mini-Preview"));
      gtk_container_add (GTK_CONTAINER (frame), view->mini);

      gnome_app_add_docked (GNOME_APP (view),
			    frame, "Mini-Preview",
			    GNOME_DOCK_ITEM_BEH_NORMAL,
			    GNOME_DOCK_RIGHT, 0, 1, 0);
      gtk_widget_show_all (frame);
    }

  view->status = ie_status_new (3, FALSE);
  gtk_widget_set_usize (IE_STATUS (view->status)->status->pdata[2], 50, -1);
  /* Naughty */
  gtk_box_set_child_packing (GTK_BOX (view->status), 
			     IE_STATUS (view->status)->status->pdata[2],
			     FALSE, FALSE, 0, GTK_PACK_START);

  gnome_app_set_statusbar(GNOME_APP(view), view->status);

  colour_vbox = gtk_vbox_new (FALSE, 2);
  gtk_container_set_border_width (GTK_CONTAINER (colour_vbox), 2);
  view->palette = gtk_palette_new (256);

  /* Make this load a default palette - the generated default is a bit sucky */
  gtk_palette_set_palette_default (GTK_PALETTE (view->palette));
  
  gtk_box_pack_start (GTK_BOX (colour_vbox), view->palette, FALSE, FALSE, 1);

  view->picker = gnome_color_picker_new ();
  gtk_box_pack_start (GTK_BOX (colour_vbox), view->picker, FALSE, FALSE, 1);

  frame = gtk_frame_new (_("Alpha level"));

  alpha_vbox = gtk_vbox_new (FALSE, 0);
  alpha_label = gtk_label_new (_("Level:100%"));
  gtk_misc_set_alignment (GTK_MISC (alpha_label), 0, 0.5);

  gtk_box_pack_start (GTK_BOX (alpha_vbox), alpha_label, TRUE, TRUE, 0);
  view->alpha = gtk_adjustment_new (255.0, 
				    0.0,
				    255.0,
				    1.0,
				    10.0,
				    0.0);
  gtk_signal_connect (GTK_OBJECT (view->alpha), "value-changed",
		      GTK_SIGNAL_FUNC (alpha_value_changed_cb), alpha_label);

  alpha = gtk_hscale_new (GTK_ADJUSTMENT (view->alpha));
  gtk_scale_set_draw_value (GTK_SCALE (alpha), FALSE);
  
  gtk_box_pack_start (GTK_BOX (alpha_vbox), alpha, TRUE, TRUE, 0);

  gtk_container_add (GTK_CONTAINER (frame), alpha_vbox);
  gtk_box_pack_start (GTK_BOX (colour_vbox), frame, FALSE, FALSE, 1);

  /* Add to dock */
  gnome_app_add_docked (GNOME_APP (view),
			colour_vbox, "Colour-Palette",
			GNOME_DOCK_ITEM_BEH_NEVER_HORIZONTAL,
			GNOME_DOCK_RIGHT, 0, 2, 0);
  gtk_widget_show_all (colour_vbox);
  view->grid = (gpointer) grid_new (view);

  ie_install_menus_and_toolbar (view);

  /* Connect to the paste buffer's signals */
  gtk_signal_connect (paste_buffer, "item-added",
		      GTK_SIGNAL_FUNC (item_added_cb), 
		      view);
  
  gtk_signal_connect (paste_buffer, "item-removed",
		      GTK_SIGNAL_FUNC (item_removed_cb), 
		      view);

  gtk_signal_connect(GTK_OBJECT(view->picker), "color_set",
                     GTK_SIGNAL_FUNC(fg_colour_set),
                     view);
  
  gtk_signal_connect(GTK_OBJECT(view->preview), "expose_event",
		     GTK_SIGNAL_FUNC(preview_expose),
		     view);
  
  if (view->mini)
    gtk_signal_connect (GTK_OBJECT (view->mini), "expose_event",
			GTK_SIGNAL_FUNC (mini_expose),
			view);

  gtk_signal_connect (GTK_OBJECT (view->palette), "selection-changed",
		      GTK_SIGNAL_FUNC (colour_changed), 
		      view);

  gtk_widget_show_all(sw);
  gtk_widget_show(view->status);
  if (view->floater)
    gtk_widget_show_all (view->floater);

  return GTK_WIDGET (view);
}

static void
fg_colour_set (GnomeColorPicker *cp,
	       guint r,
	       guint g,
	       guint b,
	       guint a,
	       IEView *view)
{
  view->current_colour.red = r;
  view->current_colour.green = g;
  view->current_colour.blue = b;
  view->current_colour.pixel = 
    gdk_rgb_xpixel_from_rgb (((r/256) << 16) +
			     ((g/256) << 8) +
			     ((b/256)));
}

static void
colour_changed (GtkPalette *palette,
		gint selection,
		IEView *view)
{
  gushort rs, gs, bs, as;
  guint r, g, b;

  gtk_palette_get_colour (palette, selection, &r, &g, &b);
  gnome_color_picker_set_i8 (GNOME_COLOR_PICKER (view->picker),
			     r, g, b, 255);
  /* Fake the emmission */
  gnome_color_picker_get_i16 (GNOME_COLOR_PICKER (view->picker),
			      &rs, &gs, &bs, &as);
  gtk_signal_emit_by_name (GTK_OBJECT (view->picker), "color-set",
			   rs, gs, bs, as);
}

static void
item_added_cb (GtkPasteBuffer *pbuffer,
	       IEView *view)
{
  AppMenus *am;
  am = view->menus;

  gtk_widget_set_sensitive (am->paste, TRUE);
  gtk_widget_set_sensitive (am->b_paste, TRUE);

#if 0
  if (gtk_object_get_data (GTK_OBJECT (app), "selection"))
    gtk_widget_set_sensitive (am->paste_into, TRUE);
#endif
}

static void
item_removed_cb (GtkPasteBuffer *pbuffer,
		 IEView *view)
{
  AppMenus *am;

  if (GTK_BUFFER (pbuffer)->buffer_size != 0)
    return;
  
  am = view->menus;
  gtk_widget_set_sensitive (am->paste, FALSE);
  gtk_widget_set_sensitive (am->b_paste, FALSE);
  gtk_widget_set_sensitive (am->paste_into, FALSE);
}

static void
mini_update_cb (IEModel *model,
		gint x,
		gint y,
		gint width,
		gint height,
		GtkWidget *da)
{
  gtk_widget_queue_draw (da);
}

static void
mini_expose (GtkWidget *da,
	     GdkEventExpose *event,
	     IEView *view)
{
  IEModel *model;
  int i, x, y, bg[3][3], rowstride;
  GdkPixbuf *minipb;
  guchar *dest, *src, *pixels;
  guchar *buffer = g_new (guchar, 60 * 20 * 3);
  GdkGC *gc;

  model = view->model;
  /* Don't want model destroyed while we're using it */
  ie_model_ref (model);

  if (model->width >= 20 && model->height >= 20)
    minipb = gdk_pixbuf_scale_simple (model->model, 20, 20,
				      GDK_INTERP_NEAREST);
  else {
    minipb = model->model;
    gdk_pixbuf_ref (minipb);
  }

  /* Setup the background colours */
  bg[0][0] = 0x00;
  bg[0][1] = 0x00;
  bg[0][2] = 0x00;
  bg[1][0] = 0xFF;
  bg[1][1] = 0xFF;
  bg[1][2] = 0xFF;
  bg[2][0] = da->style->bg[GTK_STATE_NORMAL].red / 256;
  bg[2][1] = da->style->bg[GTK_STATE_NORMAL].green / 256;
  bg[2][2] = da->style->bg[GTK_STATE_NORMAL].blue / 256;

  pixels = gdk_pixbuf_get_pixels (minipb);
  rowstride = gdk_pixbuf_get_rowstride (minipb);

  y = 0;
  while (y < 20) {
    src = pixels + (y * rowstride);
    dest = buffer + (y * 180);
    x = 0;
    while (x < 20) {
      guchar r = *(src++);
      guchar g = *(src++);
      guchar b = *(src++);
      guchar a = *(src++);
      int v;

      for (i = 0; i < 3; i++) {
	v = bg[i][0];
	dest[0 + (i * 60)] = v + (((r - v) * a + 0x80) >> 8);
	v = bg[i][1];
	dest[1 + (i * 60)] = v + (((g - v) * a + 0x80) >> 8);
	v = bg[i][2];
	dest[2 + (i * 60)] = v + (((b - v) * a + 0x80) >> 8);
      }
      dest += 3;
      ++x;
    }
    ++y;
  }

  gc = gdk_gc_new (da->window);
  gdk_draw_rgb_image (da->window,
		      gc,
		      0, 0,
		      60, 20,
		      GDK_RGB_DITHER_NORMAL,
		      buffer,
		      60 * 3);
  g_free (buffer);
  gdk_gc_unref (gc);
  gdk_pixbuf_unref (minipb);
  ie_model_unref (model);
}

static void
preview_update_cb (IEModel *model,
		   gint x,
		   gint y,
		   gint width,
		   gint height,
		   GtkWidget *da)
{
  gtk_widget_queue_draw (da);
}

static void
preview_expose (GtkWidget *da,
		GdkEventExpose *event,
		IEView *view)
{
  IEModel *model = view->model;
  gint w, h, x = 0, y = 0, i = 0;
  gint extra_w, extra_h;
  guint bufsize;
  guchar *buf, *pixels, *src, *dest;

  ie_model_ref (model);
  w = da->allocation.width;
  h = da->allocation.height;
  extra_w = w - model->width;
  extra_h = h - model->height;

  bufsize = model->width * model->height * 3;
  buf = g_malloc (bufsize);

  pixels = gdk_pixbuf_get_pixels (model->model);
  src = pixels;
  dest = buf;
  while (i < bufsize / 3) {
    guchar r = *src++;
    guchar g = *src++;
    guchar b = *src++;
    guchar a = *src++;
    int v;

    v = da->style->bg[GTK_STATE_NORMAL].red / 256;
    *(dest++) = v + (((r - v) * a + 0x80) >> 8);
    v = da->style->bg[GTK_STATE_NORMAL].green / 256;
    *(dest++) = v + (((g - v) * a + 0x80) >> 8);
    v = da->style->bg[GTK_STATE_NORMAL].blue / 256;
    *(dest++) = v + (((b - v) * a + 0x80) >> 8);
      
    ++i;
  }

  if (extra_w > 0)
    x = extra_w / 2;

  if (extra_h > 0)
    y = extra_h / 2;

  gdk_gc_set_clip_rectangle (da->style->black_gc,
			     &event->area);
  gdk_draw_rgb_image (da->window,
		      da->style->black_gc,
		      x, y,
		      model->width,
		      model->height,
		      GDK_RGB_DITHER_NORMAL,
		      buf,
		      model->width * 3);
  gdk_gc_set_clip_rectangle (da->style->black_gc, NULL);
  
  g_free (buf);
}
