/* palette.c
 * Functions for loading, saving and editing palettes.
 *
 * Copyright (C) 1999 Iain Holmes <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gtkpalette.h>
#include <config.h>
#include <gnome.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "view.h"

void palette_load (GtkButton *button,
		   GtkPalette *palette);
void palette_save (GtkButton *button,
		   GtkPalette *palette);
void palette_edit (GtkWidget *widget,
		   IEView *view);

/* Some default callbacks for both load and save file req's */
static void
palette_file_req_close (GtkWidget *file_req,
			GtkWidget *button)
{
  gtk_widget_set_sensitive (button, TRUE);
}

static void
palette_file_req_cancel (GtkWidget *button,
			 GtkPalette *palette)
{
  gtk_widget_destroy (gtk_widget_get_toplevel (button));
}

/* Load routines */
static void
palette_load_file (gchar *filename,
		   GtkPalette *palette)
{
  int numcolours = 0;
  FILE *handle;
  gchar line[1024];
  gchar *new_colours, *pos;
  gint r, g, b;

  g_return_if_fail (filename != NULL);
  g_return_if_fail (palette != NULL);

  new_colours = (gchar*) g_malloc0 (sizeof (gchar) * palette->numcolours * 3);
  pos = new_colours;

  handle = fopen (filename, "r");
  if (!handle)
    {
      g_warning ("Cannot open file %s.\n", filename);
      g_free (new_colours);
      return;
    }

  if (!fgets (line, 1024, handle))
    {
      g_warning ("Premature end of file in %s.\n", filename);
      g_free (new_colours);
      return;
    }

  if (strncmp (line, "GIMP Palette", 12) != 0)
    {
      g_warning ("%s is not a GIMP palette file.\n", filename);
      g_free (new_colours);
      return;
    }

  while (!feof (handle))
    {
      if (numcolours == palette->numcolours)
	break;

      fgets (line, 1024, handle);

      if (line[0] == '#')
	continue;

      sscanf (line, "%d %d %d", &r, &g, &b);

      *pos++ = r;
      *pos++ = g;
      *pos++ = b;

      numcolours++;
    }

  gtk_palette_set_palette (palette, numcolours, new_colours);
  g_free (new_colours);
}

static void
palette_load_okay (GtkWidget *button,
		   GtkPalette *palette)
{
  GtkWidget *file_req;
  gchar *palette_file = NULL;

  file_req = gtk_widget_get_toplevel (button);
  palette_file = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_req));

  palette_load_file (palette_file, palette);
  gtk_widget_destroy (file_req);
}

void
palette_load (GtkButton *button,
	      GtkPalette *palette)
{
  GtkWidget *file_req;

  gtk_widget_set_sensitive (GTK_WIDGET (button), FALSE);
  file_req = gtk_file_selection_new (_("Select a palette file"));

  gtk_signal_connect (GTK_OBJECT (file_req), "destroy",
		      GTK_SIGNAL_FUNC (palette_file_req_close), button);
  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (file_req)->ok_button), 
		      "clicked", GTK_SIGNAL_FUNC (palette_load_okay),
		      palette);
  gtk_signal_connect (GTK_OBJECT(GTK_FILE_SELECTION (file_req)->cancel_button),
		      "clicked", GTK_SIGNAL_FUNC (palette_file_req_cancel),
		      palette);

  gtk_widget_show_all (file_req);
}
  
static void
palette_save_file (gchar *filename,
		   GtkPalette *palette)
{
  FILE *handle;
  int numcolours, i;
  gchar *colours;

  handle = fopen (filename, "w");
  if (!handle)
    {
      g_warning ("Could not open %s for writing.\n", filename);
      return;
    }

  colours = gtk_palette_get_palette (palette);
  numcolours = palette->numcolours;

  fputs ("GIMP Palette\n", handle);
  fputs ("# Palette file written by the GNOME-Iconeditor\n", handle);
  for (i = 0; i < numcolours * 3; i+= 3)
    {
      gchar *line;

      line = g_strdup_printf ("%d %d %d\n", colours[i], colours[i+1],
			      colours[i+2]);
      g_print ("%s", line);
      fputs (line, handle);
      g_free (line);
    }

  fclose (handle);
}
  
static void
palette_save_okay (GtkWidget *button,
		   GtkPalette *palette)
{
  GtkWidget *file_req;
  gchar *filename;

  file_req = gtk_widget_get_toplevel (button);
  filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (file_req));

  palette_save_file (filename, palette);
  gtk_widget_destroy (file_req);
}

void
palette_save (GtkButton *button,
	      GtkPalette *palette)
{
  GtkWidget *file_req;

  gtk_widget_set_sensitive (GTK_WIDGET (button), FALSE);
  file_req = gtk_file_selection_new (_("Save the palette file as..."));

  gtk_signal_connect (GTK_OBJECT (file_req), "destroy",
		      GTK_SIGNAL_FUNC (palette_file_req_close), button);
  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (file_req)->ok_button), 
		      "clicked", GTK_SIGNAL_FUNC (palette_save_okay),
		      palette);
  gtk_signal_connect (GTK_OBJECT(GTK_FILE_SELECTION (file_req)->cancel_button),
		      "clicked", GTK_SIGNAL_FUNC (palette_file_req_cancel),
		      palette);

  gtk_widget_show_all (file_req);
}

static void
palette_colour_changed (GtkPalette *palette,
			gint selection,
			GtkWidget *colour_sel)
{
	gdouble colours[4];
	gint red, green, blue;

	gtk_palette_get_colour (palette, selection,
				&red, &green, &blue);

	colours[0] = (gdouble)((guchar)red) / 255;
	colours[1] = (gdouble)((guchar)green) / 255;
	colours[2] = (gdouble)((guchar)blue) / 255;
	colours[3] = 255;

	gtk_color_selection_set_color (GTK_COLOR_SELECTION (colour_sel), colours);
}	

static void
palette_selection_changed (GtkColorSelection *colour_sel,
			   GtkPalette *palette)
{
	gdouble colours[4];
	gint red, green, blue;

	if (!(palette->selection >= 0))
	  return;

	gtk_color_selection_get_color (colour_sel, colours);
	red = colours[0] * 255;
	green = colours[1] * 255;
	blue = colours[2] * 255;

	gtk_palette_set_colour (palette, palette->selection, red, green, blue);
}

static void
palette_set (GtkButton *button,
	     GnomeDialog *edit)
{
  GtkPalette *old_palette, *new_palette;
  gchar *new_colours;

  old_palette = gtk_object_get_data (GTK_OBJECT (edit), "original-palette");
  new_palette = gtk_object_get_data (GTK_OBJECT (edit), "new-palette");

  new_colours = gtk_palette_get_palette (GTK_PALETTE (new_palette));
  gtk_palette_set_palette (GTK_PALETTE (old_palette), new_palette->numcolours,
			   new_colours);
}

static void
ie_get_palette (GdkPixbuf *pixbuf,
		GtkPalette *palette)
{
  GHashTable *colours;
  guchar *new_palette, *pixels, *row, *new_palette_ptr;
  gint numcols, x, y, width, rowstride, height;
  gint has_alpha, added;
  
  colours = g_hash_table_new (NULL, NULL);
  numcols = palette->numcolours;
  added = 0;
  new_palette = g_new (guchar, numcols * 3);
  new_palette_ptr = new_palette;

  pixels = gdk_pixbuf_get_pixels (pixbuf);
  height = gdk_pixbuf_get_height (pixbuf);
  width = gdk_pixbuf_get_width (pixbuf);
  rowstride = gdk_pixbuf_get_rowstride (pixbuf);
  has_alpha = gdk_pixbuf_get_has_alpha (pixbuf);

  for (y = 0; y < height; y++)
    {
      row = pixels + (y * rowstride);
      
      for (x = 0; x < width; x++)
	{
	  guint key;
	  guchar r = *(row++);
	  guchar g = *(row++);
	  guchar b = *(row++);

	  if (has_alpha)
	    row++;

	  key = (r << 16) + (g << 8) + b;

	  if (g_hash_table_lookup (colours, GINT_TO_POINTER (key)) == NULL)
	    {
	      /* Add key */
	      g_hash_table_insert (colours, GINT_TO_POINTER (key),
				   GINT_TO_POINTER (key));
	      *(new_palette++) = r;
	      *(new_palette++) = g;
	      *(new_palette++) = b;

	      added++;
	    }
	  if (added == numcols)
	    break;
	}
      if (added == numcols)
	break;
    }

  gtk_palette_set_palette (palette, added, new_palette_ptr);
  g_hash_table_destroy (colours);
}

static void
palette_from_file (GtkButton *button,
		   GtkPalette *e_palette)
{
  GtkWidget *fileentry, *dialog;
  gint reply;
  
  dialog = gnome_dialog_new (_("Load Palette From File"),
			     GNOME_STOCK_BUTTON_OK,
			     GNOME_STOCK_BUTTON_CANCEL,
			     NULL);
  gnome_dialog_set_close (GNOME_DIALOG (dialog), TRUE);
  gnome_dialog_close_hides (GNOME_DIALOG (dialog), TRUE);

  fileentry = gnome_file_entry_new ("iconedit:iconedit_palettefromfile_history",
				    _("Icon Editor: Browse Files For Palette From File"));
  gnome_dialog_editable_enters (GNOME_DIALOG (dialog),
				GTK_EDITABLE (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (fileentry))));
  gnome_dialog_set_default (GNOME_DIALOG (dialog), GNOME_OK);

  gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox),
		      fileentry,
		      TRUE, TRUE, GNOME_PAD);

  gtk_widget_grab_focus (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (fileentry)));
  gtk_widget_show_all (dialog);

  reply = gnome_dialog_run (GNOME_DIALOG (dialog));

  if (reply == GNOME_OK)
    {
      gchar *s = gnome_file_entry_get_full_path (GNOME_FILE_ENTRY (fileentry), FALSE);
      
      if (s && s[0])
	{
	  GdkPixbuf *pixbuf;

	  pixbuf = gdk_pixbuf_new_from_file (s);
	  if (pixbuf == NULL)
	    {
	      gchar *message;

	      message = g_strdup_printf (_("Could not open %s for reading."), s);
	      gnome_error_dialog (message);
	      g_free (message);
	      g_free (s);
	      gtk_widget_destroy (dialog);
	      return;
	    }
	  ie_get_palette (pixbuf, e_palette);
	  gdk_pixbuf_unref (pixbuf);
	  g_free (s);
	}
      else
	{
	  g_free (s);
	  gnome_error_dialog (_("You must give a filename"));
	}
    }

  gtk_widget_destroy (dialog);
}

void
palette_edit (GtkWidget *widget,
	      IEView *view)
{
  static GtkWidget *edit = NULL;
  GtkWidget *frame, *hbox, *vbox;
  GtkWidget *load, *save, *from;
  GtkWidget *palette, *e_palette, *colour_sel;

  palette = view->palette;
  if (edit != NULL)
    {
      g_assert (GTK_WIDGET_REALIZED (edit));
      gdk_window_show (edit->window);
      gdk_window_raise (edit->window);
    }
  else
    {
      edit = gnome_dialog_new (_("Edit Palette"),
			       GNOME_STOCK_BUTTON_OK,
			       GNOME_STOCK_BUTTON_CANCEL, NULL);
      
      gtk_object_set_data (GTK_OBJECT (edit), "original-palette", palette);
      
      gnome_dialog_set_close (GNOME_DIALOG (edit), TRUE);
      gnome_dialog_button_connect (GNOME_DIALOG (edit), 0,
				   GTK_SIGNAL_FUNC (palette_set), edit);
      
      frame = gtk_frame_new (NULL);
      gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
      gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (edit)->vbox), frame, TRUE, TRUE, 0);
      
      hbox = gtk_hbox_new (FALSE, GNOME_PAD);
      gtk_container_set_border_width (GTK_CONTAINER (hbox), 2);
      gtk_container_add (GTK_CONTAINER (frame), hbox);
      
      colour_sel = gtk_color_selection_new ();
      gtk_color_selection_set_update_policy (GTK_COLOR_SELECTION (colour_sel),
					     GTK_UPDATE_CONTINUOUS);
      gtk_box_pack_start (GTK_BOX (hbox), colour_sel, TRUE, TRUE, 0);
      
      e_palette = gtk_palette_new (GTK_PALETTE (palette)->numcolours);
      gtk_object_set_data (GTK_OBJECT (edit), "new-palette", e_palette);
      gtk_palette_set_palette (GTK_PALETTE (e_palette), 
			       GTK_PALETTE (palette)->numcolours,
			       GTK_PALETTE (palette)->colours);
      gtk_box_pack_start (GTK_BOX (hbox), e_palette, FALSE, FALSE, 0);
      
      vbox = gtk_vbox_new (FALSE, 0);
      load = gtk_button_new_with_label (_("Load Palette"));
      gtk_signal_connect (GTK_OBJECT (load), "clicked",
			  GTK_SIGNAL_FUNC (palette_load), e_palette);
      gtk_box_pack_start (GTK_BOX (vbox), load, FALSE, FALSE, GNOME_PAD);

      from = gtk_button_new_with_label (_("Load Palette From File"));
      gtk_signal_connect (GTK_OBJECT (from), "clicked",
			  GTK_SIGNAL_FUNC (palette_from_file), e_palette);
      gtk_box_pack_start (GTK_BOX (vbox), from, FALSE, FALSE, GNOME_PAD);

      save = gtk_button_new_with_label (_("Save Palette"));
      gtk_signal_connect (GTK_OBJECT (save), "clicked",
			  GTK_SIGNAL_FUNC (palette_save), e_palette);
      gtk_box_pack_start (GTK_BOX (vbox), save, FALSE, FALSE, GNOME_PAD);

      gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, GNOME_PAD);
      gtk_signal_connect (GTK_OBJECT (edit), "destroy",
			  GTK_SIGNAL_FUNC (gtk_widget_destroyed), &edit);
      gtk_signal_connect (GTK_OBJECT (e_palette), "selection-changed",
			  GTK_SIGNAL_FUNC (palette_colour_changed), colour_sel);
      gtk_signal_connect (GTK_OBJECT (colour_sel), "color-changed",
			  GTK_SIGNAL_FUNC (palette_selection_changed), e_palette);
      
      gtk_widget_show_all (edit);
    }
}
