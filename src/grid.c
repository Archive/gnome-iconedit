/* grid.c - Grid handling code.
 *
 * Copyright (C) 1999 Havoc Pennington
 * Copyright (C) 2000 Iain Holmes  <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "app.h"
#include "grid.h"
#include "view.h"
#include "edit.h"
#include <config.h>
#include <gnome.h>

#include <ie-canvas-layer.h>
#include <ie-canvas-chequer-board.h>
#include <ie-canvas-grid.h>
#include <ie-model.h>
#include <iestatus.h>

gint  item_event(GnomeCanvasItem* item, 
		 GdkEvent* event, 
		 gpointer data);
gint  viewless_item_event(GnomeCanvasItem* item, 
			  GdkEvent* event, 
			  IEBonoboView *view);

void grid_set_alpha(Grid* g, guint x, guint y, 
		    guchar a);
void grid_fill_recursive(IEModel *model, guint x, guint y,
			 gboolean *already,
			 guchar start_r, guchar start_g, guchar start_b,
			 guchar start_a, GdkColor* color, guchar a);
void grid_fill(IEModel *model, 
	       GtkObject *adjustment,
	       guint x, guint y, 
	       GdkColor* color);
static gint rect_cb (GnomeCanvasItem *item,
		     GdkEvent *event, 
		     gpointer data);

#define SQUARE_SIZE 17.0
#define OUTLINE_WIDTH 3.0

Grid* 
grid_new(IEView *view)
{
  guint width, height;
  Grid* g;

  g = g_new0(Grid, 1);

  g->view = view;
  width = view->model->width;
  height = view->model->height;

  gnome_canvas_set_scroll_region(GNOME_CANVAS(view->canvas),
				 0.0, 0.0,
				 1.0 * width,
				 1.0 * height);
  
  /* In order: bottom->top */
  g->cboard = gnome_canvas_item_new (GNOME_CANVAS_GROUP (GNOME_CANVAS(view->canvas)->root),
				     ie_canvas_chequer_board_get_type (),
				     "x", 0.0,
				     "y", 0.0,
				     "width", (gdouble)width * 1.0,
				     "height", (gdouble)height * 1.0,
				     "square_size", 16.0,
				     NULL);

  g->mainlayer = gnome_canvas_item_new (GNOME_CANVAS_GROUP (GNOME_CANVAS(view->canvas)->root),
					ie_canvas_layer_get_type (),
					"x", 0.0,
					"y", 0.0,
					"width", (double) width,
					"height", (double) height,
					"square_size", 1.0,
					"pixbuf", gdk_pixbuf_get_pixels (view->model->model),
					"rowstride", gdk_pixbuf_get_rowstride (view->model->model),
					NULL);

  g->gridlines = gnome_canvas_item_new (GNOME_CANVAS_GROUP (GNOME_CANVAS(view->canvas)->root),
					ie_canvas_grid_get_type (),
					"x", 0.0,
					"y", 0.0,
					"width", (double) width,
					"height", (double) height,
					"thickness", 2.0,
					"square_size", 1.0,
					"fill_color_rgba", 0x000000FF,
					NULL);
  /* Copy the pixbuf in. */
  grid_set_from_pixbuf (g, view->model->model);

  /* Set the zoom level */
  gnome_canvas_set_pixels_per_unit (GNOME_CANVAS (view->canvas), 10.0);
  gtk_signal_connect (GTK_OBJECT (g->mainlayer), "event",
		      GTK_SIGNAL_FUNC (item_event), g);
  g->sel = NULL;
  g->layer = NULL;
  return g;
}

Grid *
grid_new_bonoboview (GtkWidget *canvas, 
		     IEBonoboView *view)
{
  GnomeCanvasItem *root;
  guint width, height;
  Grid* g;

  g = g_new0(Grid, 1);

  width = view->model->width;
  height = view->model->height;

  gnome_canvas_set_scroll_region(GNOME_CANVAS(canvas),
				 0.0, 0.0,
				 1.0 * width,
				 1.0 * height);
  
  root = gnome_canvas_root (GNOME_CANVAS (canvas));
  /* In order: bottom->top */
  g->cboard = gnome_canvas_item_new (GNOME_CANVAS_GROUP (root),
				     ie_canvas_chequer_board_get_type (),
				     "x", 0.0,
				     "y", 0.0,
				     "width", (gdouble)width * 1.0,
				     "height", (gdouble)height * 1.0,
				     "square_size", 16.0,
				     NULL);

  g->mainlayer = gnome_canvas_item_new (GNOME_CANVAS_GROUP (root),
					ie_canvas_layer_get_type (),
					"x", 0.0,
					"y", 0.0,
					"width", (double) width,
					"height", (double) height,
					"square_size", 1.0,
					"pixbuf", gdk_pixbuf_get_pixels (view->model->model),
					"rowstride", gdk_pixbuf_get_rowstride (view->model->model),
					NULL);

  g->gridlines = gnome_canvas_item_new (GNOME_CANVAS_GROUP (root),
					ie_canvas_grid_get_type (),
					"x", 0.0,
					"y", 0.0,
					"width", (double) width,
					"height", (double) height,
					"thickness", 2.0,
					"square_size", 1.0,
					"fill_color_rgba", 0x000000FF,
					NULL);
  /* Copy the pixbuf in. */
  grid_set_from_pixbuf (g, view->model->model);

  /* Set the zoom level */
  gnome_canvas_set_pixels_per_unit (GNOME_CANVAS (canvas), 10.0);
  gtk_signal_connect (GTK_OBJECT (g->mainlayer), "event",
		      GTK_SIGNAL_FUNC (viewless_item_event), view);
  g->sel = NULL;
  g->layer = NULL;
  return g;
}

void
grid_destroy(Grid* g)
{
  gtk_object_destroy (GTK_OBJECT (g->mainlayer));
  gtk_object_destroy (GTK_OBJECT (g->cboard));
  gtk_object_destroy (GTK_OBJECT (g->gridlines));

  if (g->sel)
    gtk_object_destroy (GTK_OBJECT (g->sel));

  g_free(g);
}

/* Ditto */
const guchar* 
grid_alpha(Grid* g, guint x, guint y)
{
  guchar *pixels;
  guint i = y*g->view->model->rowstride + x*4;

  pixels = gdk_pixbuf_get_pixels (g->view->model->model);
  return pixels + i + 3; /* +3 because alpha is the 4th char in a group and i
			    points to the beginning of the group */
}

void 
grid_update_item(Grid* g,
		 gint x,
		 gint y,
		 gint width,
		 gint height)
{
  ie_canvas_layer_update_area (IE_CANVAS_LAYER (g->mainlayer),
			       (double)x, (double)y,
			       (double)width, (double)height);
}

void 
grid_set(Grid* g, guint x, guint y, 
         guchar r, guchar gr, guchar b, guchar a,
	 gboolean update)
{
/*    mode_convert_pixel (g, x, y, &r, &gr, &b, &a); */
  ie_model_set_pixel (g->view->model, x, y, r, gr, b, a);
}

void
grid_set_gdk(Grid* g, guint x, guint y,
             GdkColor* colour, guchar a, gboolean update)
{
    grid_set(g, x, y, colour->red/256, 
	     colour->green/256, colour->blue/256, a, update);
}

void 
grid_set_alpha(Grid* g, guint x, guint y, 
	       guchar a)
{
  guchar *pixels = gdk_pixbuf_get_pixels (g->view->model->model);
  guchar* alpha = pixels + (y*g->view->model->rowstride + (x * 4)) + 3;
  
  *alpha = a;
  
  grid_update_item(g, x, y, 1, 1);
}

void
grid_set_from_pixbuf(Grid* g, GdkPixbuf *pb)
{
  gnome_canvas_item_request_update (g->mainlayer);
}

GdkPixbuf*
grid_to_pixbuf (Grid *grid)
{
  gdk_pixbuf_ref (grid->view->model->model);
  return grid->view->model->model;
}

void
grid_fill_recursive(IEModel *model, guint x, guint y,
		    gboolean* already,
                    guchar start_r, guchar start_g, guchar start_b,
                    guchar start_a, GdkColor* colour, guchar a)
{
  guchar *pixels;
  guchar* pixel;
  gboolean *revisit = already + (y * model->height + x);

  if (*revisit)
    return;

  *revisit = TRUE;

  pixels = gdk_pixbuf_get_pixels (model->model);
  pixel = pixels + y * model->rowstride + x*4;

  if (pixel[0] == start_r && pixel[1] == start_g && pixel[2] == start_b &&
      pixel[3] == start_a) {
    if (colour) {
      ie_model_set_pixel (model, x, y, colour->red / 256,
			  colour->green / 256,
			  colour->blue / 256, a);
    } else {
      ie_model_set_pixel (model, x, y, 0, 0, 0, 0);
    }
    
    if (x < (model->width - 1))
      grid_fill_recursive(model, x+1, y, already, start_r, start_g, start_b, 
			  start_a, colour, a);
    
    if (x > 0)
      grid_fill_recursive(model, x-1, y, already, start_r, start_g, start_b, 
			  start_a, colour, a);
    
    if (y < (model->height - 1))
      grid_fill_recursive(model, x, y+1, already, start_r, start_g, start_b, 
			  start_a, colour, a);
    
    if (y > 0)
      grid_fill_recursive(model, x, y-1, already, start_r, start_g, start_b, 
			  start_a, colour, a);
  } else {
    return;
  }
}

void 
grid_fill(IEModel *model, 
	  GtkObject *adjustment,
	  guint x, guint y, 
          GdkColor* color) /* if color is NULL, fill transparent */
{
  /* This isn't going to be efficient, but it's easy. It's a small
     icon we're editing. */
  guchar* pixels = gdk_pixbuf_get_pixels (model->model);
  guchar* pixel = pixels + y * model->rowstride + x*4;
  guchar start_r = pixel[0]; 
  guchar start_g = pixel[1];
  guchar start_b = pixel[2];
  guchar start_a = pixel[3];
  guchar alpha = 255; 
  gboolean *already;

  if (adjustment)
    alpha = (guchar)GTK_ADJUSTMENT (adjustment)->value;
  
  already = g_new0 (gboolean, model->width * model->height);
  
  grid_fill_recursive(model, x, y, already, start_r, start_g, start_b, 
                      start_a, color, alpha);
  g_free (already);
}

/* Item event handlers */

static gint
rect_cb (GnomeCanvasItem *item,
	 GdkEvent *event, 
	 gpointer data)
{
  int result;
  Grid *grid;

  grid = (Grid *) data;
  g_return_val_if_fail (grid != NULL, FALSE);

  /* Pass all events upwards */
  gtk_signal_emit_by_name (GTK_OBJECT (grid->mainlayer), "event",
			   event, &result);
  
  return result;
}

gint  
item_event(GnomeCanvasItem* item, GdkEvent* event, gpointer data)
{
  Grid *grid = data;
  IEView *view = grid->view;
  IECanvasLayer *gi = IE_CANVAS_LAYER (item);
  IESelection *selection; 
  static gdouble ox = 0.0, oy = 0.0;
  gint r_x, r_y;
  static gint s_x = 0, s_y = 0;
  static gint actualx, actualy;
  double size;
  GtkArg arg[1];

  /* Get the square size */
  arg[0].name = "IECanvasLayer::square_size";
  gtk_object_getv (GTK_OBJECT (gi),
		   1, arg);
  g_assert (arg[0].type == GTK_TYPE_DOUBLE);
  size = GTK_VALUE_DOUBLE (arg[0]);
  
  switch (event->type) {
  case GDK_BUTTON_PRESS: {
    actualx = event->button.x;
    actualy = event->button.y;
    r_x = (gint)(event->button.x / size);
    r_y = (gint)(event->button.y / size);
    
    if (view->selection != NULL)
      ie_selection_destroy (view);
    
    switch (view->tool) {
    case IET_FG:
      switch (event->button.button) {
      case 1:
	grid_set_gdk(grid, r_x, r_y, &view->current_colour, 
		     (gint)GTK_ADJUSTMENT (view->alpha)->value, TRUE);
	return TRUE;
	
      case 2:
	/* makes flood fill work right - haha. */
	grid_set_gdk(grid, r_x, r_y, &grid->view->black, 0x0, TRUE);
	return TRUE;

      }
      break;

    case IET_FILL:
      switch (event->button.button) {
      case 1:
	grid_fill (view->model, view->alpha, r_x, r_y, &view->current_colour);
	break;
      case 2:
	/* fill transparent */
	grid_fill (view->model, view->alpha, r_x, r_y, NULL);
	break;
      }
      
      return TRUE;

    case IET_EYEDROP:
      if (event->button.button == 1) {
	const guchar* pixel = ie_model_get_pixel(view->model, r_x, r_y);
	
	view->current_colour.red = pixel[0]*256;
	view->current_colour.green = pixel[1]*256;
	view->current_colour.blue = pixel[2]*256;
	view->current_colour.pixel = 
	  gdk_rgb_xpixel_from_rgb((((guint)pixel[0]) << 16) +
				  (((guint)pixel[1]) << 8) +
				  pixel[2]);
	
	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(grid->view->picker),
				  pixel[0],
				  pixel[1],
				  pixel[2],
				  255);
	GTK_ADJUSTMENT (grid->view->alpha)->value = (gfloat)pixel[3];
	gtk_adjustment_value_changed (GTK_ADJUSTMENT (grid->view->alpha));
      }
      break;
      
    case IET_POINT: {
      GdkCursor *crosshairs;
      selection = ie_selection_get (view);
      ox = r_x * size;
      oy = r_y * size;
      selection->x = r_x;
      selection->y = r_y;
      if (selection->selitem)
	gtk_object_destroy (GTK_OBJECT (selection->selitem));
      selection->selitem = NULL;
      crosshairs = gdk_cursor_new (GDK_CROSS);
      gdk_window_set_cursor (view->canvas->window, crosshairs);
      gdk_cursor_destroy (crosshairs);
      return TRUE;
    }
    break;
    
    case IET_BRUSH:
      edit_paste_at (view, r_x, r_y);
      break;
      
    default:
      return FALSE;
    }
    break;
  }
  
  case GDK_BUTTON_RELEASE:
    r_x = (gint)(event->button.x / size);
    r_y = (gint)(event->button.y / size);
      
    if (grid->view->tool == IET_POINT) {
      gint t_x, t_y;
      selection = ie_selection_get (view);
      
      if (actualx == r_x * size && actualy == r_y * size) {
	ie_selection_destroy (view);
	return FALSE;
      }

      gdk_window_set_cursor (view->canvas->window, NULL);
      
      if (r_x < selection->x) {
	t_x = selection->x;
	selection->x = r_x;
      } else
	t_x = r_x;
      
      if (r_y < selection->y) {
	t_y = selection->y;
	selection->y = r_y;
      } else
	t_y = r_y;
      
      selection->model = view->model;
      selection->width = (t_x - selection->x) + 1;
      selection->height = (t_y - selection->y) + 1;
      
      return FALSE;
    }
    break;
    
  case GDK_MOTION_NOTIFY:
    r_x = (gint)(event->motion.x / size);
    r_y = (gint)(event->motion.y / size);
    
    if (r_x < 0 || r_y < 0 ||
	r_x > (gi->width - 1) || r_y > (gi->height - 1))
      return TRUE;
    
    if ((grid->view->tool == IET_POINT) &&
	((event->motion.state & GDK_BUTTON1_MASK) == GDK_BUTTON1_MASK)) {
      GnomeCanvasGroup *group;
      gdouble x, y;
      
      x = MIN ((gint)size * view->model->width, event->motion.x);
      y = MIN ((gint)size * view->model->height, event->motion.y);
      
      group = GNOME_CANVAS_GROUP (GNOME_CANVAS (view->canvas)->root);
      selection = ie_selection_get (view);
      if (selection->selitem == NULL) {
	selection->selitem = gnome_canvas_item_new (group,
						    gnome_canvas_rect_get_type (),
						    "x1", (double) ox,
						    "y1", (double) oy,
						    "x2", (double) r_x * size + size,
						    "y2", (double) r_y * size + size,
						    "fill_color_rgba", 0x8888FF55,
						    "outline_color", "white",
						    "width_pixels", 1,
						    NULL);
	gtk_signal_connect (GTK_OBJECT (selection->selitem), "event",
			    GTK_SIGNAL_FUNC (rect_cb), grid);
      } else {
	double t_x1, t_y1, t_x2, t_y2;
	if ((r_x * size) < ox) {
	  t_x1 = r_x * size;
	  t_x2 = ox;
	} else {
	  t_x1 = ox;
	  t_x2 = r_x * size;
	}
	
	if ((r_y * size) < oy) {
	  t_y1 = r_y * size;
	  t_y2 = oy;
	} else {
	  t_y1 = oy;
	  t_y2 = r_y * size;
	}
	
	gnome_canvas_item_set (selection->selitem,
			       "x1", t_x1,
			       "y1", t_y1,
			       "x2", t_x2 + size,
			       "y2", t_y2 + size,
			       NULL);
      }
      return TRUE;
    } else 
      if (view->tool == IET_FG && event->button.state & GDK_BUTTON1_MASK) {
	grid_set_gdk (grid, r_x, r_y, &view->current_colour,
		      (gint)GTK_ADJUSTMENT (view->alpha)->value, TRUE); 
      } else if (view->tool == IET_FG && event->button.state & GDK_BUTTON2_MASK) {
	grid_set_gdk (grid, r_x, r_y, &grid->view->black, 0x00, TRUE);
      }   
    
    if ((s_x == r_x) && (s_y == r_y) && (grid->sel != NULL))
      return TRUE;
    
    /* Display the co-ordinates and stuff in the statusbar */
    ie_update_statusbar (GTK_WIDGET (view), r_x, r_y);
    
    if (grid->sel == NULL) {
      grid->sel = gnome_canvas_item_new (gnome_canvas_root (item->canvas),
					 gnome_canvas_rect_get_type (),
					 "x1", (double) r_x * size,
					 "y1", (double) r_y * size,
					 "x2", (double) r_x * size + size, 
					 "y2", (double) r_y * size + size,
					 "fill_color", NULL,
					 "outline_color_rgba", 0xFFFFFFFF,
					 "width_pixels", 2,
					 NULL);
      
      gtk_signal_connect (GTK_OBJECT (grid->sel), "event",
			  GTK_SIGNAL_FUNC (rect_cb), grid);
    } else {
      gnome_canvas_item_set (grid->sel,
			     "x1", (double) r_x * size,
			     "y1", (double) r_y * size,
			     "x2", (double) r_x * size + size,
			     "y2", (double) r_y * size + size,
			     NULL);
    }
    s_x = r_x;
    s_y = r_y;
    return TRUE;
    break;
    
  default:
    break;
  }
  
  return FALSE;
}

gint  
viewless_item_event(GnomeCanvasItem* item, 
		    GdkEvent *event,
		    IEBonoboView *view)
{
  Grid *grid = (Grid *) view->grid;
  IECanvasLayer *gi = IE_CANVAS_LAYER (item);
  IESelection *selection; 
  static gdouble ox = 0.0, oy = 0.0;
  gint r_x, r_y;
  static gint s_x = 0, s_y = 0;
  static gint actualx, actualy;
  double size;
  GtkArg arg[1];

  /* Get the square size */
  arg[0].name = "IECanvasLayer::square_size";
  gtk_object_getv (GTK_OBJECT (gi),
		   1, arg);
  g_assert (arg[0].type == GTK_TYPE_DOUBLE);
  size = GTK_VALUE_DOUBLE (arg[0]);
  
  switch (event->type) {
  case GDK_BUTTON_PRESS: {
    actualx = event->button.x;
    actualy = event->button.y;
    r_x = (gint)(event->button.x / size);
    r_y = (gint)(event->button.y / size);

#if 0
    if (view->selection != NULL)
      ie_selection_destroy (view);
#endif

    switch (view->tool) {
    case IET_FG:
      switch (event->button.button) {
      case 1:
	ie_model_set_pixel (view->model, r_x, r_y, 
			    view->current_colour.red / 256, 
			    view->current_colour.green / 256, 
			    view->current_colour.blue / 256, 255);
	return TRUE;
	
      case 2:
	/* makes flood fill work right - haha. */
	ie_model_set_pixel (view->model, r_x, r_y, 0, 0, 0, 0x0);
	return TRUE;

      }
      break;

    case IET_FILL:
      switch (event->button.button) {
      case 1:
	grid_fill (view->model, NULL, r_x, r_y, &view->current_colour);
	break;
      case 2:
	/* fill transparent */
	grid_fill (view->model, NULL, r_x, r_y, NULL);
	break;
      }
      
      return TRUE;

    case IET_EYEDROP:
      if (event->button.button == 1) {
	const guchar* pixel = ie_model_get_pixel(view->model, r_x, r_y);
	
	view->current_colour.red = pixel[0]*256;
	view->current_colour.green = pixel[1]*256;
	view->current_colour.blue = pixel[2]*256;
	view->current_colour.pixel = 
	  gdk_rgb_xpixel_from_rgb((((guint)pixel[0]) << 16) +
				  (((guint)pixel[1]) << 8) +
				  pixel[2]);
	
/*  	gnome_color_picker_set_i8(GNOME_COLOR_PICKER(grid->view->picker), */
/*  				  pixel[0], */
/*  				  pixel[1], */
/*  				  pixel[2], */
/*  				  255); */
/*  	GTK_ADJUSTMENT (grid->view->alpha)->value = (gfloat)pixel[3]; */
/*  	gtk_adjustment_value_changed (GTK_ADJUSTMENT (grid->view->alpha)); */
      }
      break;
      
#if 0
    case IET_POINT: {
      GdkCursor *crosshairs;
      selection = ie_selection_get (view);
      ox = r_x * size;
      oy = r_y * size;
      selection->x = r_x;
      selection->y = r_y;
      if (selection->selitem)
	gtk_object_destroy (GTK_OBJECT (selection->selitem));
      selection->selitem = NULL;
      crosshairs = gdk_cursor_new (GDK_CROSS);
      gdk_window_set_cursor (view->canvas->window, crosshairs);
      gdk_cursor_destroy (crosshairs);
      return TRUE;
    }
    break;
    
    case IET_BRUSH:
      edit_paste_at (view, r_x, r_y);
      break;
#endif
 
    default:
      return FALSE;
    }
    break;
  }

  case GDK_BUTTON_RELEASE:
    r_x = (gint)(event->button.x / size);
    r_y = (gint)(event->button.y / size);

#if 0      
    if (grid->view->tool == IET_POINT) {
      gint t_x, t_y;
      selection = ie_selection_get (view);
      
      if (actualx == r_x * size && actualy == r_y * size) {
	ie_selection_destroy (view);
	return FALSE;
      }

      gdk_window_set_cursor (view->canvas->window, NULL);
      
      if (r_x < selection->x) {
	t_x = selection->x;
	selection->x = r_x;
      } else
	t_x = r_x;
      
      if (r_y < selection->y) {
	t_y = selection->y;
	selection->y = r_y;
      } else
	t_y = r_y;
      
      selection->model = view->model;
      selection->width = (t_x - selection->x) + 1;
      selection->height = (t_y - selection->y) + 1;
      
      return FALSE;
    }
#endif
    break;

  case GDK_MOTION_NOTIFY:
    r_x = (gint)(event->motion.x / size);
    r_y = (gint)(event->motion.y / size);
    
    if (r_x < 0 || r_y < 0 ||
	r_x > (gi->width - 1) || r_y > (gi->height - 1))
      return TRUE;

#if 0
    if ((view->tool == IET_POINT) &&
	((event->motion.state & GDK_BUTTON1_MASK) == GDK_BUTTON1_MASK)) {
      GnomeCanvasGroup *group;
      gdouble x, y;
      
      x = MIN ((gint)size * view->model->width, event->motion.x);
      y = MIN ((gint)size * view->model->height, event->motion.y);
      
      group = GNOME_CANVAS_GROUP (GNOME_CANVAS (view->canvas)->root);
      selection = ie_selection_get (view);
      if (selection->selitem == NULL) {
	selection->selitem = gnome_canvas_item_new (group,
						    gnome_canvas_rect_get_type (),
						    "x1", (double) ox,
						    "y1", (double) oy,
						    "x2", (double) r_x * size + size,
						    "y2", (double) r_y * size + size,
						    "fill_color_rgba", 0x8888FF55,
						    "outline_color", "white",
						    "width_pixels", 1,
						    NULL);
	gtk_signal_connect (GTK_OBJECT (selection->selitem), "event",
			    GTK_SIGNAL_FUNC (rect_cb), grid);
      } else {
	double t_x1, t_y1, t_x2, t_y2;
	if ((r_x * size) < ox) {
	  t_x1 = r_x * size;
	  t_x2 = ox;
	} else {
	  t_x1 = ox;
	  t_x2 = r_x * size;
	}
	
	if ((r_y * size) < oy) {
	  t_y1 = r_y * size;
	  t_y2 = oy;
	} else {
	  t_y1 = oy;
	  t_y2 = r_y * size;
	}
	
	gnome_canvas_item_set (selection->selitem,
			       "x1", t_x1,
			       "y1", t_y1,
			       "x2", t_x2 + size,
			       "y2", t_y2 + size,
			       NULL);
      }
      return TRUE;
    } else 
#endif
      if (view->tool == IET_FG && event->button.state & GDK_BUTTON1_MASK) {
	ie_model_set_pixel (view->model, r_x, r_y, 
			    view->current_colour.red / 256, 
			    view->current_colour.green / 256, 
			    view->current_colour.blue / 256, 255);

      } else if (view->tool == IET_FG && event->button.state & GDK_BUTTON2_MASK) {
	ie_model_set_pixel (view->model, r_x, r_y, 0, 0, 0, 0x00);
      }   
    
    if ((s_x == r_x) && (s_y == r_y) && (grid->sel != NULL))
      return TRUE;

    /* Display the co-ordinates and stuff in the statusbar */
/*      ie_update_statusbar (GTK_WIDGET (view), r_x, r_y); */

    if (grid->sel == NULL) {
      grid->sel = gnome_canvas_item_new (gnome_canvas_root (item->canvas),
					 gnome_canvas_rect_get_type (),
					 "x1", (double) r_x * size,
					 "y1", (double) r_y * size,
					 "x2", (double) r_x * size + size, 
					 "y2", (double) r_y * size + size,
					 "fill_color", NULL,
					 "outline_color_rgba", 0xFFFFFFFF,
					 "width_pixels", 2,
					 NULL);
      
      gtk_signal_connect (GTK_OBJECT (grid->sel), "event",
			  GTK_SIGNAL_FUNC (rect_cb), grid);
    } else {
      gnome_canvas_item_set (grid->sel,
			     "x1", (double) r_x * size,
			     "y1", (double) r_y * size,
			     "x2", (double) r_x * size + size,
			     "y2", (double) r_y * size + size,
			     NULL);
    }
    s_x = r_x;
    s_y = r_y;
    return TRUE;
    break;
    
  default:
    break;
  }
  
  return FALSE;
}
