/*
  io.h - Header for file routines.

  Copyright (C) 1999-2000 Iain Holmes <ih@csd.abdn.ac.uk>
*/

#ifndef __IO_H__
#define __IO_H__

gboolean ie_save_pixbuf (GnomeApp *app,
			 GdkPixbuf *pixbuf,
			 gchar *filename);

#endif
