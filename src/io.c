/* io.c
 * Icon saving routines
 * Icon loading is handled internally by GdkPixbuf.
 *
 * Based on code from the document "A description on how to use and modify libpng"
 * Copyright (C) 1999 Iain Holmes <ih@csd.abdn.ac.uk>
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h> /* Include this before png.h, or see lots of shadowed
		      variable warnings */
#include <stdio.h> /* Must be included, otherwise there is an error in png.h.
		      Bit wierd really. */
#include <png.h>
#include <X11/xpm.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <math.h>

#include "io.h"

gboolean ie_save_xpm_pixbuf (GnomeApp *app,
			     GdkPixbuf *pixbuf,
			     gchar *filename);
gboolean ie_save_png_pixbuf (GnomeApp *app,
			     GdkPixbuf *pixbuf,
			     FILE *handle,
			     gchar *filename);

gboolean
ie_save_pixbuf (GnomeApp *app,
		GdkPixbuf *pixbuf,
		gchar *filename)
{
  FILE *handle;
  gchar *ext;

  g_return_val_if_fail (pixbuf != NULL, FALSE);
  g_return_val_if_fail (filename != NULL, FALSE);
  g_return_val_if_fail (filename[0] != '\0', FALSE);

  ext = strrchr (filename, '.');

  if (ext && (strcmp (++ext, "xpm") == 0))
    ie_save_xpm_pixbuf (app, pixbuf, filename);
  else {
    gchar *new_filename = NULL;
    
    if (!ext || (strcmp (ext, "png") != 0)) {
      new_filename = g_strdup_printf ("%s.png", filename);
      filename = new_filename;
    }
    
    handle = fopen (filename, "wb");
    if (handle == NULL) {
      GtkWidget *errorbox;
      
      errorbox = gnome_error_dialog_parented (_("There was an error writing the file."),
					      GTK_WINDOW (app));
      return FALSE;
    }
    
    ie_save_png_pixbuf (app, pixbuf, handle, filename);
    g_free (new_filename);
    fclose (handle);
  }
  
  return TRUE;
}

/* Based on code from the GIMP */

int cpp;
static const char linenoise [] =
" .+@#$%&*=-;>,')!~{]^/(_:<[}|1234567890abcdefghijklmnopqrstuvwxyz\
ABCDEFGHIJKLMNOPQRSTUVWXYZ`";

typedef struct
{
  guchar r;
  guchar g;
  guchar b;
} rgbkey;

static guint
rgbhash (rgbkey *c)
{
  return ((guint)c->r) ^ ((guint)c->g) ^ ((guint)c->b);
}

static guint 
compare (rgbkey *c1,
	 rgbkey *c2)
{
  return (c1->r == c2->r)&&(c1->g ==c2->g)&&(c1->b == c2->b);
}

static void
set_XpmImage (XpmColor *array,
	      guint ie_index,
	      gchar *colorstring)
{
  gchar *p;
  int i, charnum, indtemp;

  indtemp = ie_index;
  array[ie_index].string = p = g_new (gchar, cpp+1);
  
  /*convert the index number to base sizeof(linenoise)-1 */
  for(i=0; i<cpp; ++i) {
    charnum = indtemp%(sizeof(linenoise)-1);
    indtemp = indtemp / (sizeof (linenoise)-1);
    *p++=linenoise[charnum];
  }
  
  *p = '\0'; /* C and its stupid null-terminated strings...*/
  
  array[ie_index].symbolic = NULL;
  array[ie_index].m_color = NULL;
  array[ie_index].g4_color = NULL;
  array[ie_index].c_color = NULL;
  array[ie_index].g_color = colorstring;
}

static void
create_colormap_from_hash (gpointer gkey,
			   gpointer value,
			   gpointer user_data)
{
  rgbkey *key = gkey;
  char *string = g_new (char, 8);

  sprintf (string, "#%02X%02X%02X", (int)key->r, (int)key->g, (int)key->b);
  set_XpmImage(user_data, *((int *) value), string);
}

static void
free_hash_table (gpointer key,
		 gpointer value,
		 gpointer user_data)
{
  /* Free the rgbkey */
  g_free (key);
  /* Free the gint indexno */
  g_free (value);
}

gboolean
ie_save_xpm_pixbuf (GnomeApp *app,
		    GdkPixbuf *pixbuf,
		    gchar *filename)
{
  gint width, height, has_alpha, rowstride;
  gint ncolors = 1;
  gint *indexno;
  XpmColor *colormap;
  XpmImage *image;
  guint *ibuff;
  guchar *pixels, *row;

  GHashTable *hash = NULL;
  int x, y;

  GtkWidget *window, *progress, *label, *vbox;
  char *title;

  has_alpha = gdk_pixbuf_get_has_alpha (pixbuf);
  width = gdk_pixbuf_get_width (pixbuf);
  height = gdk_pixbuf_get_height (pixbuf);
  pixels = gdk_pixbuf_get_pixels (pixbuf);
  rowstride = gdk_pixbuf_get_rowstride (pixbuf);

  ibuff = g_new (guint, width*height);
  hash = g_hash_table_new ((GHashFunc)rgbhash, (GCompareFunc) compare);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  title = g_strdup_printf (_("Saving %s"), filename);
  gtk_window_set_title (GTK_WINDOW (window), title);
  g_free (title);

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  label = gtk_label_new (_("Saving"));
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
  progress = gtk_progress_bar_new ();
  gtk_box_pack_start (GTK_BOX (vbox), progress, TRUE, TRUE, 0);
  gtk_widget_show_all (window);

  for (y = 0; y < height; y++) {
    guint *idata = ibuff + (y * width);
    row = pixels + (y * rowstride);
    
    for (x = 0; x < width; x++) {
      rgbkey *key = g_new (rgbkey, 1);
      guchar a;
      
      key->r = *(row++);
      key->g = *(row++);
      key->b = *(row++);
      a = has_alpha ? *(row++) : 255;
      
      if (a < 127)
	*(idata++) = 0;
      else {
	indexno = g_hash_table_lookup (hash, key);
	if (!indexno) {
	  indexno = g_new (int, 1);
	  *indexno = ncolors++;
	  g_hash_table_insert (hash, key, indexno);
	  key = g_new (rgbkey, 1);
	}
	*(idata++) = *indexno;
      }
    }
    gtk_progress_bar_update (GTK_PROGRESS_BAR (progress), ((gfloat)y)/height);
    while (gtk_events_pending ())
      gtk_main_iteration ();
  }
  
  colormap = g_new (XpmColor, ncolors);
  cpp = (double)1.0 + (double)log (ncolors) / (double)log (sizeof (linenoise) - 1.0);
  
  set_XpmImage (colormap, 0, "None");
  
  g_hash_table_foreach (hash, create_colormap_from_hash, colormap);

  image = g_new (XpmImage, 1);

  image->width = width;
  image->height = height;
  image->ncolors = ncolors;
  image->cpp = cpp;
  image->colorTable = colormap;
  image->data = ibuff;

  XpmWriteFileFromXpmImage (filename, image, NULL);
  
  g_free (ibuff);
  g_hash_table_foreach (hash, free_hash_table, NULL);
  g_hash_table_destroy (hash);
  gtk_widget_destroy (window);

  return TRUE;
}

/*** End of stuff from GIMP */

gboolean
ie_save_png_pixbuf (GnomeApp *app,
		    GdkPixbuf *pixbuf,
		    FILE *handle,
		    gchar *filename)
{
  GtkWidget *window, *label, *progress, *vbox;
  char *title;
  gint width, height, depth, rowstride;
  guchar *pixels;
  png_structp png_ptr;
  png_infop info_ptr;
  png_text text[2];
  int i;

  png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (png_ptr == NULL) {
    GtkWidget *errorbox;
    
    errorbox = gnome_error_dialog_parented (_("There was an error writing the file."),
					      GTK_WINDOW (app));
    return FALSE;
  }

  info_ptr = png_create_info_struct (png_ptr);
  if (info_ptr == NULL) {
    GtkWidget *errorbox;
    png_destroy_write_struct (&png_ptr, (png_infopp)NULL);
    
    errorbox = gnome_error_dialog_parented (_("There was an error writing the file."),
					    GTK_WINDOW (app));
    return FALSE;
  }
  
  if (setjmp (png_ptr->jmpbuf)) {
    GtkWidget *errorbox;
    
    /* Error handler */
    png_destroy_write_struct (&png_ptr, &info_ptr);
    errorbox = gnome_error_dialog_parented (_("There was an error writing the file."),
					    GTK_WINDOW (app));
    return FALSE;
  }
  
  png_init_io (png_ptr, handle);
  
  width = gdk_pixbuf_get_width (pixbuf);
  height = gdk_pixbuf_get_height (pixbuf);
  depth = gdk_pixbuf_get_bits_per_sample (pixbuf);
  pixels = gdk_pixbuf_get_pixels (pixbuf);
  rowstride = gdk_pixbuf_get_rowstride (pixbuf);

  png_set_IHDR (png_ptr, info_ptr, width, height,
		depth, PNG_COLOR_TYPE_RGB_ALPHA,
		PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT,
		PNG_FILTER_TYPE_DEFAULT);
  
  /* Some text to go with the png image */
  text[0].key = "Title";
  text[0].text = filename;
  text[0].compression = PNG_TEXT_COMPRESSION_NONE;
  text[1].key = "Software";
  text[1].text = "GNOME Icon Editor";
  text[1].compression = PNG_TEXT_COMPRESSION_NONE;
  png_set_text (png_ptr, info_ptr, text, 2);

  /* Write header data */
  png_write_info (png_ptr, info_ptr);
  
  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  title = g_strdup_printf (_("Saving %s"), filename);
  gtk_window_set_title (GTK_WINDOW (window), title);
  g_free (title);

  vbox = gtk_vbox_new (FALSE, 0);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  label = gtk_label_new (_("Saving"));
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
  progress = gtk_progress_bar_new ();
  gtk_box_pack_start (GTK_BOX (vbox), progress, TRUE, TRUE, 0);
  gtk_widget_show_all (window);

  for (i = 0; i < height; i++) {
    png_bytep row_pointer = pixels;
    png_write_row (png_ptr, row_pointer);
    pixels += rowstride;
    gtk_progress_bar_update (GTK_PROGRESS_BAR (progress), ((gfloat)i)/height);
    while (gtk_events_pending ())
      gtk_main_iteration ();
  }
  
  png_write_end (png_ptr, info_ptr);
  png_destroy_write_struct (&png_ptr, &info_ptr);
  
  gtk_widget_destroy (window);
  return TRUE;
}

